import {validUser, API_BASE_PATH, UI_BASE_PATH, validDemand, validMultipleDemand} from '../const';

export const login = (user= validUser) => {
    cy.request('POST', API_BASE_PATH + '/api/employees/login', user )
    .its('body')
    .as('authResponse')
    .clearLocalStorage()
    .should(function(ls) {

        const {id, userId} = this.authResponse;

        ls.setItem("token", id);
        ls.setItem("userId", userId);

        expect(ls.getItem('token')).not.to.be.undefined;
        expect(ls.getItem('userId')).not.to.be.undefined;

        cy.visit(UI_BASE_PATH);

        // It has to be equal to root because pre authenticated
        cy.url().should('eq',  UI_BASE_PATH + '/');

    })
};

export const logout = () => {
    cy.get("#logout-btn").click();
};

export const createSingleDemand = (demand = validDemand) =>{

    // It has to be equal to root because pre authenticated
    cy.url().should('eq',  UI_BASE_PATH + '/');

    cy.get('#add-demand-btn').click();

    //Populate form with a valid demand
    // NB: reverse order is to ensure elements are not covered by popovers
    //deadline
    cy.get(".deadline-group input[type='text']").click();
    cy.get(".deadline-group .date-picker-popover td").contains("2").click();
    //assigned to
    cy.get(".assigned-to-row .react-selectize-control").click();
    cy.get(".assigned-to-row .react-selectize").contains(demand.assigned_to).click();
    cy.get(".assigned-to-row .react-selectize-control").click();
    //description
    cy.get("#create-edit-form textarea[name='description']").type(demand.description);
    //title
    cy.get("#create-edit-form input[name='title']").type(demand.title);

    //Submit
    cy.get("#submit-demand-button").click();

};

export const createMultipleDemand = ( demand = validMultipleDemand) => {

    cy.get('#add-demand-btn').click();

    //Populate form with a valid demand
    // NB: reverse order is to ensure elements are not covered by popovers
    //deadline
    cy.get(".deadline-group input[type='text']").click();

    cy.get(".deadline-group .date-picker-popover td").contains("2").click();
    //assigned to
    cy.get(".assigned-to-row .react-selectize-control").click();
    demand.assigned_to.map( u => {
        cy.get(".assigned-to-row .react-selectize").contains(u).click();

    });
    cy.get(".assigned-to-row .react-selectize-control").click();



    //description
    cy.get("#create-edit-form textarea[name='description']").type(demand.description);

    //Demand visibility
    if(demand.visibleToOtherAssignees){
        cy.get("label[for='multi-assignee-visibility-checkbox']").click();
    }

    //title
    cy.get("#create-edit-form input[name='title']").type(demand.title);



    //Submit
    cy.get("#submit-demand-button").click();

};