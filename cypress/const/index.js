export const API_BASE_PATH = "http://localhost:3001";
export const UI_BASE_PATH = 'http://localhost:3000';
export const StaffUser = {username: "selleri", password: "asd"};
export const DirectorUser = {username: "muthoo", password: "asd"};
export const DgUser = {username: "salvatore", password: "asd"};
export const validUser = DgUser;
export const invalidUser = {username: "asd", password: "asd"};
export const validDemand = {
    title: "This is the demand title!",
    description: "... and this is the description",
    assigned_to: "SELLERI, MR NICOLA"
};
export const validMultipleDemand = {
    title: "This is the multiple title!",
    description: "... and this is the multiple description",
    assigned_to: ["SELLERI, MR NICOLA", "BRAVO ALVAREZ, MR CARLOS"]
};
