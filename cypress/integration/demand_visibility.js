import {login, createMultipleDemand, logout} from '../common';
import {validMultipleDemand, DgUser, StaffUser, UI_BASE_PATH} from '../const';

describe('Demand visibility to other assignee', function () {

    let demand = Object.assign({}, validMultipleDemand);
    demand.title += new Date().getTime();

    beforeEach(() => {
        cy.viewport(1366,768)
    });

    //afterEach(logout);

    const openDemandPage = (d) => {
        cy.visit(UI_BASE_PATH);
        cy.contains(d.title).click();
    };

    // it('Demand should not be visible', () => {
    //
    //     login(DgUser);
    //     cy.visit(UI_BASE_PATH);
    //     const hiddenDemand = Object.assign({}, demand, {visibleToOtherAssignees : false});
    //     createMultipleDemand(hiddenDemand);
    //     // //Check if the first table row contains the just inserted information
    //     // expect(cy.get('.rt-tbody .rt-tr:first-child').contains(hiddenDemand.title)).to.be.undefined;
    //     // expect(cy.get('.rt-tbody .rt-tr:first-child .created-col')).to.be.undefined;
    //
    //     logout();
    //
    //     login(StaffUser);
    //     openDemandPage(hiddenDemand);
    //
    //     cy.get('#link-to-parent-demand').should('not.exist')
    //
    //
    // });

    it('Demand should be visible', () => {

        login(DgUser);
        cy.visit(UI_BASE_PATH);
        const visibleDemand = Object.assign({}, demand, {visibleToOtherAssignees : true});
        createMultipleDemand(visibleDemand);
        //Check if the first table row contains the just inserted information
        //expect(cy.get('.rt-tbody .rt-tr:first-child').contains(visibleDemand.title)).not.to.be.undefined;
        //expect(cy.get('.rt-tbody .rt-tr:first-child .created-col')).not.to.be.undefined;

        logout();

        login(StaffUser);
        openDemandPage(visibleDemand);
        cy.get('#link-to-parent-demand').should('exist')
    });

});