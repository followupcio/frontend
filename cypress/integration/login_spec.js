import {validUser, invalidUser, UI_BASE_PATH} from '../const';

describe('Login page', function(){

    it('Login with valid user', function() {
        cy.visit(UI_BASE_PATH);

        // we should be redirected to /login
        cy.url().should('eq', UI_BASE_PATH + '/login');

        cy.get('input[name=username]').type(validUser.username);

        // {enter} causes the form to submit
        cy.get('input[name=password]').type(`${validUser.password}{enter}`);

        // we should be redirected to /
        cy.url().should('eq',  UI_BASE_PATH + '/');
    });

    it('Login with invalid user', function() {
        cy.visit(UI_BASE_PATH);

        cy.get('input[name=username]').type(invalidUser.username);

        // {enter} causes the form to submit
        cy.get('input[name=password]').type(`${invalidUser.password}{enter}`);

        // we should be redirected to /dashboard
        cy.url().should('eq', UI_BASE_PATH + '/login');

        cy.get('#login-error-message')
            .contains('Incorrect username or password.').should('have.class', 'alert alert-danger');

    });

});
