import {login, logout, createSingleDemand, createMultipleDemand} from '../common';
import {validDemand, validMultipleDemand} from '../const';

describe('Home page', function () {

    beforeEach(login);

    afterEach(logout);

    it('Create a demand', () => {

        createSingleDemand();

        //Check if the first table row contains the just inserted information
        expect(cy.get('.rt-tbody .rt-tr:first-child').contains(validDemand.title)).not.to.be.undefined;
        expect(cy.get('.rt-tbody .rt-tr:first-child .created-col')).not.to.be.undefined;
    });

    it('Create a multiple demand', () => {
        createMultipleDemand(validMultipleDemand);

        //Check if the first table row contains the just inserted information
        //expect(cy.get('.rt-tbody .rt-tr:first-child').contains(validDemand.title)).not.to.be.undefined;
        //expect(cy.get('.rt-tbody .rt-tr:first-child .created-col')).not.to.be.undefined;
    });

    it('Change app style', () => {

        cy.get("[data-reactroot]").then(($root) => {

            const style = $root[0].getAttribute("data-style");

            cy.get(`.app-style.${style}-style`).should('have.class', 'active');

            cy.get(`.app-style.table-style`).click();
            cy.get(`.app-style.table-style`).should('have.class', 'active');

            cy.get(`.app-style.cards-style`).click();
            cy.get(`.app-style.cards-style`).should('have.class', 'active');

        })

    });

    it('Open and close the drawer', () => {

        const DRAWER = ".home-col-right";
        const DRAWER_TOGGLE = ".drawer-toggle";
        const HOME_CONTAINER = ".home-container";

        cy.get(DRAWER).should('be.visible');
        cy.get(HOME_CONTAINER).should('not.have.class', 'drawer-open');

        cy.get(DRAWER_TOGGLE).click();

        //cy.get(DRAWER).should('not.be.visible');
        cy.get(HOME_CONTAINER).should('have.class', 'drawer-open');
        //Check drawer status is stored after reload
        cy.reload();
        cy.get(HOME_CONTAINER).should('have.class', 'drawer-open');

        cy.get(DRAWER_TOGGLE).click();
        cy.get(HOME_CONTAINER).should('not.have.class', 'drawer-open');

    });

    /* it('Change tabs', () => {

         cy.contains("Completed").click()
     });*/


    it('Change page in demand table', () => {

        cy.get(".rt-table").then(() => {
            const intialTablePage = Cypress.$(".-currentPage").text();

            cy.contains("Next").click();

            //Wait for table loading after page change
            cy.get(".rt-table").then(() => {
                const nextTablePage = Cypress.$(".-currentPage").text();
                expect(parseInt(nextTablePage)).to.equal(parseInt(intialTablePage) + 1)
            })
        })

    });


});
