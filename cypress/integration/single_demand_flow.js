import {login, createSingleDemand, logout} from '../common';
import {validDemand, DgUser, StaffUser, UI_BASE_PATH} from '../const';
import loremIpsum from 'lorem-ipsum';

describe('Single demand flow', function () {

    let demand = Object.assign({}, validDemand);
    demand.title += new Date().getTime();
    let commentText = loremIpsum();

    beforeEach(() => {
        cy.viewport(1366,768)
    });

    afterEach(logout);

    const openDemandPage = (d) => {
        cy.visit(UI_BASE_PATH);
        cy.contains(d.title).click();
    };

    const submitDemand = () => {

        openDemandPage(demand);
        // Submit for review
        cy.get("#submit-for-review-btn").click();
        cy.get("#submit-demand-description").type(loremIpsum());
        cy.get("#submit-demand-submit-btn").click();
    };

    it('As DG: create a demand', () => {

        login(DgUser);
        cy.visit(UI_BASE_PATH);
        createSingleDemand(demand);
        //Check if the first table row contains the just inserted information
        expect(cy.get('.rt-tbody .rt-tr:first-child').contains(demand.title)).not.to.be.undefined;
        expect(cy.get('.rt-tbody .rt-tr:first-child .created-col')).not.to.be.undefined;

    });

    it('As Staff: open the new demand and read it', () => {
        login(StaffUser);

        openDemandPage(demand);

        // Comment the demand
        // Check read in timeline
        cy.get(".commentList").contains("Read").should('be.visible');
    });

    it('As Staff: comment the new demand', () => {
        login(StaffUser);

        openDemandPage(demand);

        cy.get('#demand-comment-textarea').type(commentText);
        cy.get('#demand-comment-submit-btn').click();
        cy.contains(commentText).should('be.visible');
    });

    it('As Staff: edit comment', () => {
        login(StaffUser);

        openDemandPage(demand);

        cy.get('#bg-nested-dropdown').click();
        cy.get('.dropdown-menu').contains("Edit").click();
        const updatedText = loremIpsum();
        cy.get("#comment-edit-textarea").type(updatedText);
        cy.get("#comment-edit-submit-btn").click();
        cy.get('.commentList').contains(updatedText).should('be.visible');

    });

    it('As Staff: delete comment', () => {
        login(StaffUser);

        openDemandPage(demand);

        cy.get('#bg-nested-dropdown').click();
        cy.get('.dropdown-menu').contains("Remove").click();
        cy.get('#remove-comment-submit-btn').click();
        cy.get('.commentList').contains("This message has been removed").should('be.visible');

    });

    it('As Staff: submit for review the new demand', () => {

        login(StaffUser);

        submitDemand();
    });

    it('As Staff: reopen new demand', () => {

        login(StaffUser);

        openDemandPage(demand);

        cy.get('#reopen-demand-btn').click();

        cy.get('.commentList').contains("Reopened").should('be.visible');


    });

    it('As Staff: submit again for review the new demand', () => {

        login(StaffUser);

        submitDemand();
    });

    it('As DG: reject the new demand', () => {
        login(DgUser);
        openDemandPage(demand);
        cy.get("#reject-demand-btn").click();
        cy.get("#reject-demand-description").type(loremIpsum());
        cy.get("#reject-demand-submit-btn").click();

        cy.visit(UI_BASE_PATH);
    });

    it('As Staff: submit again for review the new demand', () => {

        login(StaffUser);

        submitDemand();
    });

    it('As DG: approve the new demand', () => {
        login(DgUser);
        openDemandPage(demand);
        cy.get("#approve-demand-btn").click();
        cy.get("#approve-demand-description").type(loremIpsum());
        cy.get("#approve-demand-submit-btn").click();

        cy.visit(UI_BASE_PATH);
    });

    /*
    * Todo
    * - unarchive demand
    * - archive demand
    * - reopen a completed demand as dg
    * - delete
    * - delete permanently
    * - save submit as draft
    *
    * Everything should check the correspondent history item
    * */

});