describe('a-spec', () => {
    describe('page', () => {

        it('Health check', function () {
            expect(true).to.equal(true);

            cy.request('GET', 'http://127.0.0.1:3001/api/logs/emails')
                .then(function (response) {

                    console.log('####################');
                    console.log('####################');
                    console.log('####################');
                    console.log('####################');
                    console.log(JSON.stringify(response));
                    // response.body is automatically serialized into JSON
                    //expect(response).not.to.be.undefined;
                    expect(response.status).to.equal(200);
                    expect(response.body).be.an('array');

                })

        })


    })
})
