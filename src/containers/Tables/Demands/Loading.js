import React, {PureComponent} from 'react';

class Loading extends PureComponent {

    render() {
        return (<div className="text-center loading-holder"><span className="loading loading-48"/></div>);
    }
}

export default Loading;
