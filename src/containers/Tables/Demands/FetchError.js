import React from 'react';
import {Alert, Button} from 'react-bootstrap';

const FetchError = ({message, onRetry}) => (
    <Alert bsStyle="danger">
        <h4>Oh snap! You got an error!</h4>
        <p>Could not fetch demands. {message}</p>
        <p>
            <Button bsStyle="danger" onClick={onRetry}>Retry</Button>
        </p>
    </Alert>
);

export default FetchError;
