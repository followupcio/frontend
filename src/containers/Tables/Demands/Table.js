import React from 'react';
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import Moment from 'moment';
import {DATE_FORMAT_TABLE} from '../../../conf/index'
import * as ROLES from '../../../conf/roles'


const buildName = (d) => {
    if (!d) {
        return ""
    }
    return `${d.last_name}, ${d.first_name} (${d.division})`;
};

const formatText = (demand, text, force) => {

    const childrenCount = demand.children && demand.children.length;
    const childrenReadCount = demand.children && demand.children.reduce((acc, item) => acc + (!!item.read ? 1 : 0), 0);

    force = (childrenCount && childrenReadCount === childrenCount);

    return !!demand.read || !!force ? <span>{text}</span> : <span className="text-bold">{text}</span>;

};

const getColumns = (authUser, isInMultipleDemandPage) => {

    if (!authUser.role) {
        return [];
    }

    //const isDirector = authUser.role.indexOf(ROLES.DIRECTOR) > -1;
    const isDG = authUser.role.indexOf(ROLES.DG) > -1;
    const isFollower = authUser.role.indexOf(ROLES.FOLLOWER) > -1;
    const isStaff = authUser.role.indexOf(ROLES.STAFF) > -1;
    //const authUserId = authUser.id;

    return [
        {
            Header: 'ID',
            accessor: 'id',
            show: false

        },
        {
            show: false,
            Header: 'timestamp',
            accessor: 'created_at',

        },
        {
            Header: 'Created by',
            id: 'created_by',
            //show: !isDirector,
            accessor: d => buildName(d.issuer),
            className: 'created-col',
            maxWidth: 200,
            Cell: (props) => {
                const {original} = props;
                const issuer = original.issuer || {};
                const requester = original.requester || {};
                const authUserId = authUser.id;
                const isIssuedByMe = authUserId === original.created_by;
                const isAssignedToMe = authUserId === original.assigned_to;
                const isDelegated = !!issuer.id && !!requester.id && issuer.id !== requester.id;
                const createdByAuthUser = authUserId === original.created_by;

                if (!issuer) {
                    return <span/>
                }

                return createdByAuthUser ? <Link to={`/people/${issuer.id}`}>
                    <div
                        className={"table-stripe-user " + (isIssuedByMe ? " requested-by-me" : "") + (isDelegated ? " delegated" : "")}/>
                    <span className="user-photo small" data-user={authUser.display_name}> <img
                        src={authUser.image_url} alt=""/></span><span
                    className="user-name">{formatText(original, 'Me')}</span>
                </Link> : <Link to={`/people/${issuer.id}`}>
                    <div
                        className={"table-stripe-user " + (isAssignedToMe ? " assigned-to-me" : "" + (isDelegated ? " delegated" : ""))}/>
                    <span className="user-photo small" data-user={issuer.display_name}> <img
                        src={props.value.image_url} alt=""/></span><span
                    className="user-name">{formatText(original, buildName(issuer))} </span>
                </Link>
            },
        },
        {
            Header: 'Assigned to',
            id: 'assigned_to',
            show: !isStaff || isInMultipleDemandPage,
            accessor: d => buildName(d.receiver),
            className: 'assig-col',
            maxWidth: 200,

            Cell: (props) => {
                const {original} = props;

                if (original.parent && !isInMultipleDemandPage && !isStaff) {
                    const childrenCount = original.children && original.children.length;

                    return <span className="multiple-ass-user"><i
                        className="material-icons">group</i>{formatText(original, (childrenCount || "Multiple") + " assignees")}</span>
                }

                const authUserId = authUser.id;
                const receiver = original.receiver;

                if (!receiver) {
                    return <span> Not assigned yet</span>
                }

                return authUserId === original.receiver.id ? <Link to={'my-page'}>
                <span className="user-photo small" data-user={authUser.display_name}> <img
                    src={authUser.image_url} alt=""/></span><span
                    className="user-name">{formatText(original, 'Me')}</span>
                </Link> : <Link to={`/people/${original.receiver.id}`}>
                <span className="user-photo small" data-user={original.receiver.display_name}> <img
                    src={original.receiver.image_url} alt=""/></span><span
                    className="user-name">{formatText(original, buildName(original.receiver))} </span>
                </Link>
            },
        },
        {
            Header: 'Status',
            accessor: 'timeframe',
            className: 'risk-col',
            Cell: (props) => {
                const {original} = props;
                const status = original.status;

                if (status.toLowerCase() === "ongoing") {

                    switch (props.value) {
                        case 'ON_TIME':
                            return <span className={"label label-success"}>On time</span>;
                        case 'SOON_TO_EXPIRE':
                            return <span className={"label label-warning"}>Soon to expire</span>;
                        case 'EXPIRED':
                            return <span className={"label label-danger"}>Expired</span>;
                        default:
                            return <span className={"label label-default"}>Done</span>;
                    }
                }

                if (status.toLowerCase() === "completed") {
                    return <span className={"label label-default"}>Done</span>;
                }

                if (status.toLowerCase() === "draft") {
                    return <span className={"label label-default label-draft"}>Draft</span>;
                }

                if (status.toLowerCase() === "archived") {
                    return <span className={"label label-default label-draft"}>Archived</span>;
                }

                if (status.toLowerCase() === "deleted") {
                    return <span className={"label label-default label-draft"}>Deleted</span>;
                }

            },
        },

        {
            Header: () => <i className="material-icons header-icon">info_outline</i>,
            id: 'icons',
            className: 'info-col',
            maxWidth: 65,
            headerClassName: 'info-col-header',
            Cell: (props) => {
                const {original} = props;
                const hasActionType = (events = [], type) => events.filter(a => a.type && a.type.toUpperCase() === type.toUpperCase()).length > 0;
                const getCommentInfo = (d) => ({
                    hasComment: hasActionType(d.events, "COMMENT") > 0,
                    hasUnreadComment: !!d.unread && Array.isArray(d.unread.COMMENT) && d.unread.COMMENT.length > 0
                });
                const getChildrenCommentInfo = (d) => {
                    let children = Array.isArray(d.children) ? d.children : [];
                    let infos = children.map(getCommentInfo);
                    infos.push(getCommentInfo(d));
                    return {
                        hasComment: infos.reduce((acc, i) => acc || i.hasComment, false),
                        hasUnreadComment: infos.reduce((acc, i) => acc || i.hasUnreadComment, false),
                    }
                };
                const {hasComment, hasUnreadComment} = getChildrenCommentInfo(original);


                /*
                                 possible types
                                 REOPEN
                                 SUBMIT
                                 APPROVE
                                 REJECT
                                 COMMENT
                                 CREATE
                                 UPDATE
                                 READ

                                 ex: hasActionType(actions, "COMMENT")*/

                const {visibleToOtherAssignees} = original;

                return <span>
                    {visibleToOtherAssignees &&
                    <i className="material-icons" title="This assignment is visible to other assignees">visibility</i>}
                    {hasComment ?
                        hasUnreadComment ?
                            <i className="material-icons unseen" title="This assignment has been commented">chat</i> :
                            <i className="material-icons" title="This assignment has been commented">chat</i>
                        : <span/>}
                    </span>
            },
        },
        {
            Header: 'Title of assignment',
            accessor: 'title',
            className: 'title-col',
            minWidth: 300,
            Cell: (props) => {
                const {original} = props;
                const {visibleToOtherAssignees} = original;
                //const isMultiple = !!original.parent && !isInMultipleDemandPage;
                //const childrenCount = original.children && original.children.length;
                //const childrenReadCount = original.children && original.children.reduce((acc, item) => acc + (!!item.read ? 1 : 0), 0);
                //const childrenCompleted = original.children && original.children.reduce((acc, item) => acc + (item.status === "COMPLETED" ? 1 : 0), 0);
                const submitted = !!original.submitted;
                const hasIterations = (Array.isArray(original.iterations) && original.iterations.length > 0);
                const childrenHaveIteration = (original.parent && Array.isArray(original.children) && !!original.children[0] && Array.isArray(original.children[0].iterations) && original.children[0].iterations.length > 0);

                const childrenSubmittedCount = original.children && original.children.reduce((acc, item) => acc + (!!item.submitted ? 1 : 0), 0);
                const getSubmittedIterationCount = (d) => Array.isArray(d.iterations) ? d.iterations.reduce((acc, item) => acc + (!!item.submitted ? 1 : 0), 0) : 0;
                const childrenSubmittedIterationCount = original.children && original.children.reduce((acc, item) => acc + getSubmittedIterationCount(item), 0);
                const submittedIterationCount = getSubmittedIterationCount(original);
                const isChild = !!original.parent_id;
                //const isAssignedToMe = original && typeof original === "object" ? authUserId === original.assigned_to : false;
                //const isCreatedByMe = original && typeof original === "object" ? authUserId === original.created_by : false;
                //const isAssignedByMe = original && typeof original === "object" ? authUserId === original.assigned_by : false;

                let sub;

                if (submitted || childrenSubmittedCount || submittedIterationCount || childrenSubmittedIterationCount) {
                    let count = childrenSubmittedCount + submittedIterationCount + childrenSubmittedIterationCount;
                    sub = <span
                        className="label label-danger to-be-approved">
                        {count > 1 ? "Submitted (" + String(count) + ")" : "Submitted"}
                        </span>
                }


                return <span>
                    {((isDG || isFollower) || visibleToOtherAssignees) && isChild &&
                    <i className="material-icons filtered-child-demand">keyboard_return</i>}
                    {(hasIterations || childrenHaveIteration) && <i className="material-icons">loop</i>}
                    {sub}
                    <Link
                        to={`/demands/${original.id}`}>{formatText(original, props.value || "No title yet")}</Link></span>
            }
        },
        {
            Header: 'Deadline',
            accessor: 'deadline',
            minWidth: 120,
            maxWidth: 150,
            className: 'text-center deadline-col',
            Cell: (props) => {
                const {original} = props;

                return props.value ?
                    <span>{formatText(original, Moment(original.deadline).format(DATE_FORMAT_TABLE))}</span> : <span/>

            } // No Card
        },
        {
            id: "category",
            Header: 'Category',
            accessor: d => {
                const category = typeof d.category === "object" ? d.category.label : "";
                const subcategory = typeof d.subcategory === "object" ? d.subcategory.label : '';
                return `${category} ${subcategory}`;
            },
            minWidth: 100,
            className: 'category-col',
            // className: "category-pill pull-right", // Card
            Cell: (props) => {
                const {original, value} = props;
                const category = original.category || {};
                const subcategory = original.subcategory || {};
                const extractCode = (string = "") => {
                    const splitted = string.split("-");
                    return splitted[0]
                };

                const categoryElement = !!value && !!category &&
                    <span className="category-name">{formatText(original, extractCode(category.label))} </span>;

                const subcategoryElmenent = !!subcategory.id === "string" &&
                    <span className="category-name">- {formatText(original, subcategory.label)}</span>;

                return <span>{categoryElement} {subcategoryElmenent}</span>;
            },
        },

        {
            Header: 'Creation date',
            accessor: 'created_at',
            minWidth: 120,
            maxWidth: 150,
            className: 'text-center creation-col',
            Cell: (props) => {
                const {original} = props;
                return <span>{formatText(original, Moment(original.created_at).format(DATE_FORMAT_TABLE))}</span>
            }

        },


        /* {
            show: false,
            Header: "Related themes",
            accessor: 'relatedThemes',
            className: "rel-themes-col",
            Cell: props => Array.isArray(props.value) ?
                props.value.map(v => <span className="react-tagsinput-tag" key={v}>{v}</span>) :
                <span >{props.value}</span>
        },*/

    ];
};

class Table extends React.PureComponent {

    render() {
        const {demands = [], authUser, pages, isInMultipleDemandPage, defaultPageSize = 10, onPageChange, onSortedChange, sorted, page} = this.props;

        return (<ReactTable
            showPageSizeOptions={false}
            showPageJump={false}
            //sortable={false}
            manual
            page={page}
            pages={pages}
            defaultPageSize={defaultPageSize}
            minRows={0}
            sorted={sorted}
            className="-highlight"
            data={demands.slice(0, defaultPageSize)}
            columns={getColumns(authUser, isInMultipleDemandPage)}
            onPageChange={onPageChange}
            onSortedChange={onSortedChange}

        />)
    }
}


export default Table;
