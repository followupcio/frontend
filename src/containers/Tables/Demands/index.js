import React, {Component} from 'react';
import {Button} from 'react-bootstrap'
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import DemandTable from './Table';
import FetchError from './FetchError';
import Loading from './Loading';
import {getDemands, getErrorMessage, getIsFetching, getStats} from '../../../selectors/entities/demands';
import {getAllTimeFrames} from '../../../selectors/entities/timeFrames';
import {getAuthUser} from '../../../selectors/authentication'
import {getTimeframeCode} from '../../Commons/Timeframe'
import values from 'lodash/values';
import uniq from 'lodash/uniq';

class DemandsTable extends Component {

    defaultLimit = 10;

    skip = 0;

    fetchData() {

        const {fetchDemands, status, filters = {}, authUser, user, timeFrames} = this.props;

        if (authUser.id) {

            const currentFilters = {...filters, timeFrame: getTimeframeCode(filters.timeFrame, timeFrames)};

            if (JSON.stringify(this.filters) !== JSON.stringify(currentFilters)) {
                this.cancelRequest();
            }

            this.filters = currentFilters;

            this.request = fetchDemands({
                status,
                limit: this.defaultLimit,
                skip: (this.skip * this.defaultLimit),
                filters: this.filters,
                authUser,
                user,
                order: this.order
            });

        }
    }

    componentDidUpdate(prevProps) {
        const {filters, authUser} = this.props;

        const authUserHasChanged = authUser.id !== prevProps.authUser.id;

        const filterHasChanged = JSON.stringify(prevProps.filters) !== JSON.stringify(filters);

        if (authUserHasChanged || filterHasChanged) {
            this.fetchData();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    shouldComponentUpdate(nextProps) {

        const {filters, authUser, demands, stats, isFetching} = this.props;

        if (!nextProps.authUser.id) {
            return false;
        }

        const arraysEqual = (a, b) => {
            if (a === b) return true;
            if (a === null || b === null) return false;
            if (a.length !== b.length) return false;

            for (let i = 0; i < a.length; ++i) {
                if (a[i] !== b[i]) return false;
            }
            return true;
        };

        const demandsHasChanged = !arraysEqual(demands, nextProps.demands);

        const statsHasChanged = stats !== nextProps.stats;

        const authUserHasChanged = authUser.id !== nextProps.authUser.id;

        const filterHasChanged = JSON.stringify(nextProps.filters) !== JSON.stringify(filters);

        const loadingHasChanged = JSON.stringify(nextProps.isFetching) !== JSON.stringify(isFetching);

        return demandsHasChanged || authUserHasChanged || filterHasChanged || statsHasChanged || loadingHasChanged;

    }

    onExport() {
        const {exportDemands, status, filters = {}, authUser, user, timeFrames} = this.props;

        if (authUser.id) {
            exportDemands({
                status,
                filters: {...filters, timeFrame: getTimeframeCode(filters.timeFrame, timeFrames)},
                authUser,
                user
            });
        }
    }

    onPageChange(pageIndex) {
        this.skip = pageIndex;
        this.cancelRequest();
        this.fetchData();
    }

    onSortedChange(newSorted, column, shiftKey) {

        if (Array.isArray(newSorted) && newSorted.length > 0) {
            this.skip = 0;
            this.sorted = newSorted;
            this.order = newSorted[0].id + ' ' + (!!newSorted[0].desc ? "DESC" : "ASC");
            this.cancelRequest();
            this.fetchData();
        }
    }

    render() {
        let {demands, isFetching, authUser, status, stats, filters, errorMessage, /*changeTableSettings*/} = this.props;

        if (isFetching) {
            return (<div className="demandTableContainer">
                <Loading>Loading...</Loading>
            </div>);
        }

        if (errorMessage && !demands.length) {
            return (<div className="demandTableContainer">

                <FetchError message={errorMessage} onRetry={() => this.fetchData()}/>

            </div>);
        }

        const isFiltered = !!values(filters).filter(v => !!v).length;

        demands = demands.filter(d => d.status.toUpperCase() === status.toUpperCase());

        let amount = status && typeof stats[status] === "object" && stats[status].amount;
        let pages;

        if (!!amount) {
            pages = Math.ceil(amount / this.defaultLimit)
        }

        return (<div className="demandTableContainer">

            <Button onClick={this.onExport.bind(this)} bsStyle="default" className="btn-export">
                <i className="material-icons">file_download</i>
            </Button>

            <DemandTable
                authUser={authUser}
                isFiltered={isFiltered}
                status={status}
                page={this.skip}
                pages={pages}
                onPageChange={this.onPageChange.bind(this)}
                onSortedChange={this.onSortedChange.bind(this)}
                sorted={this.sorted}
                demands={!isFiltered ? demands : this.sortParentDemands(demands)}/>
        </div>);
    }

    sortParentDemands(rawDemands) {
        let demands = rawDemands.slice(0);

        let result = [];

        demands = demands.sort(function (x, y) {
            return (x.parent === y.parent) ? 0 : x.parent ? -1 : 1;
        });

        const mapping = {};
        demands.forEach((parent) => {

            if (!!parent.parent) {
                const indexes = [];

                demands.forEach((d, index) => {
                    if (d.parent_id === parent.id) {
                        indexes.push(index)
                    }
                });

                mapping[parent.id] = indexes.slice(0);

            }

        });

        demands.forEach(d => {


            if (!mapping.hasOwnProperty(d.id)) {
                result.push(d);
            } else {

                const is = mapping[d.id];
                result.push(d);

                is.forEach(i => {
                    let toAdd = demands[i];
                    let indexToRemove = result.indexOf(toAdd);

                    if (indexToRemove > -1) {
                        demands.splice(indexToRemove, 1);
                    }

                    result.push(toAdd);

                })
            }

        });

        result = uniq(result);
        result = result.filter(i => !!i);

        return result;
    }

    cancelRequest() {
        if (this.request && typeof this.request.source && typeof this.request.source.cancel === "function") {
            this.request.source.cancel();
        }
    }

    componentWillUnmount() {
        this.cancelRequest();
    }

}

const mapStateToProps = (state, ownProps) => {
    const status = ownProps.status; // completed, ongoing
    const filters = ownProps.filters; // on-time, soon-to-expire
    const authUser = getAuthUser(state);

    return {
        authUser,
        status,
        filters,
        stats: getStats(state),
        timeFrames: getAllTimeFrames(state),
        demands: getDemands(state),
        isFetching: getIsFetching(state),
        errorMessage: getErrorMessage(state),
    };
};

DemandsTable = connect(
    mapStateToProps,
    actions
)(DemandsTable);

export default DemandsTable;
