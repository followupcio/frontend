import React from 'react';
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';

const buildName = (d) => {
    if (!d) {
        return ""
    }
    return `${d.last_name}, ${d.first_name} (${d.division})`;
};


const getColumns = (authUser, onRemove) => {

    if (!authUser.role) {
        return [];
    }

    /*const isDirector = authUser.role.indexOf(ROLES.DIRECTOR) > -1;
     const isDG = authUser.role.indexOf(ROLES.DG) > -1;
     const isFollower = authUser.role.indexOf(ROLES.FOLLOWER) > -1;
     const isStaff = authUser.role.indexOf(ROLES.STAFF) > -1;*/

    return [
        {
            Header: 'ID',
            accessor: 'id',
            show: false

        },
        {
            Header: 'Name',
            id: 'name',
            minWidth: 200,
            //show: !isDirector,
            accessor: d => buildName(d),
            className: 'created-col',
            Cell: (props) => {

                const {value, row} = props;

                return <Link to={`/people/${row.id}`}>
                    <span className="user-photo small" data-user={row.display_name}> <img
                        src={row.image_url} alt=""/></span><span
                    className="user-name">{value}</span>
                </Link>;

            },
        },
        {
            Header: 'Division',
            accessor: "division",
            className: 'created-col text-center',
            maxWidth: 70,
            Cell: (props) => {

                const {value} = props;

                return <span>{value}</span>;

            },
        },
        {
            Header: 'Phone',
            accessor: "phone",
            className: 'created-col',
            Cell: (props) => {

                const {value} = props;
                return <span><a href={"tel:" + value}>{value}</a> </span>;
            },
        },
        {
            Header: 'Remove',
            id: "actions",
            className: 'created-col text-center',
            width: 150,
            Cell: (props) => {
                const {row} = props;

                return <Button onClick={() => onRemove(row.id)}>
                    Remove
                </Button>;

            },
        },
    ];
};

const Table = ({users, authUser, defaultPageSize = 1, onRemove, ...rest}) =>
    (<ReactTable
        data={users}
        defaultPageSize={20}
        minRows={0}
        columns={getColumns(authUser, onRemove)}
        rest
        className="-highlight"
        showPagination
        loading={false}
    />);

export default Table;
