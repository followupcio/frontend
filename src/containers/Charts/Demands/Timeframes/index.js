import React, {Component} from 'react';
import Highcharts from 'highcharts';
import {connect} from 'react-redux';
import {getChartDemandStatus, getHomeOngoingDemandsFilters} from '../../../../selectors/ui/home';
import {fetchTimeFrames, fetchUserStats, setHomeOngoingDemandsFilters} from '../../../../actions/index';
import {getOngoingTimeFrames} from '../../../../selectors/entities/timeFrames';
import {getAuthUser} from '../../../../selectors/authentication';
import {getTimeframeCode, getTimeframeId} from '../../../Commons/Timeframe'
import isEqual from 'lodash/isEqual';

const SERIES_NAME = "Time-frames";

class Charts extends Component {

    fetchData() {
        const {_fetchUserStats, fetchTimeFrames, timeFrames, authUser} = this.props;

        if (!!authUser && authUser.id) {
            _fetchUserStats(authUser);
        }

        if (!timeFrames.length) {
            fetchTimeFrames()
        }
    }

    // chart creation

    createConfig() {

        return {
            chart: {
                type: 'pie',
                margin: [0, 0, 0, 0],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                height: 250,
                backgroundColor: 'rgba(255, 255, 255, 0)',
                animation: {
                    duration: 1000
                },
                style: {
                    fontFamily: 'inherit',
                },
            },

            // remove title and subtitle
            title: {
                text: '',
                style: {
                    display: 'none',
                },
            },
            subtitle: {
                text: '',
                style: {
                    display: 'none',
                },
            },

            plotOptions: {
                pie: {
                    borderColor: '#000000',
                    allowPointSelect: false,
                    center: ['50%', '50%'],
                    borderWidth: 0,
                    // set radius
                    size: '180',
                    // labels inside the pie
                    dataLabels: {
                        enabled: true,
                        useHTML: true,
                        formatter() {
                            return `<div class="pie-label"><span>${this.point.homeLabel}</span> <b>${this.y}%</b><div>`;
                        },
                        style: {
                            color: '#333',
                            textShadow: false,
                            fontWeight: 'normal',
                        },
                        distance: -1,
                    },
                },
            },

            tooltip: {
                backgroundColor: null,
                borderWidth: 0,
                shadow: false,
                useHTML: true,
                style: {
                    padding: 0,
                },

                formatter() {
                    return `${this.key}: <b>${this.y}%</b>`;
                },
            },

            // remove credits
            credits: {
                enabled: false,
            },

            series: [{name: SERIES_NAME, data: this.createData()}],
        };
    }

    onSliceClick(slice) {
        const {timeFrame, filterValues, onClick} = this.props;
        const values = {...filterValues};

        if (timeFrame !== slice) {
            values.timeFrame = slice
        } else {
            delete values.timeFrame;
        }

        onClick(values);
    }

    createData() {

        const {timeFrame, timeFrames, values} = this.props;

        const self = this;
        const config = [
            {
                name: 'Expired',
                timeFrame: 'EXPIRED',
                color: '#d32f2e',
                y: values.expired.percentage,
                homeLabel: values.expired.count,
            },
            {
                name: 'Soon to expire',
                timeFrame: 'SOON_TO_EXPIRE',
                color: '#ff8e01',
                y: values.soonToExpire.percentage,
                homeLabel: values.soonToExpire.count,
            },
            {
                name: 'On time',
                timeFrame: 'ON_TIME',
                color: '#5cb85c',
                y: values.onTime.percentage,
                homeLabel: values.onTime.count,
            },
            {
                name: 'Submitted',
                timeFrame: 'SUBMITTED',
                color: '#6f86d6',
                y: values.submitted.percentage,
                homeLabel: values.submitted.count,
            },
        ]
            .filter(v => v.homeLabel)
            .map((s) => {
                s.sliced = (s.timeFrame === getTimeframeCode(timeFrame, timeFrames));
                s.id = s.timeFrame;
                s.events = {
                    click() {
                        self.onSliceClick(getTimeframeId(this.options.id, timeFrames));
                    },
                };

                return s;
            });

        return config
    }

    animateSlice(sliceId) {

        const {values, timeFrames} = this.props;
        const hasValues = !!values.total;

        if (!hasValues || !this.chart) {
            return;
        }

        const slice = this.chart.get(getTimeframeCode(sliceId, timeFrames));

        if (slice && typeof slice.slice === "function") {
            slice.slice();
        }
    }

    createChart() {
        const {values} = this.props;
        const hasValues = !!values.total;

        if (!hasValues) {
            return;
        }

        // Extend Highcharts with modules
        if (this.props.modules) {
            this.props.modules.forEach((module) => {
                module(Highcharts);
            });
        }

        const id = 'chart-1';
        const chartContainer = document.createElement("DIV");
        chartContainer.id = id;
        document.getElementById("chart-holder").appendChild(chartContainer);

        // Set container which the chart should render to.
        this.chart = new Highcharts[this.props.type || 'Chart'](
            id,
            this.createConfig(),
            this.renderTotal.bind(this)
        );

    }

    shouldComponentUpdate(nextProps) {
        const {timeframes, values} = this.props;

        return !isEqual(timeframes, nextProps.timeframes) ||
            !isEqual(values, nextProps.values);
    }

    renderTotal(chart) {
        const {values, onClick} = this.props;
        const c = chart || this.chart;
        const total = values.total;
        const label = c.renderer.label('<div class="total"><span class="total-title small-title">TOTAL</span>  ' + total + '</div>', 0, 0, 'rect', 0, 0, true, true, 'total').add();

        label.on('click', () => {
                onClick({})
            }
        );

    }

    updateChart() {

        this.destroyChart();
        this.createChart()

    }

    // end chart creation

    render() {

        const {values, timeFrames} = this.props;

        if (!values || !timeFrames.length) {
            return <span>Loading</span>
        }
        const hasValues = !!values.total;

        return (<div id="chart-holder" className="chart-holder">
            {!hasValues && <div className="no-data">No pending assignments</div>}
        </div>);
    }

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate() {
        const {timeFrames, /*timeFrame,*/ values} = this.props;

        if (!values || !timeFrames.length) {
            return;
        }


        const hasValues = values.hasOwnProperty('total');

        if (!hasValues) {
            this.fetchData();
            return;
        }

        if (this.chart) {
            this.updateChart();
        } else {
            this.createChart()
        }

    }


    destroyChart() {
        if (this.chart) {
            this.chart.destroy();
            delete this.chart;
        }
    }

    componentWillUnmount() {
        this.destroyChart()
    }
}

const onClick = (filters) => (dispatch) => {
    dispatch(setHomeOngoingDemandsFilters(filters));
};

const _fetchUserStats = (authUser) => (dispatch, getState) => {
    return fetchUserStats(authUser, dispatch, getState);
};

Charts = connect(
    (state) => ({
        authUser: getAuthUser(state),
        filterValues: getHomeOngoingDemandsFilters(state),
        timeFrame: getHomeOngoingDemandsFilters(state).timeFrame,
        values: getChartDemandStatus(state),
        timeFrames: getOngoingTimeFrames(state)
    }),
    {onClick, _fetchUserStats, fetchTimeFrames}
)(Charts);

export default Charts;
