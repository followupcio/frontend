import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {IndexLinkContainer, LinkContainer} from 'react-router-bootstrap';
import {setStyle} from './actions';
import {getAuthUser} from '../../selectors/authentication';
import {withRouter} from 'react-router-dom';
import {getStyle} from "./../../selectors/ui/app"


class ProtectedMenuItems extends PureComponent {

    // _getRole() {
    //
    //     const {authUser} = this.props;
    //
    //     switch (authUser.role) {
    //         case 'master':
    //             return <span className="user-ico"> DG</span>;
    //         case 'slave':
    //             return <span className="user-ico">Staff</span>;
    //         default:
    //             return <span className="user-ico"> DG</span>;
    //     }
    // }

    render() {
        const {onClick} = this.props;
        const {authUser, style} = this.props;

        // console.log(authUser.image_url);
        return (<Navbar.Collapse>
            <Nav>
                <IndexLinkContainer to="/">
                    <NavItem>Home</NavItem>
                </IndexLinkContainer>

                <LinkContainer to="/search">
                    <NavItem>
                        Search
                    </NavItem>
                </LinkContainer>

                <LinkContainer to="/logout">
                    <NavItem id="logout-btn">
                        Logout
                    </NavItem>
                </LinkContainer>
            </Nav>

            <Nav pullRight className="usr-btn">
                <NavItem>
                    <span className="display-as-text">Display as</span>
                </NavItem>
                <NavItem
                    eventKey={3} onClick={() => onClick('table')}
                    className={"app-style table-style display-icons " + (style === 'table' ? 'active' : '')}
                >
                    <i className="material-icons">grid_on</i>
                </NavItem>
                <NavItem
                    eventKey={4} onClick={() => onClick('cards')}
                    className={"app-style cards-style display-icons " + (style === 'cards' ? 'active' : '')}
                >
                    <i className="material-icons">list</i>
                </NavItem>

                <LinkContainer to="/my-page">
                    <NavItem eventKey={2} className="user-btn-photo">
                        <span className="user-photo logged"> <img src={authUser.image_url} alt=""/></span>
                    </NavItem>
                </LinkContainer>

            </Nav>
        </Navbar.Collapse>);
    }
}

const mapDispatchToState = (dispatch) => ({
    onClick: (style) => {
        dispatch(setStyle(style))
    }
});

const mapStateToProps = (state) => ({
    style: getStyle(state),
    authUser: getAuthUser(state),
});

ProtectedMenuItems = withRouter(connect(mapStateToProps, mapDispatchToState, null, {
    pure: false,
})(ProtectedMenuItems));

export default ProtectedMenuItems;
