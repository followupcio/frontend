import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Navbar} from 'react-bootstrap';
import {Link, withRouter} from 'react-router-dom';
import {getAuthUser} from '../../selectors/authentication';
import PublicMenuItems from './PublicMenuItems'
import ProtectedMenuItems from './RestrictedMenuItems'

class TopMenu extends PureComponent {

    _getRole() {

        switch ("role") {
            case 'master':
                return <span className="user-ico"> DG</span>;
            case 'slave':
                return <span className="user-ico">Staff</span>;
            default:
                return <span className="user-ico"> DG</span>;
        }
    }

    render() {
        const {authUser} = this.props;

        return (<Navbar collapseOnSelect fluid>

            <Navbar.Header>
                <Navbar.Brand>

                    <div className="brand-holder">
                        <Link to="/" className="fu-logo"> Home </Link>
                    </div>
                </Navbar.Brand>
                <Navbar.Toggle/>
            </Navbar.Header>

            {authUser.id ? <ProtectedMenuItems/> : <PublicMenuItems/>}

        </Navbar>);
    }
}


const mapStateToProps = (state) => ({
    authUser: getAuthUser(state) || {},
});

TopMenu = withRouter(connect(mapStateToProps, null, null, {
    pure: false,
})(TopMenu));

export default TopMenu;
