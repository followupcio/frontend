import {SET_STYLE} from './constants';

export const setStyle = (style) => ({
    type: SET_STYLE,
    style,
});
