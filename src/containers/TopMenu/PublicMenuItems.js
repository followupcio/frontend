import React, {PureComponent} from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

class PublicMenuItems extends PureComponent {

    render() {

        return (<Navbar.Collapse>
            <Nav>
                <LinkContainer to="/login">
                    <NavItem>
                        Login
                    </NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>);
    }
}

export default PublicMenuItems;
