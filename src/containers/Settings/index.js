import React, {PureComponent} from 'react';
import {Col, Row} from 'react-bootstrap';
import {connect} from 'react-redux';
import {getUser} from '../../selectors/entities/users.js';
import {getAuthUser} from '../../selectors/authentication.js';
import * as actions from '../../actions/index';
import TrustedList from '../MyStaff/Editor/index'

class SettingPage extends PureComponent {

    componentDidMount() {
        const {fetchUser, userId} = this.props;
        fetchUser(userId);
    }

    render() {
        const {/*isMyPage,*/ authUser, selectedUser} = this.props;

        if (!authUser.role) {
            return <span>Loading</span>
        }

        if (!selectedUser) {
            return <span>User not fond</span>
        }

        return (
            <Row className="settings-row">
                {/*<Col sm={5} className="setting-half-col">*/}
                {/*<UserInfo*/}
                {/*isMyPage={isMyPage}*/}
                {/*user={selectedUser} />*/}
                {/*<h4 className="user-list-title">General Settings</h4>*/}
                {/*<hr/>*/}

                {/*</Col>*/}
                <Col sm={8} smOffset={2} className="setting-half-col">
                    <TrustedList/>
                </Col>

            </Row>
        );
    }

}

const mapStateToProps = (state, params) => {
    const userId = params.params ? params.params.user : params.user;
    return {
        userId,
        authUser: getAuthUser(state),
        selectedUser: getUser(state, userId),
    }
};

SettingPage = connect(mapStateToProps, actions)(SettingPage);

export default SettingPage;
