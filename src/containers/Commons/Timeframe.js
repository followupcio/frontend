export const getTimeframeCode = (id, list) => {
    const timeframeObj = list.find(t => t.id === id) || {};
    return timeframeObj.code;
};

export const getTimeframeId = (code, list) => {
    const timeframeObj = list.find(t => t.code === code) || {};
    return timeframeObj.id;
};