import React from 'react';
import DebounceInput from 'react-debounce-input';
import {SimpleSelect} from 'react-selectize';
import 'react-selectize/dist/index.min.css';
//import 'react-tagsinput/react-tagsinput.css';
import DatePicker from 'react-16-bootstrap-date-picker';

export const renderInput = ({input, meta, placeholder, handleSubmit, onChangeSubmit}) => {

    return <div className="form-group text-input">

        <DebounceInput
            className="form-control"
            {...input}
            placeholder={placeholder}
            debounceTimeout={300}
            onChange={(event) => {
                input.onChange(event.target.value);
                /*
                 call input.onChange before handleSubmit
                 if you are relying on the dirty flag in the update function
                 and pass event.target.value not just event
                 see #2197 (https://github.com/erikras/redux-form/issues/2197
                 - input.value is set to event when value is blank)
                 */
                setTimeout(() => {
                    handleSubmit((values, dispatch, props) => {
                        const newValues = {...values};
                        if (event.target.value === '') {
                            delete newValues[input.name];
                        } else {
                            newValues[input.name] = event.target.value; // ensuring values obj has the latest value
                        }
                        return onChangeSubmit(newValues, dispatch, props);
                    })();
                })
            }}
        />
    </div>;
};


export const renderPassword = ({input, meta, placeholder, handleSubmit, onChangeSubmit}) =>
    <div className="form-group text-input">

        <input
            className="form-control"
            type="password"
            {...input}
            placeholder={placeholder}
            onChange={(event) => {
                input.onChange(event.target.value);
                /*
                 call input.onChange before handleSubmit
                 if you are relying on the dirty flag in the update function
                 and pass event.target.value not just event
                 see #2197 (https://github.com/erikras/redux-form/issues/2197
                 - input.value is set to event when value is blank)
                 */

                setTimeout(() => {
                    handleSubmit((values, dispatch, props) => {
                        const newValues = {...values};
                        if (event.target.value === '') {
                            delete newValues[input.name];
                        } else {
                            newValues[input.name] = event.target.value; // ensuring values obj has the latest value
                        }
                        return onChangeSubmit(newValues, dispatch, props);
                    })();
                })

            }}
        />
    </div>;

export const renderList = ({input, meta, options, disabled, handleSubmit, onChangeSubmit, placeholder}) => {

    return (<div className="form-group">
        <SimpleSelect
            disabled={disabled}
            value={options ? options.find((i) => i.value === input.value) : []}
            onValueChange={(event) => {
                input.onChange(event ? event.value : null);
                setTimeout(() => {
                    handleSubmit((values, dispatch, props) => {
                        const newValues = {...values};
                        if (!event) {
                            delete newValues[input.name]
                        } else {
                            newValues[input.name] = event.value
                        }
                        return onChangeSubmit(newValues, dispatch, props);
                    })();
                }, 10);
            }}
            options={options}
            placeholder={placeholder}
        /></div>)
};

export const renderDatePicker = ({input, handleSubmit, meta, placeholder, className, onChangeSubmit}) =>
    <div className={"form-group " + className}>
        <DatePicker
            showClearButton={true}
            value={input.value}
            onChange={(value) => {

                input.onChange(value);
                /*
                 call input.onChange before handleSubmit
                 if you are relying on the dirty flag in the update function
                 and pass event.target.value not just event
                 see #2197 (https://github.com/erikras/redux-form/issues/2197
                 - input.value is set to event when value is blank)
                 */

                setTimeout(() => {
                    handleSubmit((values, dispatch, props) => {
                        const newValues = {...values};
                        if (value === '') {
                            delete newValues[input.name];
                        } else {
                            newValues[input.name] = value; // ensuring values obj has the latest value
                        }
                        return onChangeSubmit(newValues, dispatch, props);
                    })();
                })

            }}
            dateFormat={"DD/MM/YYYY"}
        />
        {meta.error && meta.touched && <span className="control-label">{meta.error}</span>}
    </div>;


//NEW COMPONENTS
export const InputText = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <input className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;

export const InputPassword = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <input type="password" className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;