import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {renderInput, renderList} from '../../../Commons/RenderComponents';
import {NAME} from "./constants"

class FilterCompletedDemandsForm extends PureComponent {

    _onSubmit(values = {}) {
        const {onSubmit} = this.props;
        onSubmit(values);
    }

    _reset() {
        const {initialize} = this.props;
        initialize(NAME, {});
        this._onSubmit();
    }

    render() {
        const {handleSubmit, timeFrames} = this.props;

        return (
            <form>

                <Field
                    name="q"
                    component={renderInput}
                    handleSubmit={handleSubmit}
                    onChangeSubmit={this._onSubmit.bind(this)}
                    placeholder="Filter text"
                />

                <Field
                    name="timeFrame"
                    component={renderList}
                    options={timeFrames}
                    handleSubmit={handleSubmit}
                    onChangeSubmit={this._onSubmit.bind(this)}
                    placeholder="Any status"
                />

                <button type="button" className="btn btn-default " onClick={this._reset.bind(this)}>
                    Clear
                </button>

            </form>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: ownProps.getCompletedDemandsFilters(state)
});

FilterCompletedDemandsForm = connect(mapStateToProps,)(reduxForm({
    form: NAME,
})(FilterCompletedDemandsForm));

export default FilterCompletedDemandsForm;
