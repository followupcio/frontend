import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import DemandsFilterForm from './Form';
import {getCompletedTimeFrames} from '../../../../selectors/entities/timeFrames';
import {fetchTimeFrames} from "../../../../actions/index"

class FilterCompletedDemands extends PureComponent {

    _onSubmit(filters) {
        const {setCompletedDemandsFilters, onSubmit} = this.props;
        onSubmit(filters, setCompletedDemandsFilters)
    }

    fetchData() {
        const {fetchTimeFrames, timeFrames} = this.props;

        if (!timeFrames.length) {
            fetchTimeFrames();
        }

    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        const {getCompletedDemandsFilters, timeFrames} = this.props;

        return (
            <DemandsFilterForm
                getCompletedDemandsFilters={getCompletedDemandsFilters}
                timeFrames={timeFrames}
                onSubmit={this._onSubmit.bind(this)}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    timeFrames: getCompletedTimeFrames(state)
});

const onSubmit = (filters, setCompletedDemandsFilters) => (dispatch) => {
    dispatch(setCompletedDemandsFilters(filters));
};

FilterCompletedDemands = connect(mapStateToProps, {
    fetchTimeFrames,
    onSubmit
})(FilterCompletedDemands);

export default FilterCompletedDemands;
