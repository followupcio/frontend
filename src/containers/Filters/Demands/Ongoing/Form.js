import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import {renderInput, renderList} from '../../../Commons/RenderComponents';
import {NAME} from "./constants"

class FilterOngoingDemandsForm extends Component {

    _onSubmit(values) {
        const {onSubmit} = this.props;
        onSubmit(values);
    }

    _reset() {
        const {initialize} = this.props;
        initialize(NAME, {});
        this._onSubmit({});
    }

    componentDidUpdate() {
        const {filterValues, change} = this.props;

        if (filterValues) {
            change('timeFrame', filterValues.timeFrame || null);
        }
    }

    shouldComponentUpdate(nextProps) {
        const {filterValues, categories, timeFrames} = this.props;

        return (JSON.stringify(filterValues) !== JSON.stringify(nextProps.filterValues)) ||
            JSON.stringify(nextProps.categories) !== JSON.stringify(categories) ||
            JSON.stringify(nextProps.timeFrames) !== JSON.stringify(timeFrames);
    }

    render() {
        const {handleSubmit, timeFrames, categories} = this.props;

        return (
            <form ref="filterForm">

                <Field
                    name="q"
                    component={renderInput}
                    handleSubmit={handleSubmit}
                    onChangeSubmit={this._onSubmit.bind(this)}
                    placeholder="Filter text"
                />

                <Field
                    name="category"
                    component={renderList}
                    options={categories}
                    handleSubmit={handleSubmit}
                    onChangeSubmit={this._onSubmit.bind(this)}
                    placeholder="Any category"
                />

                <Field
                    name="timeFrame"
                    component={renderList}
                    options={timeFrames}
                    handleSubmit={handleSubmit}
                    onChangeSubmit={this._onSubmit.bind(this)}
                    placeholder="Any time frame"
                />

                <button type="button" className="btn btn-default" onClick={this._reset.bind(this)}>
                    Clear
                </button>

            </form>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: ownProps.getOngoingDemandsFilters(state),
    filterValues: ownProps.getOngoingDemandsFilters(state),
});

FilterOngoingDemandsForm = connect(mapStateToProps)(reduxForm({
    form: NAME,
})(FilterOngoingDemandsForm));

export default FilterOngoingDemandsForm;
