import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import DemandsFilterForm from './Form';
import {getCategories} from '../../../../selectors/entities/categories';
import {getOngoingTimeFrames} from '../../../../selectors/entities/timeFrames';
import {fetchCategories, fetchTimeFrames} from "../../../../actions/index"

class FilterOngoingDemands extends PureComponent {

    _onSubmit(filters) {
        const {setOngoingDemandsFilters, onSubmit} = this.props;
        onSubmit(filters, setOngoingDemandsFilters)
    }

    fetchData() {
        const {fetchCategories, fetchTimeFrames, timeFrames, categories} = this.props;

        if (!categories.length) {
            fetchCategories();
        }

        if (!timeFrames.length) {
            fetchTimeFrames();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        const {getOngoingDemandsFilters, categories, timeFrames} = this.props;

        return (<DemandsFilterForm
            getOngoingDemandsFilters={getOngoingDemandsFilters}
            timeFrames={timeFrames}
            categories={categories}
            onSubmit={this._onSubmit.bind(this)}
        />);
    }
}

const mapStateToProps = (state) => ({
    categories: getCategories(state),
    timeFrames: getOngoingTimeFrames(state)
});

const onSubmit = (filters, setOngoingDemandsFilters) => (dispatch) => {
    dispatch(setOngoingDemandsFilters({...filters}));
};

FilterOngoingDemands = connect(mapStateToProps, {
    onSubmit,
    fetchCategories,
    fetchTimeFrames,
})(FilterOngoingDemands);

export default FilterOngoingDemands;
