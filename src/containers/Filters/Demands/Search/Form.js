import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {renderDatePicker, renderInput, renderList} from '../../../Commons/RenderComponents';
import {getSearchDemandFilters} from '../../../../selectors/ui/search'
import {getCategory} from "../../../../selectors/entities/categories"

class DemandForm extends PureComponent {

    _onSubmit(values = {}) {
        const {onSubmit} = this.props;
        onSubmit(values);
    }

    render() {
        const {category, users, handleSubmit, categories, categoryValue} = this.props;

        let subcategories = category ? category.children : [];

        return (
            <form>
                <div className="free-text">
                    <Field
                        name="q"
                        component={renderInput}
                        handleSubmit={handleSubmit}
                        onChangeSubmit={this._onSubmit.bind(this)}
                        placeholder="Search text"
                    />
                </div>

                <div className="second-line-filters">
                    <div className="filters">
                        {/*<Field
                            name="country"
                            component={renderList}
                            handleSubmit={handleSubmit}
                            options={countries}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="Country"
                        />*/}

                        <Field
                            name="category"
                            component={renderList}
                            handleSubmit={handleSubmit}
                            options={categories}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="Category"
                        />

                        <Field
                            name="subcategory"
                            component={renderList}
                            disabled={!categoryValue}
                            handleSubmit={handleSubmit}
                            options={subcategories}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="Sub-Category"
                        />

                        <Field
                            name="assigned_to"
                            component={renderList}
                            handleSubmit={handleSubmit}
                            options={users ? users.map(u => ({
                                ...u,
                                value: u.id,
                                id: u.id,
                                label: u.display_name || u.last_name
                            })).sort((a, b) => {
                                if (a.display_name < b.display_name) return -1;
                                if (a.display_name > b.display_name) return 1;
                                return 0;
                            }) : []}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="Assignee"
                        />
                    </div>
                    <div className="timeframe">
                        <span className="form-label">From</span>
                        <Field
                            name="from"
                            component={renderDatePicker}
                            handleSubmit={handleSubmit}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="From"
                            className="data-picker-from"
                        />
                        <span className="form-label">To</span>
                        <Field
                            name="to"
                            component={renderDatePicker}
                            handleSubmit={handleSubmit}
                            onChangeSubmit={this._onSubmit.bind(this)}
                            placeholder="To"
                            className="data-picker-to"
                        />
                    </div>

                </div>


            </form>
        );
    }
}

const validate = (values) => {
    const errors = {};

    // if (!values.title) {
    //    errors.title = 'Required'
    // }

    return errors;
};

DemandForm = connect((state) => {
    const categoryValue = formValueSelector('search-demand')(state, 'category');
    return {
        categoryValue,
        initialValues: getSearchDemandFilters(state),
        category: getCategory(state, categoryValue)
    }
})(reduxForm({
    form: 'search-demand',
    validate,
})(DemandForm));

export default DemandForm;
