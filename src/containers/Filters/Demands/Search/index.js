import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import SearchFormRepr from './Form';
import {getTrustedUsers} from '../../../../selectors/entities/trustedUsers';
import {getCategories} from '../../../../selectors/entities/categories';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {getSearchDemandFormValues} from '../../../../selectors/ui/forms';

class SearchDemand extends PureComponent {

    fetchData() {
        const {fetchTrustedUsers, fetchCategories, authUser, categories} = this.props;

        if (!authUser.id) {
            return;
        }

        fetchTrustedUsers();

        if (!categories.length) {
            fetchCategories();
        }

    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        const {onSubmit, user, users, categories} = this.props;

        return (<div className="search-form-container">
            <SearchFormRepr
                onSubmit={onSubmit}
                users={users}
                categories={categories}
                user={user}
            />
        </div>);
    }
}

const mapStateToProps = (state) => ({
    demandsFormValues: getSearchDemandFormValues(state),
    authUser: getAuthUser(state),
    modal: getModal(state),
    users: getTrustedUsers(state),
    categories: getCategories(state),
});

const onSubmit = (values) => (dispatch) => {
    actions.setSearchDemandFilters(values)(dispatch);
};

SearchDemand = connect(
    mapStateToProps,
    {...actions, onSubmit}
)(SearchDemand);

export default SearchDemand;
