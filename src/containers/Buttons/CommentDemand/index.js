import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import {Button} from 'react-bootstrap';

class CommentButton extends PureComponent {

    _onAddClick(e) {
        e.preventDefault();
        const {demand, onAddClick} = this.props;
        onAddClick({demand});
    }

    render() {

        const {disabled} = this.props;

        return (<Button disabled={disabled} onClick={this._onAddClick.bind(this)} bsStyle="default"
                        className="margin-right attachment-btn">
            <i className="material-icons">attach_file</i>
        </Button>);
    }
}

const onAddClick = ({demand}) => (dispatch) => {
    dispatch(actions.showModal({
        type: 'COMMENT_DEMAND',
        props: {
            demand
        },
    }));
};

const dispatchHideModal = () => (dispatch) => {
    dispatch(actions.hideModal());
};

CommentButton = connect(
    null,
    {...actions, onAddClick, dispatchHideModal}
)(CommentButton);

export default CommentButton;
