import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import {Button} from 'react-bootstrap';

class AddDemand extends PureComponent {

    _onAddClick(e) {
        e.preventDefault();
        const {user, onAddClick} = this.props;
        onAddClick({user});
    }

    render() {
        const {title} = this.props;

        return (<Button id='add-demand-btn' onClick={this._onAddClick.bind(this)} bsStyle="primary"
                        className="margin-right">
            {title || 'Add staff assignment'}
        </Button>);
    }
}

const onAddClick = ({user}) => (dispatch) => {

    dispatch(actions.showModal({
        type: 'ADD_DEMAND',
        props: {
            user
        },
    }));
};

const dispatchHideModal = () => (dispatch) => {
    dispatch(actions.hideModal());
};

AddDemand = connect(
    null,
    {...actions, onAddClick, dispatchHideModal}
)(AddDemand);

export default AddDemand;
