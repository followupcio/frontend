/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {Grid} from 'react-bootstrap';
import {connect} from 'react-redux';
import TopMenu from '../TopMenu'
import ModalRoot from '../Modals/ModalRoot'
import {getStyle} from "./../../selectors/ui/app"
import { withUserAgent } from 'react-useragent';
import * as actions from '../../actions'
import {hasAuthUser, hasToken} from '../../selectors/authentication'
import Notification from '../Notifications';
import {Route, Switch, withRouter} from 'react-router-dom';

import HomePage from '../Pages/Home/Loadable';
import MyPage from '../Pages/MyPage/Loadable';
import Demand from '../Pages/Demand/Loadable';
import Search from '../Pages/Search/Loadable';
import Employee from '../Pages/Employee/Loadable';
import Login from '../Pages/Login/Loadable';
import Logout from '../Pages/Logout/Loadable';
import NotFoundPage from '../Pages/NotFound/Loadable';

import restricted from '../Pages/Restricted';


class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    fetchAuthUserIfNeeded() {
        const {fetchAuthUser, hasToken, hasAuthUser} = this.props;

        if (hasToken && !hasAuthUser) {
            fetchAuthUser();
        }

    }

    componentDidUpdate() {
        this.fetchAuthUserIfNeeded();
    }

    componentDidMount() {
        this.fetchAuthUserIfNeeded();
    }

    render() {

        const {style, ua, location} = this.props;
        let pathname = location.pathname.split("/") || [];
        pathname = pathname.filter((i) => !!i);

        return (<Grid
            data-desktop={!ua.mobile}
            data-mobile={ua.mobile}
            data-tablet={ua.tablet}
            data-os={ua.os}
            data-section={Array.isArray(pathname) && pathname.length > 0 ? pathname[0] : ""}
            fluid
            data-style={style}
            data-reactroot
            className="main-app-container">

            <TopMenu/>

            <Switch>
                <Route exact path="/" component={restricted(HomePage)}/>
                <Route path="/my-page" component={restricted(MyPage)}/>
                <Route path="/demands/:id" component={restricted(Demand)}/>
                <Route path="/search" component={restricted(Search)}/>
                <Route path="/people/:id" component={restricted(Employee)}/>
                <Route path="/login" component={Login}/>
                <Route path="/logout" component={Logout}/>
                <Route path="" component={NotFoundPage}/>
            </Switch>

            <ModalRoot/>

            <Notification/>
        </Grid>);
    }

}

const mapStateToProps = (state) => ({
    style: getStyle(state),
    hasToken: hasToken(),
    hasAuthUser: hasAuthUser(state)
});

App = withRouter(withUserAgent(connect(mapStateToProps, actions)(App)));

export default App;
