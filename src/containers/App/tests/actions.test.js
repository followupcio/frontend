import {DEFAULT_LOCALE} from '../constants';

describe('App Actions', () => {
    describe('Default language', () => {
        it('should return "en" ', () => {
            expect(DEFAULT_LOCALE).toEqual('en');
        });
    });
});