import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {TabContent, TabLink, Tabs} from 'react-tabs-redux';
import {Badge} from 'react-bootstrap';
import CompletedFilter from '../../Filters/Demands/Complete/index';
import OngoingFilter from '../../Filters/Demands/Ongoing/index';
import DemandsTable from '../../Tables/Demands/index';
import {getAuthUser} from '../../../selectors/authentication'
import {getStats} from '../../../selectors/entities/demands'
import {getSelectedTab} from '../../../selectors/ui/tabs'
import * as actions from '../../../actions/index';
import * as ROLES from '../../../conf/roles'

class DemandsTabs extends PureComponent {

    fetchData() {

        const {authUser, fetchDemandsStats, user} = this.props;

        if (authUser.id) {
            fetchDemandsStats({authUser, user})
        }

    }

    componentDidUpdate(prevProps) {
        const {authUser, draftAmount} = this.props;
        const authUserHasChanged = authUser.id !== prevProps.authUser.id;
        const amountHasChanged = JSON.stringify(draftAmount) !== JSON.stringify(prevProps.draftAmount);

        if (authUserHasChanged || amountHasChanged) {
            this.fetchData();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    onChangeTab(selectedTab, namespace) {

        const {changeSelectedTab, isHome} = this.props;

        isHome ? changeSelectedTab(selectedTab, "home") : changeSelectedTab(selectedTab, "employee");

        this.fetchData();

    }

    render() {
        const {demandsStats = {}, isHome, homeSelectedTab, employeeSelectedTab, filterByAuthUser, completedDemandsFilters, authUser, getCompletedDemandsFilters, getOngoingDemandsFilters, setOngoingDemandsFilters, setCompletedDemandsFilters, user, ongoingDemandsFilters, filter} = this.props;

        const authUserId = authUser.id;
        const isMyPage = authUserId === user;

        if (!authUserId) {
            return <span/>
        }

        const isStaff = authUser.role.indexOf(ROLES.STAFF) > -1;
        const isDG = authUser.role.indexOf(ROLES.DG) > -1;
        const isFollower = authUser.role.indexOf(ROLES.FOLLOWER) > -1;
        const showDraft = (!user || isMyPage) && !isStaff;
        const showDeleted = (!user || isMyPage) && (isDG || isFollower);
        const showArchived = (!user || isMyPage);

        const draftAmount = demandsStats.draft || {};
        const ongoingAmount = demandsStats.ongoing || {};
        const completedAmount = demandsStats.completed || {};
        const deletedAmount = demandsStats.deleted || {};
        const archivedAmount = demandsStats.archived || {};

        return (

            <Tabs
                renderActiveTabContentOnly={true}
                name="demands"
                handleSelect={this.onChangeTab.bind(this)}
                className="tabs"
                selectedTab={isHome ? homeSelectedTab : employeeSelectedTab}
            >

                <div className="tabs-list">

                    <TabLink to="ongoing">
                        {ongoingAmount.amount}
                        <span> Ongoing </span>
                        {ongoingAmount && (ongoingAmount.new ?
                            <Badge className="badge-draft">{ongoingAmount.new}</Badge> :
                            "")
                        }
                    </TabLink>

                    <TabLink to="completed">
                        {completedAmount.amount}
                        <span> Completed </span>
                        {completedAmount && (completedAmount.new ?
                            <Badge className="badge-draft">{completedAmount.new}</Badge> :
                            "")}
                    </TabLink>

                    {showDraft &&
                    <TabLink to="drafts">
                        {draftAmount.amount}<span> Drafts </span>
                        {draftAmount && (draftAmount.new ?
                            <Badge className="badge-draft">{draftAmount.new}</Badge> :
                            "")
                        }</TabLink>}

                    {showArchived && <TabLink to="archived">
                        {archivedAmount.amount}
                        <span> Archived </span> {archivedAmount && (archivedAmount.new ?
                        <Badge className="badge-draft">{archivedAmount.new}</Badge> :
                        "")
                    }</TabLink>}

                    {showDeleted && <TabLink to="deleted">
                        {deletedAmount.amount}
                        <span> Deleted </span> {deletedAmount && (deletedAmount.new ?
                        <Badge className="badge-draft">{deletedAmount.new}</Badge> :
                        "")
                    }</TabLink>}

                </div>

                <TabContent for="ongoing">
                    <div className="home-filter-container">
                        <OngoingFilter
                            setOngoingDemandsFilters={setOngoingDemandsFilters}
                            getOngoingDemandsFilters={getOngoingDemandsFilters}
                            filter={filter}
                        />
                    </div>
                    <div className="home-demands-container">
                        <DemandsTable
                            filterByAuthUser={filterByAuthUser}
                            status="ongoing"
                            user={user}
                            filters={ongoingDemandsFilters}
                        />
                    </div>
                </TabContent>

                <TabContent for="completed">
                    <div className="home-filter-container">
                        <CompletedFilter
                            setCompletedDemandsFilters={setCompletedDemandsFilters}
                            getCompletedDemandsFilters={getCompletedDemandsFilters}
                        />
                    </div>

                    <div className="home-demands-container">
                        <DemandsTable
                            filterByAuthUser={filterByAuthUser}
                            status="completed"
                            user={user}
                            filters={completedDemandsFilters}
                        />
                    </div>
                </TabContent>

                {showDraft && <TabContent for="drafts">

                    <div className="home-demands-container">
                        <DemandsTable
                            filterByAuthUser={filterByAuthUser}
                            status="draft"
                            user={user}
                        />
                    </div>
                </TabContent>}

                {showArchived && <TabContent for="archived">

                    <div className="home-demands-container">
                        <DemandsTable
                            filterByAuthUser={filterByAuthUser}
                            status="archived"
                        />
                    </div>
                </TabContent>}

                {showDeleted && <TabContent for="deleted">

                    <div className="home-demands-container">
                        <DemandsTable
                            filterByAuthUser={filterByAuthUser}
                            status="deleted"
                        />
                    </div>
                </TabContent>}

            </Tabs>);
    }
}

const mapStateToProps = (state, ownProps) => ({
    completedDemandsFilters: ownProps.getCompletedDemandsFilters(state),
    ongoingDemandsFilters: ownProps.getOngoingDemandsFilters(state),
    filter: ownProps.getOngoingDemandsFilters(state).timeFrame,
    authUser: getAuthUser(state),
    demandsStats: getStats(state),
    homeSelectedTab: getSelectedTab(state, "home"),
    employeeSelectedTab: getSelectedTab(state, "employee"),

});

DemandsTabs = connect(mapStateToProps, actions)(DemandsTabs);

export default DemandsTabs;
