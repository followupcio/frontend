import React, {PureComponent} from 'react';
import * as ROLES from '../../../conf/roles'
import AddDemand from '../../Buttons/AddDemand/index.js';
import {getAuthUser} from '../../../selectors/authentication.js';
import {connect} from 'react-redux';

class UserInfo extends PureComponent {

    render() {
        const {user, isMyPage, authUser} = this.props;

        if (!user || !authUser || !Array.isArray(authUser.role)) {
            return (<div/>);
        }

        const isDirector = authUser.role.indexOf(ROLES.DIRECTOR) > -1;
        const isDG = authUser.role.indexOf(ROLES.DG) > -1;

        return (

            <div className="user-info">
                <div className="user-info-img-container">
                    <div className="user-info-img"><img src={user.image_url} alt=""/></div>
                </div>

                <div>

                    <div className="user-info-text">
                        <h2>{user.last_name}, {user.first_name} ({user.division})</h2>
                        <div className="user-info-text-item">
                            <span className="small-title">Phone</span>
                            <a href={"tel:" + user.phone}>  {user.phone}</a>
                        </div>
                        <div className="user-info-text-item">
                            <span className="small-title">Email</span>
                            {user.email}
                        </div>
                    </div>

                    <div>
                        {(isDirector || isDG) && !isMyPage ? <AddDemand user={user}/> : <span/>}
                    </div>

                </div>

            </div>);
    }
}

const mapStateToProps = (state) => {
    return {
        authUser: getAuthUser(state),
    };
};

UserInfo = connect(mapStateToProps)(UserInfo);

export default UserInfo;
