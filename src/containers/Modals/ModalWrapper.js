import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Modal} from 'react-bootstrap';
import * as actions from '../../actions';

class ModalWrapper extends PureComponent {
    render() {
        const {title, show, children, type, hideModal} = this.props;

        return (<div className="static-modal">
            <Modal animation={false} id='fu-modal-root' type={type} show={show} onHide={hideModal} enforceFocus={false}>
                <Modal.Header closeButton>
                    <Modal.Title>{title}</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    {children}
                </Modal.Body>

                {/* <Modal.Footer /> */}

            </Modal>
        </div>);
    }

    componentDidMount() {

        if (document.getElementById("fu-modal-root")) {
            document.getElementById("fu-modal-root").removeAttribute("tabindex");
        }
    }

    componentDidUpdate() {
        if (document.getElementById("fu-modal-root")) {
            document.getElementById("fu-modal-root").removeAttribute("tabindex");
        }
    }

}

ModalWrapper = connect(
    null,
    actions
)(ModalWrapper);

export default ModalWrapper;
