import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {withRouter} from 'react-router-dom';

class SendDraftDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, demands, router, authUser, onClose} = this.props;
        return onSubmit(values, demand, demands, router, authUser, onClose);
    }

    render() {
        const {onClose, multiple} = this.props;

        return (<Form
            multiple={multiple}
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (values, demand, demands, router, authUser, onClose) => (dispatch) => {

    const payload = {
        type: "activate_draft"
    };

    if (!Array.isArray(demand.assigned_to)) {
        demand.assigned_to = [demand.assigned_to]
    }

    return actions.activateDraftDemand(payload, demand, dispatch)
        .then(onClose)
};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

SendDraftDemandModal = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(SendDraftDemandModal));

export default SendDraftDemandModal;
