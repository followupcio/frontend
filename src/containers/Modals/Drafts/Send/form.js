import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class SendDraftDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, multiple, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                {multiple ?
                    <span>These demands will now be sent to each assignee and they will be visible in the demand list of your the main page</span> :
                    <p>This demand will now be visible in the demand list of your the main page.</p>}

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}> Send</button>
                    </div>
                </div>

            </form>
        );
    }
}

SendDraftDemandForm = connect()(reduxForm({
    form: 'draft-demand',
})(SendDraftDemandForm));

export default SendDraftDemandForm;
