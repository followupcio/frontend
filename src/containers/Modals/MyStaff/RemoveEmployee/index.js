import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {withRouter} from 'react-router-dom';
import {reset} from 'redux-form';

class RemoveTrustedListModal extends PureComponent {

    _onSubmit() {
        const {onSubmit, id} = this.props;
        return onSubmit(id);
    }

    render() {
        const {onClose} = this.props;
        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (id) => (dispatch) => {

    return dispatch(actions.removeTrustedUser(id)).then(() => {
        dispatch(reset('remove-trusted-demand'));
        dispatch(actions.hideModal());
    });
};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

RemoveTrustedListModal = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(RemoveTrustedListModal));

export default RemoveTrustedListModal;
