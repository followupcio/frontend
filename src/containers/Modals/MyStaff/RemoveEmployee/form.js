import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class RemoveTrustedListForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit)}>

                <p>If removed, this person will not be visible in "My Staff" list.</p>

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right btn-group-danger">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}> Remove</button>
                    </div>
                </div>

            </form>
        );
    }
}

RemoveTrustedListForm = connect()(reduxForm({
    form: 'remove-trusted-demand',
})(RemoveTrustedListForm));

export default RemoveTrustedListForm;
