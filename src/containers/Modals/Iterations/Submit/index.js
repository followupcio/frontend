import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class IterationSubmit extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, iteration} = this.props;
        return onSubmit(values, iteration);
    }

    render() {
        const {onClose} = this.props;


        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const createSubmitPayload = (values, iteration, state) => {
    const authUser = getAuthUser(state);
    const payload = {
        type: "submit",
        author_id: authUser.id,
        iteration_id: iteration.id,
        attachments: values.attachments,
        description: values.description
    };

    if (!payload.attachment) {
        delete payload.attachment
    }

    return payload
};

const onSubmit = (values, iteration) => (dispatch, getState) => {
    const payload = createSubmitPayload(values, iteration, getState());
    return actions.submitIteration(payload, iteration, dispatch).then(() => {
        dispatch(reset('submit-iteration'));
        dispatch(actions.hideModal());
    });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

IterationSubmit = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(IterationSubmit);

export default IterationSubmit;
