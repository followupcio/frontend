import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import Dropzone from 'react-dropzone';
import Textarea from 'react-textarea-autosize';

const renderTextarea = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <Textarea className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span>{meta.error}</span>}
    </div>;


const renderDropzoneInput = (field) => {
    const files = field.input.value;

    const getExtensionIcon = (name) => {
        let extension = name.replace(/\s/g, '').slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || 'Unknown';
        extension = extension.toLocaleLowerCase();
        switch (extension) {
            case 'pdf':
                return <span key={name} className="label label-default">PDF</span>;
            case 'png':
                return <span key={name} className="label label-default">PNG</span>;
            default :
                return <span key={name} className="label label-default">{extension}</span>;

        }
    };

    function formatBytes(bytes, decimals) {
        if (bytes === 0) return '0 Bytes';
        let k = 1000,
            dm = decimals + 1 || 3,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    return (
        <div className="form-group">
            <label htmlFor="first_name">{field.placeholder}</label>

            <Dropzone className="attachment-dropzone"
                      name={field.name}
                      maxSize={10485760}
                      onDropRejected={() => alert("Unable to upload the file: allowed file size exceeded (max 10 MB)")}
                      onDrop={(filesToUpload) => {
                          let files = [...field.input.value, ...filesToUpload];
                          files = files.filter((item, pos) => files.indexOf(item) === pos);
                          field.input.onChange(files)
                      }}
            >
                {({getRootProps, getInputProps}) => (
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <div>Drop files here, or click to select files to upload. (Max file size 10 MB)</div>
                    </div>
                )}
            </Dropzone>

            {field.meta.touched && field.meta.error && <span className="error">{field.meta.error}</span>}

            {files && Array.isArray(files) && (
                <ul className="list-unstyled attached-file">

                    {files.map((file, i) => <li key={i}>
                        <button type="button" className="btn btn-default clear-attachment" onClick={() => {
                            let result = [...files];
                            result.splice(i, 1);
                            field.input.onChange(result)
                        }}>
                            <i className="material-icons">close</i>
                        </button>


                        {getExtensionIcon(file.name)} {file.originalName || file.name} <span
                        className="file-size">({formatBytes(file.size)})</span></li>)}
                </ul>
            )}
        </div>
    );
};

const normalizeFiles = (values) => {

    let result = [];

    values.forEach(v => {
        let duplicate = result.find(i => i.name === v.name || i.originalName === v.name);
        if (!duplicate) {
            result.push(v);
        }
    });

    return result

};

class SubmitDemandForm extends PureComponent {

    _onResetClick() {
        const {initialize} = this.props;
        initialize('submit-iteration', {});
    }

    render() {
        const {handleSubmit, onSubmit, onClose, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                <Field
                    name="description"
                    component={renderTextarea}
                    placeholder="Description"
                />

                <Field
                    name="attachments"
                    component={renderDropzoneInput}
                    normalize={normalizeFiles}
                    placeholder="Attachments"
                />

                <hr/>

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-left">
                        <button
                            type="button" className="btn btn-default"
                            onClick={onClose}
                        > Close
                        </button>
                    </div>

                    <div className="btn-group pull-right">
                        <button
                            type="button" className="btn btn-default"
                            onClick={this._onResetClick.bind(this)}
                        > {/* disabled={pristine || submitting}*/} Clear Values
                        </button>
                        <button type="submit" className="btn btn-primary "
                                disabled={submitting}> {/* disabled={pristine || submitting}*/}
                            Submit
                        </button>
                    </div>
                </div>

            </form>
        );
    }
}

SubmitDemandForm = connect()(reduxForm({
    form: 'submit-iteration',
})(SubmitDemandForm));

export default SubmitDemandForm;
