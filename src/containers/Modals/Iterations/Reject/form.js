import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import Textarea from 'react-textarea-autosize';

const renderTextarea = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <Textarea className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span>{meta.error}</span>}
    </div>;


class RejectDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, reset, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                <Field
                    name="description"
                    component={renderTextarea}
                    placeholder="Description"
                />

                <hr/>

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-left">
                        <button
                            type="button" className="btn btn-default"
                            onClick={onClose}
                        > Close
                        </button>
                    </div>

                    <div className="btn-group pull-right">
                        <button
                            type="button" className="btn btn-default"
                            onClick={reset}
                        > Clear Values
                        </button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}>
                            Submit
                        </button>
                    </div>
                </div>

            </form>
        );
    }
}

RejectDemandForm = connect()(reduxForm({
    form: 'approve-iteration',
})(RejectDemandForm));

export default RejectDemandForm;
