import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class IterationReject extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, iteration} = this.props;
        return onSubmit(values, iteration);
    }

    render() {
        const {onClose} = this.props;

        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state),
});

const onSubmit = (values, demand) => (dispatch) => {

    const payload = {
        type: "reject",
        description: values.description
    };

    return actions.rejectIteration(payload, demand, dispatch).then(() => {
        dispatch(reset('reject-iteration'));
        dispatch(actions.hideModal());
    })

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

IterationReject = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(IterationReject);

export default IterationReject;
