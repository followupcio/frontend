import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class IterationApprove extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, iteration, authUser} = this.props;
        return onSubmit(values, iteration, authUser);
    }

    render() {
        const {onClose} = this.props;

        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state),
});

const onSubmit = (values, iteration, authUser) => (dispatch) => {
    const payload = {
        type: "approve",
        description: values.description
    };

    return actions.approveIteration(payload, iteration, dispatch).then(() => {
        dispatch(reset('approve-iteration'));
        dispatch(actions.hideModal());
    });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

IterationApprove = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(IterationApprove);

export default IterationApprove;
