import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';
import {getMostRecentEvent} from '../../../../utils/index'


class SubmitDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand} = this.props;
        return onSubmit(values, demand);
    }

    _onDraftSubmit(values) {
        const {onSaveDraft, demand} = this.props;
        return onSaveDraft(values, demand);
    }

    render() {
        const {onClose, demand} = this.props;

        const initialValues = getMostRecentEvent({
            demand,
            types: ["DRAFT_SUBMIT", "SUBMIT"]
        });

        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
            initialValues={initialValues}
            onSaveDraft={this._onDraftSubmit.bind(this)}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const createSubmitPayload = (values, demand, state) => {
    const authUser = getAuthUser(state);
    const payload = {
        type: "submit",
        author_id: authUser.id,
        demand_id: demand.id,
        attachments: values.attachments,
        description: values.description
    };

    if (!payload.attachment) {
        delete payload.attachment
    }

    return payload
};

const onSubmit = (values, demand) => (dispatch, getState) => {

    const payload = createSubmitPayload(values, demand, getState());

    return actions.submitDemand(payload, demand, dispatch).then(() => {
        dispatch(reset('submit-demand'));
        dispatch(actions.hideModal());
    });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

const onSaveDraft = (values, demand) => (dispatch, getState) => {

    const payload = createSubmitPayload(values, demand, getState());

    return actions.draftSubmitDemand(payload, demand, dispatch).then(() => {
        dispatch(reset('submit-demand'));
        dispatch(actions.hideModal());
    })

};

SubmitDemandModal = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose, onSaveDraft}
)(SubmitDemandModal);

export default SubmitDemandModal;
