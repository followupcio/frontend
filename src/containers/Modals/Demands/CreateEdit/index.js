import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getTrustedUser, getTrustedUsers} from '../../../../selectors/entities/trustedUsers';
import {getUser} from '../../../../selectors/entities/users';
import {getCategories} from '../../../../selectors/entities/categories';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';
import Moment from 'moment'
import {getMostRecentEvent} from '../../../../utils/index'
import Notifications from 'react-notification-system-redux';
import {withRouter} from 'react-router-dom';
import * as ROUTES from '../../../../conf/routes';
import {getStoredDraft} from "../../../../selectors/entities/draft";

class AddDemand extends PureComponent {

    fetchData() {
        const {fetchTrustedUsers, fetchCategories, categories} = this.props;
        fetchTrustedUsers();

        if (!categories.length) {
            fetchCategories();
        }

    }

    componentDidMount() {
        this.fetchData();
    }

    _onSubmit(values) {
        const {onSubmit, demand, editMode} = this.props;
        return onSubmit(values, demand, editMode);
    }

    _onEdit(values) {
        const {onEdit, demand, editMode} = this.props;
        return onEdit(values, demand, editMode);
    }

    _onEditDraft(values) {
        const {onEditDraft, demand} = this.props;
        return onEditDraft(values, demand);
    }

    _onSendDraft(values) {

        const {storedDraft, onSendDraft, unpinDraft} = this.props;

        if (values.id === storedDraft) {
            unpinDraft()
        }

        return onSendDraft(values)
    }

    _onAddStaff(values) {

        const {history, onSaveAsDraft, pinDraft} = this.props;

        return onSaveAsDraft(values)
            .then(demand => {

                history.push(ROUTES.MY_PAGE);
                pinDraft(demand.id || values.id)

            })

    }

    processDemand(d) {

        if (!d) {
            return {};
        }

        if (d.parent) {
            d.assigned_to = d.children.map(c => c.receiver);
        }

        const convertedObj = {
            title: d.title,
            deadline: d.deadline,
            assigned_to: d.assigned_to,
            assigned_by: d.created_by === d.assigned_by ? undefined : d.assigned_by,
            category_id: d.category_id,
            showDelegate: d.assigned_by !== d.created_by,
            subcategory_id: d.subcategory_id,
            description: d.description,
            repeat: d.repeat,
            showRepeat: (Array.isArray(d.monthlyFrequency) && d.monthlyFrequency.length > 0) || (Array.isArray(d.weeklyFrequency) && d.weeklyFrequency.length > 0),
            monthlyFrequency: d.monthlyFrequency,
            weeklyFrequency: d.weeklyFrequency,
            visibleToOtherAssignees: d.visibleToOtherAssignees,
            attachments: getMostRecentEvent({
                demand: d,
                types: ["DRAFT_UPDATE", "DRAFT"]
            }).attachments
        };

        return convertedObj;
    }

    render() {

        const {editMode, demand, user, users, categories, onClose, onSaveAsDraft, sendNotification} = this.props;

        return (<Form
            editMode={editMode}
            initialValues={{
                assigned_to: user,
                category_id: categories.length > 0 ? categories.find(c => c.label.toLowerCase() === "no category").id : "",
                ...this.processDemand(demand),
            }}
            onSubmit={this._onSubmit.bind(this)}
            onEdit={this._onEdit.bind(this)}
            onAddStaff={this._onAddStaff.bind(this)}
            onClose={onClose}
            onSaveAsDraft={onSaveAsDraft}
            onSendDraft={this._onSendDraft.bind(this)}
            sendNotification={sendNotification}
            onEditDraft={this._onEditDraft.bind(this)}
            periods={[
                {value: 'weekly', label: "Weekly"}, {value: 'monthly', label: "Monthly"}
            ]}
            daysOfMonth={[
                {value: '1', label: "1st"}, {value: '2', label: "2nd"}, {value: '3', label: "3rd"},
                {value: '4', label: "4th"}, {value: '5', label: "5th"}, {value: '6', label: "6th"},
                {value: '7', label: "7th"}, {value: '8', label: "8th"}, {value: '9', label: "9th"},
                {value: '10', label: "10th"}, {value: '11', label: "11th"}, {value: '12', label: "12th"},
                {value: '13', label: "13th"}, {value: '14', label: "14th"}, {value: '15', label: "15th"},
                {value: '16', label: "16th"}, {value: '17', label: "17th"}, {value: '18', label: "18th"},
                {value: '19', label: "19th"}, {value: '20', label: "20th"}, {value: '21', label: "21st"},
                {value: '22', label: "22nd"}, {value: '23', label: "23rd"}, {value: '24', label: "24th"},
                {value: '25', label: "25th"}, {value: '26', label: "26th"}, {value: '27', label: "27th"},
                {value: '28', label: "28th"}, {value: 'last', label: "Last day of the month"}
            ]}
            daysOfWeek={[
                {value: "1", label: "M"},
                {value: "2", label: "T"},
                {value: "3", label: "W"},
                {value: "4", label: "T"},
                {value: "5", label: "F"},
            ]}
            users={users}
            categories={categories}
            user={user}
            demand={demand}
        />);
    }
}

const mapStateToProps = (state) => {

    return {
        authUser: getAuthUser(state),
        modal: getModal(state),
        users: getTrustedUsers(state),
        categories: getCategories(state),
        storedDraft: getStoredDraft(state)
    };
};

const createModelFromForm = (values, demand = {}, state) => {

    const authUser = getAuthUser(state);
    const deadline = values.deadline ? Moment(values.deadline).startOf('day').add(17, 'hours').toISOString() : undefined;

    const assigned_to = values.assigned_to
        .map(d => {
            const id = typeof d === "object" ? d.id : d;
            const user = getTrustedUser(state, id) || getUser(state, id);
            return user

        })
        .filter(a => a)
        .map(a => a.id)
        .filter(a => a);

    const model = {
        id: demand.id || values.id,
        title: values.title,
        assigned_to: assigned_to,
        assigned_by: !!values.showDelegate ? (values.assigned_by || demand.assigned_by || authUser.id) : (demand.assigned_by || authUser.id),
        created_by: demand.created_by || authUser.id,
        deadline,
        repeat: values.repeat,
        showRepeat: values.showRepeat,
        category_id: values.category_id,
        subcategory_id: values.subcategory_id ? values.subcategory_id : null,
        attachments: values.attachments,
        description: values.description,
        visibleToOtherAssignees: !!values.visibleToOtherAssignees
    };

    if (demand.parent) {
        model.assigned_to = null;
    }


    //Recurring
    if (!values.showRepeat) {
        model.weeklyFrequency = null;
        model.monthlyFrequency = null;
        model.repeat = null;
    } else {

        model.weeklyFrequency = values.repeat && values.repeat.toUpperCase() === "WEEKLY" && Array.isArray(values.weeklyFrequency) && values.weeklyFrequency.length > 0 ? values.weeklyFrequency : null;
        model.monthlyFrequency = values.repeat && values.repeat.toUpperCase() === "MONTHLY" && Array.isArray(values.monthlyFrequency) && values.monthlyFrequency.length > 0 ? values.monthlyFrequency : null;
    }

    return model
};

const onSubmit = (values, demand) => (dispatch, getState) => {

    const model = createModelFromForm(values, demand, getState());
    const authUser = getAuthUser(getState());

    return actions.addDemand(model, dispatch)
        .then(() => {
            const notificationOpts = {
                title: 'Assignment created successfully.',
                message: 'It will now added to your assignments table.',
                position: 'tr',
                autoDismiss: 10
            };
            dispatch(Notifications.info(notificationOpts));
            actions.fetchDemandsStats({authUser})(dispatch);
            dispatch(reset('demand'));
            dispatch(actions.hideModal());
        });

};

const onEdit = (values, demand) => (dispatch, getState) => {

    const model = createModelFromForm(values, demand, getState());
    const promise = actions.editDemand(model, dispatch)
        .then(() => {
            dispatch(reset('demand'));
            dispatch(actions.hideModal());
        });

    return promise;

};

const onSaveAsDraft = (values) => (dispatch, getState) => {

    const model = createModelFromForm(values, undefined, getState());
    const authUser = getAuthUser(getState());

    model.status = "DRAFT";
    return actions.createDemandDraft(model, dispatch)
        .then((d) => {
            actions.fetchDemandsStats({authUser})(dispatch);
            dispatch(reset('demand'));
            dispatch(actions.hideModal());
            return d;
        });


};

const onSendDraft = (demand) => (dispatch, getState) => {

    const payload = {
        type: "activate_draft"
    };
    const authUser = getAuthUser(getState());

    if (!demand.showDelegate) {
        demand.assigned_by = demand.created_by || authUser.id;
        delete demand.showDelegate;
    }

    return actions.activateDraftDemand(payload, demand, dispatch)
        .then(() => {
            actions.fetchDemandsStats({authUser})(dispatch);
            dispatch(reset('demand'));
            dispatch(actions.hideModal());
        })
};

const onEditDraft = (values, demand) => (dispatch, getState) => {
    const model = createModelFromForm(values, demand, getState());
    model.status = "DRAFT";
    const authUser = getAuthUser(getState());

    if (!values.showDelegate) {
        model.assigned_by = model.created_by || authUser.id;
        delete model.showDelegate;
    }

    return actions.onEditDraft(model, dispatch)
        .then(() => {
            dispatch(reset('demand'));
            dispatch(actions.hideModal());
        });

};

const sendNotification = ({title, message}) => (dispatch) => {
    const notificationOpts = {
        title,
        message,
        position: 'tr',
        autoDismiss: 10
    };
    dispatch(Notifications.info(notificationOpts));
};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

AddDemand = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose, onSaveAsDraft, onSendDraft, onEditDraft, onEdit, sendNotification}
)(AddDemand));

export default AddDemand;
