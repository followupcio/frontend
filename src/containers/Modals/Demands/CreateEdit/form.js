import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Alert} from 'react-bootstrap'
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {MultiSelect, SimpleSelect} from 'react-selectize';
import 'react-selectize/dist/index.min.css';
import DatePicker from 'react-16-bootstrap-date-picker';
//import TagsInput from 'react-tagsinput';
//import 'react-tagsinput/react-tagsinput.css';
import Dropzone from 'react-dropzone';
import Textarea from 'react-textarea-autosize';
import {getCategory} from "../../../../selectors/entities/categories"
import {getUser} from "../../../../selectors/entities/users"

const renderInput = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <input className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;

const renderCheckbox = ({input, meta, id, placeholder, classNames, options}) =>
    <div className={"form-group " + classNames}>
        <input id={id} defaultChecked={input.value} className="form-control" type="checkbox" {...input}
               placeholder={placeholder}/>
        <label htmlFor={id}>{placeholder}</label>
    </div>;

const renderRepeatWeek = ({input, meta, options, disabled, classNames, label}) => {

    const {name, onChange} = input;
    const {touched, error} = meta;
    const inputValue = input.value;

    const checkboxes = options.map(({label, value}, index) => {

        const handleChange = (event) => {
            const arr = [...inputValue];
            if (event.target.checked) {
                arr.push(value);
            } else {
                arr.splice(arr.indexOf(value), 1);
            }
            return onChange(arr);
        };
        const checked = inputValue.includes(value);

        return (
            <li key={`checkbox-${index}`} disabled={!!disabled}>
                <input
                    type="checkbox"
                    disabled={!!disabled}
                    name={`${name}[${index}]`}
                    id={`${name}[${index}]`}
                    className="form-control"
                    onChange={handleChange}
                    value={value}
                    checked={checked}
                />
                <label htmlFor={`${name}[${index}]`}>{label}</label>
            </li>

        );
    });

    return (
        <div className={"form-group " + classNames}>
            <label htmlFor="">{label}</label>
            <ul className="list-unstyled list-inline frequency-weekly">{checkboxes}</ul>
            {touched && error && <p className="error">{error}</p>}
        </div>
    );
};

const renderRepeatMonthly = ({input, meta, placeholder, options, disabled, defaultValues, helper,}) => {

    const rawValues = input.value ? Array.isArray(input.value) ? input.value : [input.value] : [];

    let values = options ? rawValues.map(v => {
        const id = typeof v === "object" ? (v.id || v.value) : v;
        return options.find(o => o.id === id) || options.find(o => o.value.toString() === id.toString()) || helper(id);
    }) : [];

    values = values.filter(v => v);

    return <div
        className="form-group"
    >
        <label htmlFor={input.name}>{placeholder}</label>
        <MultiSelect
            disabled={disabled}
            defaultValues={defaultValues}
            values={values}
            //onValuesChange={(evt) => input.onChange(evt ? evt.value : defaultValue)}
            onValuesChange={(evt) => {
                input.onChange(evt.map(e => e.value))
            }}
            options={options || []}
            placeholder={placeholder}
        />
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;
};

const renderTextarea = ({input, meta, placeholder, options}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <Textarea className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;

const renderList = ({input, meta, placeholder, hideResetButton, hideLabel, options, classNames, disabled, defaultValue}) => {

    return <div
        className={"form-group " + classNames}
    >
        {!hideLabel && <label htmlFor={input.name}>{placeholder}</label>}
        <SimpleSelect
            disabled={disabled}
            defaultValue={defaultValue}
            hideResetButton={!!hideResetButton}
            value={options ? options.find((i) => i.value === input.value) : null}
            onValueChange={(evt) => input.onChange(evt ? evt.value : defaultValue ? defaultValue : "")}
            options={options}
            placeholder={placeholder}
        />
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;
};

const renderMultipleList = ({input, meta, placeholder, options, disabled, defaultValues, helper,}) => {

    const rawValues = input.value ? Array.isArray(input.value) ? input.value : [input.value] : [];
    let values = options ? rawValues.map(v => {
        const id = typeof v === "object" ? (v.id || v.value) : v;
        return options.find(o => o.id === id) || options.find(o => o.value.toString() === id.toString()) || helper(id);
    }) : [];

    values = values.filter(v => v);
    if (Array.isArray(values) && values.length > 0 && values[0].display_name) {
        values = values.map(u => ({...u, key: u.id, value: u.id, label: u.display_name}));
    }

    return <div
        className="form-group"
    >
        <label htmlFor={input.name}>{placeholder}</label>
        <MultiSelect
            disabled={disabled}
            defaultValues={defaultValues}
            values={values}
            //onValuesChange={(evt) => input.onChange(evt ? evt.value : defaultValue)}
            onValuesChange={(evt) => input.onChange(evt)}
            options={options || []}
            placeholder={placeholder}
        />
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;
};

const renderDatePicker = ({input, meta, placeholder, disabled}) =>
    <div className="form-group">
        <label htmlFor="first_name">{placeholder}</label>
        <br/>
        <DatePicker
            showClearButton={false}
            value={input.value}
            disabled={disabled}
            onChange={input.onChange}
            dateFormat={"DD/MM/YYYY"}
        />
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;

/*const renderTags = ({input, meta, placeholder}) =>
 <div className="form-group has-error">
 <label htmlFor="first_name">{placeholder}</label>
 <br />
 <TagsInput
 onlyUnique
 inputProps={{placeholder}}
 value={input.value || []}
 onChange={input.onChange}
 />
 {meta.error && meta.touched && <span className="control-label">{meta.error}</span>}
 </div>;*/

const normalizeFiles = (values) => {

    let result = [];

    values.forEach(v => {
        let duplicate = result.find(i => i.name === v.name || i.originalName === v.name);
        if (!duplicate) {
            result.push(v);
        }
    });

    return result

};

const renderDropzoneInput = (field) => {
    let files = field.input.value;

    const getExtensionIcon = (name) => {
        let extension = name.replace(/\s/g, '').slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || 'Unknown';
        extension = extension.toLocaleLowerCase();
        switch (extension) {
            case 'pdf':
                return <span key={name} className="label label-default">PDF</span>;
            case 'png':
                return <span key={name} className="label label-default">PNG</span>;
            default :
                return <span key={name} className="label label-default">{extension}</span>;

        }
    };

    function formatBytes(bytes, decimals) {
        if (bytes === 0) return '0 Bytes';
        let k = 1000,
            dm = decimals + 1 || 3,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    return (
        <div className="form-group">
            <label htmlFor="first_name">{field.placeholder}</label>

            <Dropzone className="attachment-dropzone"
                      name={field.name}
                      maxSize={10485760}
                      onDropRejected={() => alert("Unable to upload the file: allowed file size exceeded (max 10 MB)")}
                      onDrop={(filesToUpload) => {
                          let files = [...field.input.value, ...filesToUpload];
                          files = files.filter((item, pos) => files.indexOf(item) === pos);
                          field.input.onChange(files)
                      }}
            >

                {({getRootProps, getInputProps}) => (
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <div>Drop files here, or click to select files to upload. (Max file size 10 MB)</div>
                    </div>
                )}


            </Dropzone>

            {field.meta.touched && field.meta.error && <span className="error">{field.meta.error}</span>}

            {files && Array.isArray(files) && (
                <ul className="list-unstyled attached-file">

                    {files.map((file, i) => <li key={i}>
                        <button type="button" className="btn btn-default clear-attachment" onClick={() => {
                            let result = [...files];
                            result.splice(i, 1);
                            field.input.onChange(result)
                        }}>
                            <i className="material-icons">close</i>
                        </button>


                        {getExtensionIcon(file.name)} {file.name} <span
                        className="file-size">({formatBytes(file.size)})</span></li>)}
                </ul>
            )}
        </div>
    );
};

class DemandForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            disabledButtons: false
        };
    }

    shouldComponentUpdate(nextProps) {
        return Array.isArray(nextProps.users) && nextProps.users.length > 0;
    }

    _getDraftValues() {
        let {draftValues, users, demand = {}} = this.props;

        draftValues = this._normalizeValues(draftValues);

        if (!Array.isArray(draftValues.assigned_to)) {
            draftValues.assigned_to = [];
        }

        draftValues.assigned_to = draftValues.assigned_to.map(a => users.find(u => a.id ? u.id === a.id : u.id === a));
        draftValues.id = demand.id;

        return draftValues;

    }

    _onSaveDraft() {
        const {onSaveAsDraft} = this.props;

        const promise = onSaveAsDraft(this._getDraftValues());
        return this._wrapToDisableButtonsOnExecution(promise);
    }

    _onEditDraft() {
        const {onEditDraft} = this.props;

        const promise = onEditDraft(this._getDraftValues());
        return this._wrapToDisableButtonsOnExecution(promise);
    }

    _onSubmitDraft() {
        const {onSendDraft, demand} = this.props;

        const promise = onSendDraft({...this._getDraftValues(), status: "ONGOING", id: demand.id});
        return this._wrapToDisableButtonsOnExecution(promise);
    }

    _onAddStaff() {

        const {onAddStaff} = this.props;
        let promise = onAddStaff(this._getDraftValues());

        return this._wrapToDisableButtonsOnExecution(promise);
    }

    _onEdit(values) {

        const {onEdit} = this.props;

        let promise = onEdit(this._normalizeValues(values));

        return this._wrapToDisableButtonsOnExecution(promise);
    }

    _onSubmit(values) {

        const {onSubmit} = this.props;

        return onSubmit(this._normalizeValues(values));
    }

    _wrapToDisableButtonsOnExecution(promise) {

        this.setState({
            disabledButtons: true
        });

        return promise.then(() => {

            if (this._ismounted) {
                this.setState({
                    disabledButtons: false
                });
            }
        })
    }

    _onReset() {
        const {initialize} = this.props;
        initialize('demands', {});
    }

    _getDemandInfo(demand) {
        return {
            isNewDemand: !demand.id,
            isDraft: demand.status && demand.status.toUpperCase() === "DRAFT",
            isOngoing: demand.status && demand.status.toUpperCase() === "ONGOING",
        }
    }

    _normalizeValues(values) {

        if (values.assigned_to && !Array.isArray(values.assigned_to)) {
            values.assigned_to = [values.assigned_to]
        }

        return {...values}

    }

    render() {

        const {editMode, showDelegate, repeat, daysOfWeek, category, handleSubmit, users, showRepeat, periods, daysOfMonth, onClose, categories, categoryValue, demand = {}, state, /*initialValues,*/ submitting, draftValues} = this.props;
        const {isNewDemand, isDraft, isOngoing} = this._getDemandInfo(demand);
        const subcategories = category ? category.children : [];
        const isWeeklyRepeat = repeat === "weekly";
        const isMonthlyRepeat = repeat === "monthly";
        const isMultipleDemand = Array.isArray(draftValues.assigned_to) && draftValues.assigned_to.length > 1;

        const {disabledButtons} = this.state;

        return (
            <form onSubmit={handleSubmit(this._onSubmit.bind(this))} id="create-edit-form">

                <Field
                    name="title"
                    component={renderInput}
                    placeholder="Title"
                />

                <Field
                    name="description"
                    component={renderTextarea}
                    placeholder="Description"
                />

                <div className="assigned-to-row">
                    <Field
                        name="assigned_to"
                        component={renderMultipleList}
                        options={users && users.map(u => ({...u, key: u.id, value: u.id, label: u.display_name}))}
                        disabled={isOngoing && editMode}
                        placeholder="Assigned to"
                        helper={id => {
                            return getUser(state, id)
                        }}
                    />

                    <button
                        type="button"
                        className="btn btn-default"
                        disabled={(submitting || (isOngoing && editMode))}
                        onClick={this._onAddStaff.bind(this)}> Add staff
                    </button>

                </div>

                {isMultipleDemand && <Field
                    name="visibleToOtherAssignees"
                    placeholder="Make assignment visible between assignees"
                    id="multi-assignee-visibility-checkbox"
                    component={renderCheckbox}
                />}

                {!(isOngoing && editMode) && <Field
                    name="showDelegate"
                    placeholder="Add a follower-up"
                    id="delegate-checkbox"
                    component={renderCheckbox}
                />}

                {(!!showDelegate || (isOngoing && editMode)) && <Field
                    name="assigned_by"
                    component={renderList}
                    disabled={(isOngoing && editMode)}
                    options={users && users.map(u => ({...u, key: u.id, value: u.id, label: u.display_name}))}
                    placeholder="Follower-up"
                    helper={id => {
                        return getUser(state, id)
                    }}
                />}

                <hr/>

                <div className="deadline-group">

                    <Field
                        name="deadline"
                        component={renderDatePicker}
                        placeholder={showRepeat ? "Final deadline" : "Deadline"}
                        disabled={(showRepeat && editMode)}
                    />

                    {!(isOngoing && editMode) && <Field
                        name="showRepeat"
                        placeholder="Repeat"
                        classNames="recurring-check"
                        id="repeat-checkbox"
                        disabled={editMode}
                        component={renderCheckbox}
                    />}

                    {(showRepeat || (isOngoing && editMode && !!showRepeat)) && <Field
                        name="repeat"
                        component={renderList}
                        classNames="period-dropdown"
                        hideLabel={true}
                        hideResetButton={true}
                        disabled={editMode}
                        options={periods}
                        tether={false}
                        placeholder="Repeat"
                    />}

                    {(showRepeat || (isOngoing && editMode && !!showRepeat)) && isWeeklyRepeat && <Field
                        name="weeklyFrequency"
                        options={daysOfWeek}
                        disabled={editMode}
                        component={renderRepeatWeek}
                        classNames="week-dropdown"
                        label="On"
                    />}

                    {(showRepeat || (isOngoing && editMode && !!showRepeat)) && isMonthlyRepeat && <Field
                        name="monthlyFrequency"
                        component={renderRepeatMonthly}
                        classNames="month-dropdown"
                        disabled={editMode}
                        options={daysOfMonth}
                        tether={false}
                        placeholder="On"
                    />}

                </div>

                {(editMode && showRepeat) &&
                <Alert bsStyle="warning">Kindly note that the deadline editing is disabled for assignment with recurring
                    deliveries.</Alert>}

                <div className="category-group">

                    <Field
                        name="category_id"
                        component={renderList}
                        options={categories}
                        tether={false}
                        placeholder="Category"
                    />

                    <Field
                        name="subcategory_id"
                        component={renderList}
                        disabled={!categoryValue}
                        options={subcategories}
                        tether={false}
                        placeholder="Sub-Category"
                    />

                </div>

                {!(isOngoing && editMode) && <Field
                    name="attachments"
                    component={renderDropzoneInput}
                    normalize={normalizeFiles}
                    placeholder="Attachments"
                />}

                {/*  <Field name="relatedThemes"
                 component={renderTags}
                 placeholder="Related themes"/>*/}

                <hr/>

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-left">
                        <button
                            type="button" className="btn btn-default"
                            onClick={onClose}> Close
                        </button>

                        {isNewDemand &&
                        <button onClick={this._onSaveDraft.bind(this)} type="button"
                                disabled={(submitting || disabledButtons)} className="btn btn-default ">
                            Save as draft
                        </button>}

                        {isDraft && editMode &&
                        <button onClick={this._onEditDraft.bind(this)} type="button"
                                disabled={(submitting || disabledButtons)} className="btn btn-default ">
                            Save draft
                        </button>}

                    </div>

                    <div className="btn-group pull-right">
                        {!editMode && <button
                            type="button" className="btn btn-default"
                            onClick={this._onReset.bind(this)}>
                            Clear Values
                        </button>}

                        {!editMode &&
                        <button type="submit" id="submit-demand-button" disabled={(submitting || disabledButtons)}
                                className="btn btn-primary">
                            Submit
                        </button>}

                        {editMode && isOngoing &&
                        <button type="button"
                                disabled={(submitting || disabledButtons)}
                                onClick={handleSubmit(this._onEdit.bind(this))}
                                className="btn btn-primary ">
                            Save
                        </button>}

                        {isDraft &&
                        <button
                            onClick={handleSubmit(this._onSubmitDraft.bind(this))}
                            type="button"
                            disabled={(submitting || disabledButtons)}
                            className="btn btn-primary">
                            Submit
                        </button>}
                    </div>
                </div>

            </form>
        );
    }

    componentDidMount() {
        this._ismounted = true;
    }

    componentWillUnmount() {
        this._ismounted = false;
    }
}

const mapPropToState = (state) => {

    const categoryValue = formValueSelector('demands')(state, 'category_id');

    return {
        state: state,
        categoryValue,
        draftValues: formValueSelector('demands')(state, 'title', 'repeat', 'visibleToOtherAssignees', 'weeklyFrequency', 'showRepeat', 'monthlyFrequency', 'description', 'showDelegate', 'assigned_to', 'assigned_by', 'deadline', 'category_id', 'subcategory_id', 'attachments'),
        showDelegate: formValueSelector('demands')(state, 'showDelegate'),
        showRepeat: formValueSelector('demands')(state, 'showRepeat'),
        repeat: formValueSelector('demands')(state, 'repeat'),
        category: getCategory(state, categoryValue)
    }
};

export const validate = (values) => {
    const errors = {};

    if (!values.title) {
        errors.title = 'Required'
    }

    if (!values.assigned_to || ((Array.isArray(values.assigned_to)) && values.assigned_to.length === 0)) {
        errors.assigned_to = 'Required'
    }

    if (!values.deadline) {
        errors.deadline = 'Required'
    }

    if (!values.category) {
        errors.category = 'Required'
    }

    /*  if (!values.subcategory) {
     errors.subcategory = 'Required'
     }*/

    return errors;
};

DemandForm = connect(mapPropToState)(reduxForm({
    form: 'demands',
    validate
})(DemandForm));

export default DemandForm;
