import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class ApproveDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, authUser} = this.props;
        return onSubmit(values, demand, authUser);
    }

    render() {
        const {onClose} = this.props;

        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state),
});

const onSubmit = (values, demand, authUser) => (dispatch) => {
    const payload = {
        type: "approve",
        description: values.description
    };

    return actions.approveDemand(payload, demand, dispatch).then(() => {
        dispatch(reset('approve-demand'));
        dispatch(actions.hideModal());
        actions.fetchDemandsStats({authUser})(dispatch);
    });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

ApproveDemandModal = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(ApproveDemandModal);

export default ApproveDemandModal;
