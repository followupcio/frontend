import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class RejectDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand} = this.props;
        return onSubmit(values, demand);
    }

    render() {
        const {onClose} = this.props;

        const onSubmit = this._onSubmit.bind(this);

        return (<Form
            onSubmit={onSubmit}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state),
});

const onSubmit = (values, demand) => (dispatch) => {

    const payload = {
        type: "reject",
        description: values.description
    };

    const promise = actions.rejectDemand(payload, demand, dispatch).then(() => {
        dispatch(reset('reject-demand'));
        dispatch(actions.hideModal());
    });

    return promise

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

RejectDemandModal = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(RejectDemandModal);

export default RejectDemandModal;
