import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class UndeleteDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, multiple, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                {multiple ?
                    <span>These demands will now be visible in the demand list and you will be redirected to the main page.</span> :
                    <p>This demand will now be visible in the demand list and you will be redirected to the main
                        page.</p>}

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right btn-group">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}> Restore</button>
                    </div>
                </div>

            </form>
        );
    }
}

UndeleteDemandForm = connect()(reduxForm({
    form: 'undelete-demand',
})(UndeleteDemandForm));

export default UndeleteDemandForm;
