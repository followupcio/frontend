import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';
import {withRouter} from 'react-router-dom';
import * as ROUTES from '../../../../conf/routes'

class DeletePermanentlyDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, demands, history} = this.props;
        return onSubmit(values, demand, demands, history);
    }

    render() {
        const {onClose, multiple} = this.props;

        return (<Form
            multiple={multiple}
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (values, demand, demands, history) => (dispatch) => {

    const payload = {
        type: "delete",
    };

    if (demand) {

        return actions.deletePermanentlyDemand(payload, demand, dispatch)
            .then(() => {
                dispatch(reset('delete-demand'));
                dispatch(actions.hideModal());
                history.push(ROUTES.HOME);
            })

    }

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

DeletePermanentlyDemandModal = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(DeletePermanentlyDemandModal));

export default DeletePermanentlyDemandModal;
