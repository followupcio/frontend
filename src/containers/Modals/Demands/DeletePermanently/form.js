import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class DeletePermanentlyDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, multiple, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                {multiple ?
                    <p>These demands will be removed permanently from the system.</p> :
                    <p>This demand will will be removed permanently from the system.</p>}

                <p><strong>Kindly note that the operation cannot be reversed.</strong></p>

                <hr/>
                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right btn-group-danger">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}> Delete permanently
                        </button>
                    </div>
                </div>


            </form>
        );
    }
}

DeletePermanentlyDemandForm = connect()(reduxForm({
    form: 'delete-demand',
})(DeletePermanentlyDemandForm));

export default DeletePermanentlyDemandForm;
