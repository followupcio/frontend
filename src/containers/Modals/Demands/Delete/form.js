import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class DeleteDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, multiple, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                {multiple ?
                    <span>These demands will not be visible in the demand list and you will be redirected to the main page.</span> :
                    <p>This demand will not be visible in the demand list and you will be redirected to the main
                        page.</p>}


                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right btn-group-danger">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit" className="btn btn-primary " disabled={submitting}> Delete</button>
                    </div>
                </div>


            </form>
        );
    }
}

DeleteDemandForm = connect()(reduxForm({
    form: 'delete-demand',
})(DeleteDemandForm));

export default DeleteDemandForm;
