import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';
import {withRouter} from 'react-router-dom';
import * as ROUTES from '../../../../conf/routes'

class ArchiveDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, demands, history} = this.props;
        return onSubmit(values, demand, demands, history);
    }

    render() {
        const {onClose, multiple} = this.props;

        return (<Form
            multiple={multiple}
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (values, demand, demands, history) => (dispatch) => {

    if (demand) {

        const payload = {
            type: "archive",
        };

        return actions.archiveDemand(payload, demand, dispatch).then(() => {
            dispatch(reset('archive-demand'));
            dispatch(actions.hideModal());
            history.push(ROUTES.HOME);
        })

    }

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

ArchiveDemandModal = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(ArchiveDemandModal));

export default ArchiveDemandModal;
