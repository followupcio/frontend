import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class ArchiveDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, multiple, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit.bind(this))}>

                {multiple ?
                    <span>These demands will now be visible in the "Archived" tab in the main page.</span> :
                    <p>This demand will will now be visible in the "Archived" tab in the main page.</p>}

                <hr/>
                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right">
                        <button type="button" className="btn btn-default" onClick={onClose}> Cancel</button>
                        <button type="submit"
                                className="btn btn-primary "
                                id="archive-demand-submit-btn"
                                disabled={submitting}> Archive
                        </button>
                    </div>
                </div>


            </form>
        );
    }
}

ArchiveDemandForm = connect()(reduxForm({
    form: 'archive-demand',
})(ArchiveDemandForm));

export default ArchiveDemandForm;
