import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {reset} from 'redux-form';

class CommentDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, authUser} = this.props;
        if (!values || (!values.description && !values.attachments)) {
            return
        }

        return onSubmit(values, demand, authUser);
    }

    render() {
        const {onClose, demand, event, draft} = this.props;

        return (<Form
            demand={demand}
            initialValues={event}
            draft={draft}
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (event, demand, authUser) => (dispatch) => {
    return actions.editEvent({event, demand, authUser})(dispatch).then(() => {
        dispatch(actions.hideModal());
        dispatch(reset('edit-comment-demand'));
    })


};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

CommentDemandModal = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(CommentDemandModal);

export default CommentDemandModal;
