import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm} from 'redux-form';
import Dropzone from 'react-dropzone';
import Textarea from 'react-textarea-autosize';

const renderTextarea = ({input, id, meta, placeholder, disabled, options}) =>
    <div className="form-group">
        <Textarea id={id} disabled={disabled} className="form-control" {...input} placeholder={placeholder}/>
        {meta.error && meta.touched && <span className="error-message">{meta.error}</span>}
    </div>;


const renderDropzoneInput = (field) => {

    return (
        <div className="form-group attachments-holder">
            <Dropzone className="attachment-dropzone"
                      name={field.name}
                      maxSize={10485760}
                      disableClick={field.disabled}
                      onDropRejected={() => alert("Unable to upload the file: allowed file size exceeded (max 10 MB)")}
                      onDrop={(filesToUpload) => {
                          let files = [...field.input.value, ...filesToUpload];
                          files = files.filter((item, pos) => files.indexOf(item) === pos);
                          field.input.onChange(files)
                      }}
            >
                {({getRootProps, getInputProps}) => (
                    <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <button type="button" className="attachment-btn btn btn-default">
                            <i className="material-icons">attach_file</i>
                        </button>
                    </div>
                )}
              
            </Dropzone>

            {field.meta.touched && field.meta.error && <span className="error">{field.meta.error}</span>}

        </div>
    );
};


const renderAttachments = (field) => {
    const files = field.input.value;

    const getExtensionIcon = (name) => {
        let extension = name.replace(/\s/g, '').slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || 'Unknown';
        extension = extension.toLocaleLowerCase();
        switch (extension) {
            case 'pdf':
                return <span key={name} className="label label-default">PDF</span>;
            case 'png':
                return <span key={name} className="label label-default">PNG</span>;
            default :
                return <span key={name} className="label label-default">{extension}</span>;

        }
    };

    function formatBytes(bytes, decimals) {
        if (bytes === 0) return '0 Bytes';
        let k = 1000,
            dm = decimals + 1 || 3,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    return (
        <div className="form-group">
            {files && Array.isArray(files) && (
                <ul className="list-unstyled attached-file comment-attachment">

                    {files.map((file, i) => <li key={i}>
                        <button type="button" className="btn btn-default clear-attachment" onClick={() => {
                            let result = [...files];
                            result.splice(i, 1);
                            field.input.onChange(result)
                        }}>
                            <i className="material-icons">close</i>
                        </button>
                        {getExtensionIcon(file.name)} {file.name} <span
                        className="file-size">({formatBytes(file.size)})</span></li>)}
                </ul>
            )}
        </div>
    );
};

const normalizeFiles = (values) => {

    let result = [];

    values.forEach(v => {
        let duplicate = result.find(i => i.name === v.name || i.originalName === v.name);
        if (!duplicate) {
            result.push(v);
        }
    });

    return result

};

class CommentDemandForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, draft, /*onClose, reset,*/ demand, submitting} = this.props;

        return (
            <form className="comment-form" onSubmit={handleSubmit((values) => onSubmit(values, demand))}>

                <div className="form-group-holder">
                    <Field
                        name="description"
                        disabled={draft}
                        id="comment-edit-textarea"
                        component={renderTextarea}
                        placeholder="Your comment"
                    />

                    <Field
                        name="attachments"
                        disabled={draft}
                        component={renderAttachments}
                    />

                </div>

                <Field
                    name="attachments"
                    disabled={draft}
                    normalize={normalizeFiles}
                    component={renderDropzoneInput}
                    placeholder="Attachments"

                />

                <div className="btn-confirm-group">
                    <div className="btn-group">
                        <button type="submit"
                                id="comment-edit-submit-btn"
                                disabled={draft || submitting}
                                className="btn btn-default ">
                            Save
                        </button>
                    </div>
                </div>

            </form>


        );
    }
}


const validate = (values) => {
    const errors = {};

    /*   if (!values.description) {
           errors.description = 'Required'
       }*/

    return errors;
};

CommentDemandForm = connect()(reduxForm({
    form: 'edit-comment-demand',
    validate
})(CommentDemandForm));

export default CommentDemandForm;
