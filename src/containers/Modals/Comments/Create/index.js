import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {initialize} from 'redux-form';
import Notifications from 'react-notification-system-redux';

class CommentDemandModal extends PureComponent {

    _onSubmit(values) {
        const {onSubmit, demand, authUser} = this.props;
        if (!values || (!values.description && !values.attachments)) {
            return
        }

        return onSubmit(values, demand, authUser);
    }

    render() {
        const {onClose, demand, /*event,*/ draft} = this.props;

        return (<Form
            demand={demand}
            draft={draft}
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = (values, demand = {}, authUser) => (dispatch) => {


    const events = demand.events || [];
    const myComments = events.filter(e => e.type === "COMMENT" && e.author_id === authUser.id) || [];
    const isFistComment = myComments.length === 0 && authUser.id === demand.assigned_to;

    if (isFistComment && demand.status && demand.status.toUpperCase() === "ONGOING") {

        const notificationOpts = {
            title: 'Comment created successfully.',
            message: 'Kindly note that to complete the task, you should "submit for review" your work with the correspondent button.',
            position: 'tr',
            autoDismiss: 10
        };
        dispatch(Notifications.info(notificationOpts));

    }

    const payload = {
        type: "comment",
        attachments: values.attachments,
        description: values.description
    };

    return actions.addComment(payload, demand, dispatch)
        .then(() => {
            dispatch(initialize('comment-demand', {}));
            dispatch(actions.hideModal());
        });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

CommentDemandModal = connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(CommentDemandModal);

export default CommentDemandModal;
