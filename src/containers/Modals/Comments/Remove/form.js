import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';

class RemoveTrustedListForm extends PureComponent {

    render() {
        const {handleSubmit, onSubmit, onClose, submitting} = this.props;

        return (
            <form onSubmit={handleSubmit(onSubmit)}>

                <p>The comment will not be visible in the demand history.</p>

                <div className="btn-confirm-group clearfix">
                    <div className="btn-group pull-right btn-group-danger">
                        <button type="button" className="btn btn-default" onClick={onClose}
                                disabled={submitting}> Cancel
                        </button>
                        <button
                            type="submit"
                            id="remove-comment-submit-btn"
                            className="btn btn-primary "
                        >Remove
                        </button>
                    </div>
                </div>

            </form>
        );
    }
}

RemoveTrustedListForm = connect()(reduxForm({
    form: 'remove-comment',
})(RemoveTrustedListForm));

export default RemoveTrustedListForm;
