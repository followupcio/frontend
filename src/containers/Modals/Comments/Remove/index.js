import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../../actions/index';
import Form from './form';
import {getModal} from '../../../../selectors/ui/modal';
import {getAuthUser} from '../../../../selectors/authentication';
import {withRouter} from 'react-router-dom';
import {reset} from 'redux-form';

class RemoveComment extends PureComponent {

    _onSubmit() {
        const {onSubmit, id, demand} = this.props;
        return onSubmit({id, demand});
    }

    render() {
        const {onClose} = this.props;
        return (<Form
            onSubmit={this._onSubmit.bind(this)}
            onClose={onClose}
        />);
    }
}

const mapStateToProps = (state, ownProps) => ({
    authUser: getAuthUser(state),
    modal: getModal(state)
});

const onSubmit = ({id, demand}) => (dispatch) => {

    return actions.removeEvent({id, demand})(dispatch).then(() => {
        dispatch(reset('remove-comment'));
        dispatch(actions.hideModal());
    });

};

const onClose = () => (dispatch) => {
    dispatch(actions.hideModal());
};

RemoveComment = withRouter(connect(
    mapStateToProps,
    {...actions, onSubmit, onClose}
)(RemoveComment));

export default RemoveComment;
