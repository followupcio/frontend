import React from 'react';
import * as actions from '../../actions';

import DemandCreateEdit from './Demands/CreateEdit/index';
import DemandArchive from './Demands/Archive/index';
import DemandUnarchive from './Demands/Unarchive/index';
import DemandSubmit from './Demands/Submit/index';
import DemandApprove from './Demands/Approve/index';
import DemandReject from './Demands/Reject/index';
import DemandDelete from './Demands/Delete/index';
import DemandUndelete from './Demands/Undelete/index';
import DemandDeletePermanently from './Demands/DeletePermanently/index';

import IterationSubmit from './Iterations/Submit/index';
import IterationApprove from './Iterations/Approve/index';
import IterationReject from './Iterations/Reject/index';

import CommentCreate from './Comments/Create/index';
import CommentEdit from './Comments/Edit/index';
import CommentRemove from './Comments/Remove/index';

import DraftSend from './Drafts/Send/index';

import MyStaffRemoveEmployee from './MyStaff/RemoveEmployee/index';

import ModalWrapper from './ModalWrapper';
import {connect} from 'react-redux';
import {getModal} from '../../selectors/ui/modal';

const MODAL_COMPONENTS = {
    ADD_DEMAND: DemandCreateEdit,
    SUBMIT_DEMAND: DemandSubmit,
    APPROVE_DEMAND: DemandApprove,
    DELETE_DEMANDS: DemandDelete,
    UNDELETE_DEMANDS: DemandUndelete,
    DELETE_PERMANENTLY_DEMANDS: DemandDeletePermanently,
    ARCHIVE_DEMANDS: DemandArchive,
    UNARCHIVE_DEMANDS: DemandUnarchive,
    REJECT_DEMAND: DemandReject,
    COMMENT_DEMAND: CommentCreate,
    EDIT_COMMENT: CommentEdit,
    EDIT_DEMAND: DemandCreateEdit,
    SEND_DRAFT_DEMAND: DraftSend,
    REMOVE_TRUSTED_LIST: MyStaffRemoveEmployee,
    REMOVE_COMMENT: CommentRemove,

    SUBMIT_ITERATION: IterationSubmit,
    APPROVE_ITERATION: IterationApprove,
    REJECT_ITERATION: IterationReject

};

const MODAL_TITLES = {
    ADD_DEMAND: 'Add staff assignment',
    ARCHIVE_DEMANDS: 'Archive assignment',
    UNARCHIVE_DEMANDS: 'Unarchive assignment',

    SUBMIT_DEMAND: 'Resolve assignment',
    APPROVE_DEMAND: 'Approve assignment',
    REJECT_DEMAND: 'Reject assignment',
    DELETE_DEMANDS: 'Delete?',
    UNDELETE_DEMANDS: 'Restore?',
    DELETE_PERMANENTLY_DEMANDS: 'Permanently delete?',

    COMMENT_DEMAND: 'Comment assignment',
    EDIT_DEMAND: 'Edit assignment',
    SEND_DRAFT_DEMAND: 'Send draft assignment',
    REMOVE_TRUSTED_LIST: 'Remove staff from "My Staff"?',
    REMOVE_COMMENT: 'Remove comment',
    EDIT_COMMENT: 'Edit comment',

    SUBMIT_ITERATION: "Submit delivery",
    APPROVE_ITERATION: "Approve delivery",
    REJECT_ITERATION: "Reject delivery"

    /* other modals */
};

const getAdditionalProps = (type) => {

    switch (type) {
        case "EDIT_DEMAND":
            return {
                editMode: true
            };
        default:
            return {};
    }
};


const ModalRoot = ({modalType, modalProps, stopServerPing, startServerPing}) => {

    if (!modalType) {
        stopServerPing();

        return <ModalWrapper show={!!modalType}/>;
    } else {

        startServerPing();

        const SpecificModal = MODAL_COMPONENTS[modalType];
        const title = MODAL_TITLES[modalType];
        const additionalProps = getAdditionalProps(modalType);

        if (!SpecificModal) {
            return <span/>
        }

        return (<ModalWrapper title={title} type={modalType} show={!!modalType}>
            <SpecificModal {...additionalProps} {...modalProps} />
        </ModalWrapper>);
    }
};


/**
 * prevent session timeout
 *
 * Ping server when modal is open to
 * refresh auth token
 */

let sessionTimeout;
const startServerPing = () => (dispatch) => {
    sessionTimeout = setInterval(() => {
        dispatch(actions.ping())
    }, 60000); //every minute
};
const stopServerPing = () => (dispatch) => {
    clearInterval(sessionTimeout);
};

export default connect(
    (state) => getModal(state),
    {startServerPing, stopServerPing}
)(ModalRoot);
