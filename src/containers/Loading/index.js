import React from 'react';

const LoadingIndicator = () => (
    <div className="text-center loading-holder"><span className="loading loading-48"/></div>
);

export default LoadingIndicator;