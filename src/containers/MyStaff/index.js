import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Table} from 'react-bootstrap';
import {Link, withRouter} from 'react-router-dom';
import * as actions from '../../actions/';
import {getErrorMessage, getIsFetching, getTrustedUsers} from '../../selectors/entities/trustedUsers.js';
import {getAuthUser} from '../../selectors/authentication'

class MyStaff extends Component {

    fetchData() {
        const {fetchTrustedUsers, authUser} = this.props;

        if (authUser && authUser.id) {
            fetchTrustedUsers();
        }

    }

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate() {
        const {users} = this.props;
        if (!users) {
            this.fetchData();
        }
    }

    shouldComponentUpdate(nextProps) {

        const {users, authUser} = this.props;

        if (Array.isArray(nextProps.users)) {
            return true;
        }

        return (!Array.isArray(users) || !Array.isArray(nextProps.users) ? true : nextProps.users.length !== users.length) ||
            (JSON.stringify(authUser) !== JSON.stringify(nextProps.authUser));
    }

    render() {

        const {history, setUserOngoingDemandsFilters} = this.props;
        let {users} = this.props;

        const aggregateStats = (e) => typeof e.stats === "object" && Object.keys(e.stats).reduce((acc, i) => acc + (i !== "submitted" ? e.stats[i] : 0), 0);

        users = users || [];
        users = users.filter((u) => !!aggregateStats(u));
        users = users.sort((a, b) => aggregateStats(b) - aggregateStats(a));

        if (!users || users.length === 0) {
            return (<div className="user-list">
                    <h4 className="user-list-title">My staff</h4>
                    <div className="noTrustedContainer">
                        <span>No staff assigned yet</span>
                    </div>

                </div>
            );
        }

        return (
            <div className="user-list">
                <h4 className="user-list-title">My staff</h4>
                <div className="user-list-table-header">
                    <Table responsive>

                        <thead>
                        <tr>

                            <th className="small-title text-center"/>
                            <th className="small-title user-col-title-ontime">ON TIME</th>
                            {/* <th className="small-title text-center">REC<br/>COMPL</th>*/}
                            <th className="small-title user-col-title-exp">EXPIRED</th>
                        </tr>

                        </thead>

                    </Table>
                </div>

                <div className="user-list-table-body">
                    <Table responsive>

                        <tbody>

                        {users.map((person) => (
                            <tr key={person.id}>

                                <td className="name-col"><Link to={`/people/${person.id}`}>
                                    <span className="user-photo">
                                            <img src={person.image_url} alt=""/></span>
                                    <span className="user-name">{person.last_name}, {person.first_name}
                                        ({person.division})</span>

                                </Link>
                                </td>
                                <td
                                    className="text-center" onClick={() => {
                                    setUserOngoingDemandsFilters({filter: 'ON_TIME'});
                                    //appHistory.push(`/people/${person.id}`);
                                    history.push(`/people/${person.id}`);
                                }
                                }
                                ><span
                                    className="badge green">{person.stats ? String(person.stats.onTime + person.stats.soonToExpire + 0) : "-"}</span>
                                </td>

                                {/* <td className="text-center"><span className="badge default ">{person.recentlySolved}</span></td>*/}

                                <td
                                    className="text-center" onClick={() => {
                                    setUserOngoingDemandsFilters({filter: 'EXPIRED'});
                                    //appHistory.push(`/people/${person.id}`);
                                    history.push(`/people/${person.id}`);
                                }}
                                ><span
                                    className="badge red ">{person.stats ? String(person.stats.expired + 0) : "-"}</span>
                                </td>
                            </tr>
                        ))}

                        </tbody>
                    </Table>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    users: getTrustedUsers(state),
    isFetching: getErrorMessage(state),
    errorMessage: getIsFetching(state),
    authUser: getAuthUser(state)
});

MyStaff = withRouter(connect(
    mapStateToProps,
    actions
)(MyStaff));

export default MyStaff;

