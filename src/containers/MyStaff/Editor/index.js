import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import {withRouter} from 'react-router-dom';
import {getErrorMessage, getIsFetching, getTrustedUsers} from '../../../selectors/entities/trustedUsers.js';
import {getAuthUser} from '../../../selectors/authentication'
import AddTrustedForm from './Form'
import UsersTable from '../../Tables/Employees/Table'
import * as ROLES from '../../../conf/roles'

class TrustedList extends Component {

    constructor(opts) {
        super(opts);
        this.state = {};
    }

    fetchData() {
        const {fetchTrustedUsers, authUser} = this.props;

        if (authUser && authUser.id) {
            fetchTrustedUsers();
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate() {
        const {users} = this.props;
        if (!users) {
            this.fetchData();
        }
    }

    shouldComponentUpdate(nextProps) {

        const {users, authUser} = this.props;

        if (Array.isArray(nextProps.users)) {
            return true;
        }

        return (!Array.isArray(users) || !Array.isArray(nextProps.users) ? true : nextProps.users.length !== users.length) ||
            (JSON.stringify(authUser) !== JSON.stringify(nextProps.authUser));
    }

    _onRemove(id) {
        const {onRemove} = this.props;
        return onRemove(id)
    }

    _onAdd(email) {
        const {addTrustedUser} = this.props;

        this.setState({
            email
        });

        return addTrustedUser(email)
    }

    render() {

        let {users, authUser} = this.props;
        //const isDirector = authUser.role.indexOf(ROLES.DIRECTOR) > -1;
        //const isDG = authUser.role.indexOf(ROLES.DG) > -1;
        //const isFollower = authUser.role.indexOf(ROLES.FOLLOWER) > -1;
        const isStaff = authUser.role.indexOf(ROLES.STAFF) > -1;

        if (isStaff) {
            return (<div className="user-list">
                    <h4 className="user-list-title">Add staff</h4>
                    <div className="noTrustedContainer">
                        <span>The "Add staff to user trusted list" operation is currently not supported for "staff" user.</span>
                    </div>

                </div>
            );
        }

        if (this.state.email && Array.isArray(users)) {
            let index = users.findIndex(u => u.email === this.state.email);
            let newEmployee = users[index];
            if (newEmployee) {
                users.splice(index, 1);
                users.unshift(newEmployee);
            }

        }

        return (
            <div className="user-list">
                <h4 className="user-list-title">Add staff</h4>

                <AddTrustedForm
                    onSubmit={this._onAdd.bind(this)}
                />

                <UsersTable
                    authUser={authUser}
                    users={users || []}
                    onRemove={this._onRemove.bind(this)}
                />

            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    users: getTrustedUsers(state),
    isFetching: getErrorMessage(state),
    errorMessage: getIsFetching(state),
    authUser: getAuthUser(state)
});

const onRemove = (id) => (dispatch) => {

    dispatch(actions.showModal({
        type: 'REMOVE_TRUSTED_LIST',
        props: {
            id,
        },
    }));
};

TrustedList = withRouter(connect(
    mapStateToProps,
    {...actions, onRemove}
)(TrustedList));

export default TrustedList;

