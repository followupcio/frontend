import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Field, formValueSelector, reduxForm} from 'redux-form';
import {getAuthUser} from '../../../selectors/authentication';
import {getSearchUsers, getSearchUsersQuery} from '../../../selectors/entities/searchUsers';
import * as actions from '../../../actions/index';
import {SimpleSelect} from 'react-selectize';
import 'react-selectize/dist/index.min.css';
import debounce from 'lodash/debounce';

const renderList = ({input, search, placeholder, onSearchChange, options = []}) => {
    options = options || [];

    //let value = options.find(u => u.email === input.value);

    return (<div className="form-group">
        <SimpleSelect
            placeholder={placeholder}
            options={options || []}
            search={search || ""}
            value={input.value ? options.find(u => u.email === input.value) : undefined}
            onSearchChange={onSearchChange}
            onValueChange={(event) => {
                input.onChange(event ? event.value : null)
            }}

            // disable client side filtering
            filterOptions={(options, search) => options}
        />

    </div>)
};

class CommentForm extends PureComponent {

    _onSearch = debounce(this.props.onSearch, 300);

    constructor(opts) {
        super(opts);
        this.state = {
            readyToAdd: false
        }
    }

    _onSearchChange = (query = "") => {
        const {onSearchChange} = this.props;

        onSearchChange(query);

        let q = query.trim();
        if (q.length > 0) {
            this._onSearch(q)
        }

    };

    _onSubmit(values) {
        const {onSubmit, reset} = this.props;


        this.setState({
            readyToAdd: true
        });

        return onSubmit(values.query).then(() => {
            reset('add-trusted-list');

            this.setState({
                readyToAdd: false
            })
        })
    }

    render() {
        const {handleSubmit, search, searchedUsers = [], valid, submitting} = this.props;
        return (
            <div className="trusted-filter">
                <form className="form-inline" onSubmit={handleSubmit(this._onSubmit.bind(this))}>

                    <Field
                        name="query"
                        component={renderList}
                        placeholder="Add staff by last name"
                        options={searchedUsers && searchedUsers.map(u => ({
                            ...u,
                            key: u.email,
                            value: u.email,
                            label: u.display_name
                        }))}

                        //forceEmpty={this.state.readyToAdd}
                        //setState={this.setState.bind(this)}

                        search={search}
                        onSearchChange={this._onSearchChange.bind(this)}
                    />

                    <div className="form-group pull-right">
                        <button disabled={!valid || submitting} type="submit" className="btn btn-default ">
                            Add
                        </button>
                    </div>

                </form>
            </div>

        );
    }
}

const validate = (values) => {
    const errors = {};
    const validateEmail = (email) => {
        const re = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        return re.test(email);
    };

    if (!validateEmail(values.query)) {
        errors.query = 'Must be a valid email'
    }

    return errors;
};

const mapStateToProps = (state, params) => {
    const commentId = params.commentId;

    return {
        commentId,
        searchedUsers: getSearchUsers(state),
        search: getSearchUsersQuery(state),
        authUser: getAuthUser(state),
        employeeToAdd: formValueSelector('add-trusted-list')(state, 'query')
    };
};

const onSearch = (query) => (dispatch, getState) => {
    actions.searchUsers(query)(dispatch, getState);
};

const onSearchChange = (query) => (dispatch) => {
    dispatch(actions.setSearchUsersQuery(query));
};

CommentForm = connect(mapStateToProps,
    {
        ...actions,
        onSearch,
        onSearchChange
    })(reduxForm({
    form: 'add-trusted-list',
    validate,
})(CommentForm));

export default CommentForm;
