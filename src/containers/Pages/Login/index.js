import React, {PureComponent} from 'react';
import LoginForm from './LoginForm';
import {getAuthenticationError, getAuthUser, getToken} from '../../../selectors/authentication';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import packageJson from '../../../../package.json'
import {HOME} from '../../../conf/routes'
import {PROJECT_NAME} from "../../../conf";

class Login extends PureComponent {

    componentDidUpdate() {

        /**
         *   If user has a token (valid or not) redirect to home
         *   If the token is not valid,
         *   the axios interceptor will redirect user to login page
         */
        if (getToken()) {
            const {history} = this.props;
            history.push(HOME);
        }
    }

    componentDidMount() {

        if (getToken()) {
            const {history} = this.props;
            history.push(HOME);
        }
    }

    render() {
        const {loginError} = this.props;

        return (
            <div className="login-main-container">

                <div className="login-form-container">

                    <div className="login-form-holder">

                        <div className="fao-logo-login"/>

                        <h1>Sign in to {PROJECT_NAME}</h1>

                        <br/>

                        {loginError &&
                        <div
                            id="login-error-message"
                            className="alert alert-danger">
                            Incorrect username or password.</div>}

                        <LoginForm/>

                        <br/>

                        <p
                            className="text-center">
                            <a href="https://dgms.fao.org/static/dgms-help.pdf"
                               target="_blank"
                               rel="noopener noreferrer">
                                Need Help?
                            </a>
                        </p>

                    </div>
                </div>

                <div className="big-logo"/>

                <p>Version {packageJson.version}</p>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
    authUser: getAuthUser(state),
    loginError: getAuthenticationError(state)
});

Login = withRouter(connect(
    mapStateToProps
)(Login));

export default Login;
