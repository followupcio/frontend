/*
 * NotFoundPage Messages
 *
 */
import {defineMessages} from 'react-intl';

export default defineMessages({
    title: {
        id: 'app.components.Login.header',
        defaultMessage: 'Login',
    },
});
