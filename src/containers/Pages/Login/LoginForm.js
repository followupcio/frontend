import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {login} from './actions';
import Form from './Form';

class LoginForm extends PureComponent {

    render() {
        const {onSubmit} = this.props;

        return (<Form
            onSubmit={onSubmit}
        />);
    }
}

const onSubmit = ({username, password}) => (dispatch) => {
    
    const credentials = {
        username,
        password
    };

    login({credentials, dispatch});

};

LoginForm = withRouter(connect(
    undefined,
    {onSubmit}
)(LoginForm));

export default LoginForm;
