// in src/restricted.js
import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {hasToken} from '../../../selectors/authentication'
import * as ROUTES from '../../../conf/routes'


/**
 * Higher-order component (HOC) to wrap restricted pages
 */

function restricComponent(BaseComponent) {
    class Restricted extends Component {

        componentWillMount() {
            this.checkAuthentication(this.props);
        }

        componentWillReceiveProps(nextProps) {
            if (nextProps.location !== this.props.location) {
                this.checkAuthentication(nextProps);
            }
        }

        checkAuthentication(params) {
            const {history} = params;
            const authenticated = hasToken();

            if (!authenticated) {
                history.replace({pathname: ROUTES.LOGIN})
            }

        }

        render() {
            return <BaseComponent {...this.props} />;
        }
    }

    return withRouter(Restricted);
}

export default restricComponent;