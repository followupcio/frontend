import React, {PureComponent} from 'react';
import {Col, Row} from 'react-bootstrap';
import {connect} from 'react-redux';
import UserInfo from '../../Employees/Info/index.js';
import {getUser} from '../../../selectors/entities/users.js';
import {getAuthUser} from '../../../selectors/authentication.js';
import * as actions from '../../../actions/index';
import {setUserCompletedDemandsFilters, setUserOngoingDemandsFilters} from '../../../actions/index';
import DemandsTabs from '../../Tabs/Demands/index';
import {getUserCompletedDemandsFilters, getUserOngoingDemandsFilters,} from '../../../selectors/ui/user';


class UserDetail extends PureComponent {

    componentDidMount() {
        const {fetchUser, userId} = this.props;
        fetchUser(userId);
    }

    render() {
        const user = this.props.user || this.props.userId;
        const {isMyPage, authUser, selectedUser} = this.props;

        if (!authUser.role) {
            return <span>Loading</span>
        }

        if (!selectedUser) {
            return <span>User not fond</span>
        }

        return (<Row className="user-info-main-row">

            <Col xs={12} className="user-info-main-col">
                <h4 className="main-section-title">Staff member details</h4>
                <UserInfo
                    isMyPage={isMyPage}
                    user={selectedUser}/>

                <DemandsTabs
                    user={user}
                    filterByAuthUser={isMyPage}
                    setCompletedDemandsFilters={setUserCompletedDemandsFilters}
                    setOngoingDemandsFilters={setUserOngoingDemandsFilters}
                    getCompletedDemandsFilters={getUserCompletedDemandsFilters}
                    getOngoingDemandsFilters={getUserOngoingDemandsFilters}
                />

            </Col>
        </Row>);
    }

    componentWillUnmount() {
        const {setUserCompletedDemandsFilters, setUserOngoingDemandsFilters} = this.props;

        setUserCompletedDemandsFilters();
        setUserOngoingDemandsFilters();
    }
}

const mapStateToProps = (state, params) => {
    const userId = params.match && params.match.params ? params.match.params.id : null;

    return {
        userId,
        authUser: getAuthUser(state),
        selectedUser: getUser(state, userId)
    };
};

UserDetail = connect(mapStateToProps, actions)(UserDetail);

export default UserDetail;
