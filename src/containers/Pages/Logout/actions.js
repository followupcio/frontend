import {LOGOUT_REQUEST, LOGOUT_SUCCESS} from './constants';
import * as api from '../../../api/index';
import LocalStorage from 'localStorage'
import {getToken} from '../../../selectors/authentication'
import * as ROUTES from '../../../conf/routes'

export const logout = ({history}) => (dispatch) => {

    const onLogoutSuccess = () => {

        LocalStorage.clear();

        dispatch({
            type: LOGOUT_SUCCESS,
        });

        history.push(ROUTES.LOGIN);

    };

    dispatch({
        type: LOGOUT_REQUEST,
    });

    if (!getToken()) {
        return new Promise(onLogoutSuccess)
    }

    return api.logout()
        .then(onLogoutSuccess)
        .catch(onLogoutSuccess)
};
