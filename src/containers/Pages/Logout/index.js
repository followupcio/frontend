import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {logout} from "./actions"
import {hideModal} from '../../../actions'

class Logout extends PureComponent {

    componentDidMount() {

        const {logout, history, hideModal} = this.props;
        hideModal();
        logout({history});
    }

    render() {
        return <span/>;
    }

}

const mapDispatchToState = (dispatch) => ({
    logout,
    hideModal
});


Logout = withRouter(connect(undefined, mapDispatchToState())(Logout));

export default Logout;
