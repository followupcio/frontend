import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import FormDemand from '../../Filters/Demands/Search/index';
import DemandsTable from '../../Tables/Demands/index';
import {TabContent, TabLink, Tabs} from 'react-tabs-redux';
import {getSearchDemandFilters} from '../../../selectors/ui/search';
import * as actions from '../../../actions/index';
import {getAuthUser} from '../../../selectors/authentication';

class Search extends PureComponent {

    render() {
        const {demandFilters, authUser} = this.props;

        if (!authUser.id) {
            return <div>Loading</div>
        }

        return (<div className="search-main-container">
            <h4 className="main-section-title">Search</h4>

            <Tabs
                renderActiveTabContentOnly={true}
                name="search"
                className="search-tabs-holder"
            >
                <div className="tabs-list">
                    <TabLink to="demands">ASSIGNMENTS</TabLink>
                </div>

                <TabContent for="demands">

                    <div className="search-main">

                        <FormDemand/>
                        <DemandsTable
                            status="ongoing"
                            filters={demandFilters}
                        />
                    </div>

                </TabContent>

            </Tabs>
        </div>);
    }

    componentWillUnmount() {
        const {setSearchDemandFilters} = this.props;

        setSearchDemandFilters({});
    }
}

const mapStateToProps = (state) => ({
    demandFilters: getSearchDemandFilters(state),
    authUser: getAuthUser(state)
});

Search = connect(mapStateToProps, actions)(Search);

export default Search;
