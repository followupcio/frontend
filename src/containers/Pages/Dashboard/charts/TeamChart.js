import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Highcharts from 'highcharts';
import * as actions from '../../../../actions/index';

class Charts extends PureComponent {

    _onDataClick(risk) {
        const {setHomeTableRiskLevel} = this.props;
        setHomeTableRiskLevel(risk);
    }

    componentDidMount() {
        // Extend Highcharts with modules
        if (this.props.modules) {
            this.props.modules.forEach((module) => {
                module(Highcharts);
            });
        }

        /*   chart: {
               type: 'pie',
                   margin: [0, 0, 0, 0],
                   spacingTop: 0,
                   spacingBottom: 0,
                   spacingLeft: 0,
                   spacingRight: 0,
                   height: 270,
                   backgroundColor: 'rgba(255, 255, 255, 0)',
                   style: {
                   fontFamily: 'inherit'
               }
           },

           //remove title and subtitle
           title: {
               text: '',
                   style: {
                   display: 'none'
               }
           },
           subtitle: {
               text: '',
                   style: {
                   display: 'none'
               }
           },*/


        // Set container which the chart should render to.
        this.chart = new Highcharts[this.props.type || 'Chart'](
            'chart-3',
            {
                chart: {
                    type: 'pie',
                    margin: [0, 0, 0, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0,

                    backgroundColor: 'rgba(255, 255, 255, 0)',
                    style: {
                        fontFamily: 'inherit',
                    },
                },

                // remove title and subtitle
                title: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },

                plotOptions: {
                    pie: {
                        borderColor: '#000000',
                        allowPointSelect: true,
                        center: ['50%', '50%'],
                        borderWidth: 0,
                        // set radius
                        slicedOffset: 0,
                        size: '70%',
                        // labels inside the pie
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            distance: 5,
                            formatter() {
                                return `<div class="pie-label"><span class="user-photo small" data-user=${this.point.name}></span><span>${this.point.homeLabel}</span><b>${this.y}%</b><div>`;
                            },
                            style: {
                                color: '#333',
                                textShadow: false,
                                fontWeight: 'normal',
                            },
                            softConnector: false,
                        },
                    },
                },

                tooltip: {
                    backgroundColor: null,
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    enabled: false,
                    style: {
                        padding: 0,
                    },

                    formatter() {
                        return `${this.key}: <b>${this.y}%</b>`;
                    },
                },

                // remove credits
                credits: {
                    enabled: false,
                },

                series: [{
                    name: 'Risk',
                    data: [
                        {
                            name: 'Triani',
                            risk: '2 - high',
                            color: '#fed97e',
                            y: 2,
                            homeLabel: 'Triani, Mario (CIO)',
                        },
                        {
                            name: 'Cortesini',
                            risk: '1 - warn',
                            color: '#ffc77c',
                            y: 8,
                            homeLabel: 'Cortesini, Ivano (CIO)',
                        },
                        {
                            name: 'Wagner',
                            risk: '0 - low',
                            color: '#ffa37a',
                            y: 12,
                            homeLabel: 'Wagner, Marie (CIO)',
                        },
                        {
                            name: 'Abblasio',
                            risk: '0 - low',
                            color: '#ff8679',
                            y: 14,
                            homeLabel: 'Abblasio, Giovanni (CIO)',
                        },
                        {
                            name: 'Salvatore',
                            risk: '0 - low',
                            color: '#ff6e79',
                            y: 18,
                            homeLabel: 'Salvatore, Daniele (CIO)',
                        },
                        {
                            name: 'Maskey',
                            risk: '0 - low',
                            color: '#ff6178',
                            y: 21,
                            homeLabel: 'Maskey, Samrat (CIO)',
                        },
                        {
                            name: 'Selleri',
                            risk: '0 - low',
                            color: '#fe4f76',
                            y: 25,
                            homeLabel: 'Selleri, Nicola (CIO)',
                        },
                    ],
                }],
            }
        );
    }


    componentWillUnmount() {
        this.chart.destroy();
    }

    render() {
        return (
            <div className="dashboard-chart-container">
                <h4 className="dashboard-title">Ongoing assignments by staff</h4>
                <div id="chart-3"/>
            </div>);
    }
}

Charts = connect( // what allows us to connect a component to Redux's store, and action creators
    null,
    actions
)(Charts);

export default Charts;
