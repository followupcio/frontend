import React, {PureComponent} from 'react';
import Highcharts from 'highcharts';

class Charts extends PureComponent {

    componentDidMount() {
        // Extend Highcharts with modules
        if (this.props.modules) {
            this.props.modules.forEach((module) => {
                module(Highcharts);
            });
        }

        // Set container which the chart should render to.
        this.chart1 = new Highcharts[this.props.type || 'Chart'](
            'chart-1',
            {
                chart: {
                    type: 'pie',
                    margin: [0, 0, 0, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0,

                    backgroundColor: 'rgba(255, 255, 255, 0)',
                    style: {
                        fontFamily: 'inherit',
                    },
                },

                // remove title and subtitle
                title: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },

                plotOptions: {
                    pie: {
                        borderColor: '#000000',
                        allowPointSelect: true,
                        center: ['50%', '50%'],
                        borderWidth: 0,
                        // set radius
                        slicedOffset: 0,
                        size: '70%',
                        // labels inside the pie
                        dataLabels: {
                            enabled: true,
                            useHTML: true,
                            formatter() {
                                return `<div class="pie-label"><span>${this.point.homeLabel}</span> <b>${this.y}%</b><div>`;
                            },
                            style: {
                                color: '#333',
                                textShadow: false,
                                fontWeight: 'normal',
                            },
                            distance: -1,
                        },
                    },
                },

                tooltip: {
                    backgroundColor: null,
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    enabled: false,
                    style: {
                        padding: 0,
                    },


                    formatter() {
                        return `${this.key}: <b>${this.y}%</b>`;
                    },
                },

                // remove credits
                credits: {
                    enabled: false,
                },

                series: [{
                    name: 'Risk',
                    data: [
                        {
                            name: 'Expired',
                            status: 'expired',
                            color: '#d32f2e',
                            y: 11,
                            homeLabel: '11',
                        },
                        {
                            name: 'Soon to expire',
                            status: 'soon-to-expire',
                            color: '#ff8e01',
                            y: 20,
                            homeLabel: '20',
                        },
                        {
                            name: 'On time',
                            status: 'on-time',
                            color: '#5cb85c',
                            y: 69,
                            homeLabel: '69',
                        },
                    ],
                }],
            }
            // (chart) => {
            //    const label = chart.renderer.label('<div class="total"><span class="total-title small-title dashboard-total">TOTAL</span>  200</div>', 0, 0, 'rect', 0, 0, true, true, 'total').add()
            //
            //    label.on("click", () =>
            //            onClick({...filterValues, filter: "all"})
            //    )
            // } // on complete
        );
    }

    componentWillUnmount() {
        this.chart1.destroy();
    }

    render() {
        return (
            <div className="dashboard-chart-container">
                <h4 className="dashboard-title">My assignments by expiration date</h4>
                <div id="chart-1"/>
            </div>

        );
    }
}

export default Charts;
