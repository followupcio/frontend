import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import Highcharts from 'highcharts';
import HighchartsDrilldown from 'highcharts/modules/drilldown';
import * as actions from '../../../../actions/index';

class Charts extends PureComponent {

    _onDataClick(risk) {
        const {setHomeTableRiskLevel} = this.props;
        setHomeTableRiskLevel(risk);
    }

    componentDidMount() {
        // Extend Highcharts with modules
        if (this.props.modules) {
            this.props.modules.forEach((module) => {
                module(Highcharts);
            });
        }

        HighchartsDrilldown(Highcharts);

        // Set container which the chart should render to.
        this.chart2 = new Highcharts[this.props.type || 'Chart'](
            'chart-2',
            {
                chart: {
                    type: 'bar',
                    margin: [0, 0, 0, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0,
                    // height: 270,
                    backgroundColor: 'rgba(255, 255, 255, 0)',
                    style: {
                        fontFamily: 'open_sansregular',
                    },
                },

                // remove title and subtitle
                title: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none',
                    },
                },

                plotOptions: {
                    bar: {
                        stacking: 'percent',
                        dataLabels: {
                            enabled: true,
                            // borderRadius: 5,
                            // backgroundColor: 'rgba(252, 255, 197, 0.7)',
                            // borderWidth: 1,
                            // borderColor: '#AAA',
                            allowOverlap: true,
                            useHTML: true,
                            textDecoration: 'none',
                            formatter() {
                                return `<div class="pie-label"><span>${this.point.drilldown}</span><b>${this.y}%</b><div>`;
                            },
                        },
                    },
                },
                legend: {
                    enabled: false,
                },


                xAxis: {
                    lineWidth: 0,
                    tickLength: 0,
                    labels: {
                        enabled: false,
                    },
                    title: {
                        text: null,
                    },
                },
                yAxis: {
                    lineWidth: 0,
                    tickLength: 0,
                    labels: {
                        enabled: false,
                    },
                    title: {
                        text: null,
                    },
                },

                tooltip: {
                    backgroundColor: null,
                    borderWidth: 0,
                    shadow: false,
                    useHTML: true,
                    enabled: false,
                    style: {
                        padding: 0,
                    },

                    formatter() {
                        return `${this.key}: <b>${this.y}%</b>`;
                    },
                },

                // remove credits
                credits: {
                    enabled: false,
                },


                series: [{
                    name: 'SDG Agenda',
                    data: [{
                        name: 'Demands',
                        y: 40,
                        drilldown: 'SDG Agenda',
                        color: '#48e3ff',
                    }],
                }, {
                    name: 'SP',
                    data: [{
                        name: 'Demands',
                        y: 60,
                        drilldown: 'SP',
                        color: '#4da2ff',

                    }],
                }],

                drilldown: {
                    drillUpButton: {
                        relativeTo: 'spacingBox',
                        position: {
                            y: -35,
                            x: 0,
                        },
                        theme: {
                            fill: 'rgba(255,255,255,0.5)',
                            'stroke-width': 1,
                            stroke: '#1976d3',
                            r: '15px',
                            states: {
                                hover: {
                                    fill: 'rgba(255,255,255,0.5)',
                                },
                                select: {
                                    stroke: '#1976d3',
                                    fill: 'rgba(255,255,255,0.5)',
                                },
                            },
                        },

                    },
                    series: [{
                        name: 'SDG Agenda',
                        id: 'SDG Agenda',
                        legend: {
                            enabled: false,
                        },
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            borderRadius: 5,
                            // backgroundColor: 'rgba(252, 255, 197, 0.7)',
                            // borderWidth: 1,
                            borderColor: '#AAA',
                            allowOverlap: true,
                            useHTML: true,
                            align: 'right',
                            formatter() {
                                return `<div class="pie-label bar-drill"><span>${this.key}</span><b>${this.y}%</b><div>`;
                            },
                        },
                        data: [
                            // ['SDG 1 - No poverty', 1],
                            // ['SDG 2 - Zero hunger', 3],
                            // ['SDG 3 - Good, health and well-being', 8],
                            // ['SDG 4 - Quality education', 5],
                            // ['SDG 5 - Gender equality', 1],
                            // ['SDG 6 - Clean water and sanitation', 1],
                            // ['SDG 7 - Affordable and clean energy', 1],
                            // ['SDG 8 - Decent work and economic growth', 3],
                            // ['SDG 9 - Industry, innovation and infrastructure', 1],
                            // ['SDG 10 - Reduced inequalities', 1],
                            // ['SDG 11 - Sustainable cities and communities', 1],
                            // ['SDG 12 - Responsible consumption and production', 1],
                            // ['SDG 13 - Climate action', 5],
                            // ['SDG 14 - Life below water', 5],
                            // ['SDG 15 - Life in land', 5],
                            // ['SDG 16 - Peace, justice and strong institutions', 10],
                            // ['SDG 17 - Partnerships for the goals', 10]

                            ['SDG 1', 1],
                            ['SDG 2', 3],
                            ['SDG 3', 8],
                            ['SDG 4', 5],
                            ['SDG 5', 7],
                            ['SDG 6', 8],
                            ['SDG 7', 1],
                            ['SDG 8', 3],
                            ['SDG 9', 1],
                            ['SDG 10', 1],
                            ['SDG 11', 6],
                            ['SDG 12', 1],
                            ['SDG 13 ', 5],
                            ['SDG 14', 5],
                            ['SDG 15', 5],
                            ['SDG 16', 10],
                            ['SDG 17', 10],

                        ],
                    }, {
                        name: 'SP',
                        id: 'SP',
                        legend: {
                            enabled: false,
                        },
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            borderRadius: 5,
                            // backgroundColor: 'rgba(252, 255, 197, 0.7)',
                            // borderWidth: 1,
                            borderColor: '#AAA',
                            allowOverlap: true,
                            useHTML: true,
                            align: 'right',
                            formatter() {
                                return `<div class="pie-label  bar-drill"><span>${this.key}</span><b>${this.y}%</b><div>`;
                            },
                        },
                        data: [
                            // ['SP 1 - Help eliminate hunger, food insecurity and malnutrition', 27],
                            // ['SP 2 - make agriculture, forestry and fisheries more productive and sustainable', 23],
                            // ['SP 3 - Reduce rural poverty', 35],
                            // ['SP 4 - Enable inclusive and efficient agricultural and food systems', 5],
                            // ['SP 5 - Increase the resilience of livelihoods from disasters', 10]

                            ['SP 1', 27],
                            ['SP 2', 23],
                            ['SP 3', 35],
                            ['SP 4', 5],
                            ['SP 5', 10],
                        ],
                    }]
                },

            }
        );
    }


    componentWillUnmount() {
        this.chart2.destroy();
    }

    render() {
        return (
            <div className="dashboard-chart-container">
                <h4 className="dashboard-title">Ongoing assignments by category</h4>
                <div id="chart-2"/>
            </div>);
    }
}

Charts = connect( // what allows us to connect a component to Redux's store, and action creators
    null,
    actions
)(Charts);

export default Charts;
