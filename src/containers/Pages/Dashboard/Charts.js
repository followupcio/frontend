import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Col, Row} from 'react-bootstrap';
import RiskChart from './charts/RiskChart-dashboard';
import TagChart from './charts/TagChart';
import TeamChart from './charts/TeamChart';
import * as actions from '../../../actions/index';

class Charts extends PureComponent {

    _onDataClick(risk) {
        const {setHomeTableRiskLevel} = this.props;
        setHomeTableRiskLevel(risk);
    }

    render() {
        return (

            <div className="dashboard-container">
                <Row className="dashboard-row-title">
                    <Col sm={12}>
                        <h4 className="main-section-title">Dashboard</h4>
                    </Col>
                    <Col lgOffset={2} lg={4}/>
                </Row>

                <div className="dashboard-row bar-row">
                    <div className="dashboard-col">
                        <div className="elegant-card chart">
                            <TagChart/>
                        </div>
                    </div>
                </div>
                <div className="dashboard-row">
                    <div className="dashboard-col top">
                        <div className="elegant-card chart">
                            <RiskChart/>
                        </div>

                    </div>
                    <div className="dashboard-col top">
                        <div className="elegant-card chart">
                            <TeamChart/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Charts = connect(
    null,
    actions
)(Charts);

export default Charts;
