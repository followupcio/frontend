import React, {PureComponent} from 'react';
import {connect} from 'react-redux'
import SettingPage from '../../Settings/index';
import {getAuthUser} from '../../../selectors/authentication'
import {getStoredDraft} from '../../../selectors/entities/draft'
import {getDemand} from '../../../selectors/entities/demands'
import {Col, Row} from 'react-bootstrap';
import {showModal} from '../../../actions/index';
import * as ROUTES from '../../../conf/routes';


class MyPage extends PureComponent {

    _onEditDraft() {

        const {history, onEditDraft, showModal, draft} = this.props;

        history.push(ROUTES.HOME);

        onEditDraft({demand: draft, showModal});
    }

    render() {

        const {authUser, draft} = this.props;

        if (!authUser.id) {
            return <div>Loading</div>
        }

        return (
            <div className="setting-page">
                <Row className="user-info-main-row">

                    <Col xs={12} className="user-info-main-col">
                        <h4 className="main-section-title">Staff member setting</h4>

                        <Row>
                            <Col sm={8} smOffset={2}>
                                {draft && <button
                                    type="button"
                                    className="btn btn-primary back-to-draft"
                                    onClick={this._onEditDraft.bind(this)}>
                                    <i className="material-icons back">chevron_left</i> Back To Draft
                                </button>}
                            </Col>
                        </Row>


                        <SettingPage isMyPage={true} user={authUser.id}/>

                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    const storedDraft = getStoredDraft(state);
    const draft = !!storedDraft ? getDemand(state, storedDraft) : undefined;

    return {
        authUser: getAuthUser(state),
        draft,
    }
};

const onEditDraft = ({demand, showModal}) => (dispatch) => {

    dispatch(showModal({
        type: 'EDIT_DEMAND',
        props: {
            demand,
            multiple: true
        },
    }))
};


MyPage = connect(mapStateToProps, {
    onEditDraft,
    showModal
})(MyPage);

export default MyPage;
