import React, {PureComponent} from 'react';
import {Link} from 'react-router-dom';
import Moment from 'moment';
import {API_BASE_URL, BACKEND_BASE_URL} from '../../../conf/axios'
import {DATE_FORMAT, DATE_FORMAT_DEADLINE} from '../../../conf/index'
import {DropdownButton, MenuItem} from 'react-bootstrap';

const getExtensionIcon = (name) => {
    const extension = name.slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || '';
    switch (extension.toLocaleLowerCase()) {
        case 'pdf':
            return <span key={name} className="label label-default">PDF</span>;
        case 'png':
            return <span key={name} className="label label-default">PNG</span>;
        default :
            return <span key={name} className="label label-default">{extension.toLocaleLowerCase()}</span>;
    }
};

function humanFileSize(bytes, si = true) {
    let thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    let units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
}

const printTextWithLineBreak = (text) => {
    return text ? text.split('\n').map((item, i) => {
        return (<span key={i}>{item}<br/></span>)
    }) : "";
};

class Timeline extends PureComponent {

    getComment(c, demand, authUser, editable) {

        const {onRemoveEvent, onEditEvent} = this.props;
        const isYourComment = c.author_id === authUser.id;

        return <li key={c.id}>

            <div className="commenterImage">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo" data-user={c.author.display_name}
                        title={c.author.display_name}
                    >
                        <img src={c.author.image_url} alt=""/>
                    </div>


                </Link>

            </div>
            <div className="commentText" data-author={c.author.display_name}>

                <div className="comment-header">
                    <Link to={`/people/${c.author.id}`}>{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}

                    {editable && isYourComment && !c.removed &&
                    <div className="pull-right"><DropdownButton title="..." id="bg-nested-dropdown"
                                                                className="more-dropdown comment-dropdown">
                        <MenuItem eventKey="1" onClick={() => {
                            onEditEvent({id: c.id, event: c, demand})
                        }
                        }>Edit</MenuItem>
                        <MenuItem divider/>
                        <MenuItem eventKey="2" onClick={() => {
                            onRemoveEvent({id: c.id, demand})
                        }} className="more-dropdown-delete">Remove</MenuItem>
                    </DropdownButton></div>}
                </div>

                <p className="them">{c.removed ?
                    <span className="message-removed"><i className="material-icons">delete_forever</i><span>This message has been removed</span> </span> :
                    <span>{c.edited && <i className="material-icons message-edited">mode_edit</i>}
                        <span>  {printTextWithLineBreak(c.description)}   </span></span>}</p>


                <span
                    className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {!c.removed && c.attachments.map((f, i) => {
                            return (<li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                                href={BACKEND_BASE_URL + f.url}
                                target="_blank" rel="noopener noreferrer">{f.originalName || f.name} </a><span
                                className="file-size">({humanFileSize(f.size)})</span></li>)
                        }
                    )}
                </ul>

            </div>
        </li>;
    }

    getDraftSubmit(c) {
        const getExtensionIcon = (name) => {
            const extension = name.slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || '';
            switch (extension.toLocaleLowerCase()) {
                case 'pdf':
                    return <span key={name} className="label label-default">PDF</span>;
                case 'png':
                    return <span key={name} className="label label-default">PNG</span>;
                default :
                    return <span key={name} className="label label-default">{extension.toLocaleLowerCase()}</span>;
            }
        };

        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    >
                        <img src={c.author.image_url} alt=""/>
                    </div>
                </Link>

            </div>
            <div className="commentText submitted-demand">

                <div className="comment-header resolved-demand">
                    <i className="material-icons">gesture</i>
                    Response draft created by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin">{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getSubmit(c) {
        const getExtensionIcon = (name) => {
            const extension = name.slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || '';
            switch (extension.toLocaleLowerCase()) {
                case 'pdf':
                    return <span key={name} className="label label-default">PDF</span>;
                case 'png':
                    return <span key={name} className="label label-default">PNG</span>;
                default :
                    return <span key={name} className="label label-default">{extension.toLocaleLowerCase()}</span>;
            }
        };

        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    >
                        <img src={c.author.image_url} alt=""/>
                    </div>
                </Link>

            </div>
            <div className="commentText submitted-demand">

                <div className="comment-header resolved-demand">
                    <i className="material-icons">schedule</i>
                    Submitted by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin">{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getApproved(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText approved-demand">


                <div className="comment-header approved-demand">
                    <i className="material-icons">check_circle</i>
                    Approved by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin">{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>
                <span
                    className="date sub-text"/>

            </div>
        </li>;
    }

    getRejected(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText rejected-demand">


                <div className="comment-header">
                    <i className="material-icons">remove_circle</i>
                    Returned to staff by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>
                <span className="date sub-text"/>
            </div>
        </li>;
    }

    getReopen(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText reopen-demand">

                <div className="comment-header">
                    <i className="material-icons">replay</i>

                    <span>Reopened</span> by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>
                <span className="date sub-text"/>
            </div>
        </li>;
    }

    getCreation(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">add_circle</i>

                    Created by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    showUpdateChanges(c) {

        if (!c.tracking) {
            return <span/>
        }

        const trunc = (string, n = 50) => {

            if (!string) {
                return " - ";
            }
            return (string.length > n) ? string.substr(0, n - 1) + '...' : string;
        };

        const tracking = c.tracking || {};
        const {previous, next, diff} = tracking;
        const fields = {
            title: {label: "Title", format: (t) => trunc(t)},
            description: {label: "Description", format: (t) => trunc(t)},
            deadline: {
                label: "Deadline", format: (d) => {
                    return Moment(d).format(DATE_FORMAT)
                }
            },
            attachments: {label: "Attachments"},
            category_id: {label: "Category"},
            subcategory_id: {label: "Subcategory"},
            assigned_to: {label: "Assignee"},
            draft_assigned_to: {label: "Assignee"},
            visibleToOtherAssignees: {label: "Visibility"}
        };

        return <div className="changes-holder">
            <span className="small-title">Changes</span>
            <ul className="changes-list list-unstyled">
                {diff.map(d => {
                    const x = fields[d];
                    if (!x) {
                        return <span key={d}/>
                    }
                    return <li key={d}>{x.label || d} {x.format ? <span>:&nbsp;
                        from <b>{x.format(previous[d])}</b> to <b>{x.format(next[d])}</b></span> : ""} </li>;
                })}

            </ul>

        </div>
    }

    getDraft(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">gesture</i>

                    Draft created by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getDelete(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">delete</i>

                    Deleted by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getUndelete(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">undo</i>

                    Restored by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getArchived(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">archive</i>

                    Archived by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>


            </div>
        </li>;
    }

    getUnarchived(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">unarchive</i>

                    Unarchived by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>


            </div>
        </li>;
    }

    getUpdate(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand edited-demand">

                <div className="comment-header">
                    <i className="material-icons">mode_edit</i>

                    Edited by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

                {this.showUpdateChanges(c)}

            </div>

        </li>;
    }

    getDraftUpdate(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">mode_edit</i>

                    Draft edited by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <span className="date sub-text"/>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>
                {this.showUpdateChanges(c)}
            </div>

        </li>;
    }

    getRead(c, demand) {

        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText created-demand">

                <div className="comment-header">
                    <i className="material-icons">done_all</i>

                    <span>Read</span>&nbsp;on {Moment(demand.read).format(DATE_FORMAT)}</div>


            </div>
        </li>;

    }

    //Iterations
    getIterationSubmit(c) {
        const getExtensionIcon = (name) => {
            const extension = name.slice((name.lastIndexOf(".") - 1 >>> 0) + 2) || '';
            switch (extension.toLocaleLowerCase()) {
                case 'pdf':
                    return <span key={name} className="label label-default">PDF</span>;
                case 'png':
                    return <span key={name} className="label label-default">PNG</span>;
                default :
                    return <span key={name} className="label label-default">{extension.toLocaleLowerCase()}</span>;
            }
        };

        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    >
                        <img src={c.author.image_url} alt=""/>
                    </div>
                </Link>

            </div>
            <div className="commentText submitted-demand">

                <div className="comment-header resolved-demand">
                    <i className="material-icons">schedule</i>
                    Recurring delivery of {Moment(c.iteration.deadline).format(DATE_FORMAT_DEADLINE)} submitted by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin">{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>

                <ul className="list-unstyled attached-file">
                    {c.attachments.map((f, i) =>
                        <li className="submitted-link" key={i}>{getExtensionIcon(f.originalName || f.name)} <a
                            href={API_BASE_URL + 'containers/demands/download/' + f.name}
                            target="_blank" rel="noopener noreferrer">{f.originalName} </a><span
                            className="file-size">({humanFileSize(f.size)})</span></li>
                    )}
                </ul>

            </div>
        </li>;
    }

    getIterationApproved(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText approved-demand">


                <div className="comment-header approved-demand">
                    <i className="material-icons">check_circle</i>
                    Recurring delivery of {Moment(c.iteration.deadline).format(DATE_FORMAT_DEADLINE)} approved by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin">{c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{c.description}</p>
                <span
                    className="date sub-text"/>

            </div>
        </li>;
    }

    getIterationRejected(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText rejected-demand">


                <div className="comment-header">
                    <i className="material-icons">remove_circle</i>
                    Recurring delivery of {Moment(c.iteration.deadline).format(DATE_FORMAT_DEADLINE)} returned to staff
                    by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>
                <span className="date sub-text"/>
            </div>
        </li>;
    }

    getIterationReopen(c) {
        return <li key={c.id}>
            <div className="commenterImage hide-image">
                <Link to={`/people/${c.author.id}`}>
                    <div
                        className="user-photo"
                        title={c.author.display_name}
                    />
                </Link>

            </div>
            <div className="commentText reopen-demand">

                <div className="comment-header">
                    <i className="material-icons">replay</i>

                    Recurring delivery of {Moment(c.iteration.deadline).format(DATE_FORMAT_DEADLINE)} reopened by <Link
                    to={`/people/${c.author.id}`}
                    className="name-margin"> {c.author.display_name} </Link>
                    on {Moment(c.created_at).format(DATE_FORMAT)}</div>

                <p className="them">{printTextWithLineBreak(c.description)}</p>
                <span className="date sub-text"/>
            </div>
        </li>;
    }

    render() {
        const {actions = [], demand, isYou, isToYou, authUser} = this.props;

        //const hasActionType = (actions, type) => actions.filter(a => a.type && a.type.toUpperCase() === type).length > 0;

        return (
            <ul className="commentList">
                {actions.map((c, index) => {
                        switch (c.type.toUpperCase()) {
                            case "CREATE":
                                return this.getCreation(c);
                            case "UPDATE":
                                return this.getUpdate(c);
                            case "REOPEN" :
                                return this.getReopen(c);
                            case "SUBMIT":
                                return this.getSubmit(c);
                            case "APPROVE":
                                return this.getApproved(c);
                            case "REJECT":
                                return this.getRejected(c);
                            case "COMMENT":
                                // Comment is editable only if it is the most recent event in the history
                                // this will not allow users to alter the old history of the demand
                                return this.getComment(c, demand, authUser, index === 0);
                            case "DELETE":
                                return this.getDelete(c);
                            case "UNDELETE":
                                return this.getUndelete(c);
                            case "ARCHIVE":
                                return this.getArchived(c);
                            case "UNARCHIVE":
                                return this.getUnarchived(c);
                            case "DRAFT":
                                return isYou && this.getDraft(c);
                            case "DRAFT_SUBMIT":
                                return isToYou && this.getDraftSubmit(c);
                            case "DRAFT_UPDATE":
                                return isYou && this.getDraftUpdate(c);
                            case "READ":
                                return this.getRead(c, demand);
                            case "ITERATION_SUBMIT":
                                return this.getIterationSubmit(c);
                            case "ITERATION_REOPEN":
                                return this.getIterationReopen(c);
                            case "ITERATION_APPROVE":
                                return this.getIterationApproved(c);
                            case "ITERATION_REJECT":
                                return this.getIterationRejected(c);
                            default:
                                return null;

                        }
                    }
                )}
            </ul>

        );
    }
}

export default Timeline;
