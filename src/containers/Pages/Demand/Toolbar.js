import React, {PureComponent} from 'react';
import {Button, DropdownButton, MenuItem} from 'react-bootstrap';
import {connect} from 'react-redux'
import {validate} from '../../Modals/Demands/CreateEdit/form'

class Toolbar extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            disabledButtons: false
        };
    }

    _isValidDraft(model) {
        const errors = validate(model) || {};
        return Object.keys(errors).length === 0;
    }

    render() {
        const {isAssignedByMe, isCreatedByMe, isDelegatedToYou, isDirector, allowSubmit, isDG, isToYou, isYou, draft, onDeleteBtnClick, onUndeleteBtnClick, onSendDraftClick, onEditBtnClick, onArchiveBtnClick, onUnarchiveBtnClick, isManager, isFollower, archived, submitted, completed, deleted, ongoing, onReopenClick, onDeletePermanentlyBtnClick, onSubmitBtnClick, onRejectClick, onApproveClick, demand} = this.props;

        const events = demand.events;
        const draftEvent = events.slice(0).reverse().find(e => e.type === "DRAFT_SUBMIT");
        const isDraft = !!draftEvent;
        const isSubmit = events.slice(0).reverse().find(e => e.type === "SUBMIT");

        const {disabledButtons} = this.state;

        return (<div className="demand-details-toolbar">

            {!!allowSubmit && !submitted && ongoing && (isManager || isToYou) && <Button
                type="button" bsStyle="primary" className="margin-right" id="submit-for-review-btn"
                onClick={() => onSubmitBtnClick(demand)}
            > {isDraft || isSubmit ? "Submit for review (draft)" : "Submit for review"}</Button>}

            {submitted && (isManager || isToYou) && <Button
                disabled={disabledButtons}
                type="button" bsStyle="default" className="margin-right"
                id="reopen-demand-btn"
                onClick={() => this._wrapToDisableButtonsOnExecution(onReopenClick(demand))}
            > Reopen assignment </Button>}

            {submitted && (((isFollower && !isToYou) || isDG) || (isDirector && (isAssignedByMe || isCreatedByMe)) || isDelegatedToYou) &&
            <span>
                    <Button
                        type="button"
                        bsStyle={demand.finalApproved === true ? "primary" : "default"}
                        className="margin-right"
                        id="approve-demand-btn"
                        onClick={() => onApproveClick(demand)}
                    > Approve </Button>
                    <Button
                        type="button"
                        bsStyle={demand.finalApproved === false ? "primary" : "default"}
                        className="margin-right"
                        id="reject-demand-btn"
                        onClick={() => onRejectClick(demand)}
                    > Return to staff </Button>
                </span>}

            {ongoing && (isYou || (isFollower && !isToYou) || isDG || isDelegatedToYou) && demand.parent &&
            <DropdownButton title="..." id="bg-nested-dropdown" className="more-dropdown">
                <MenuItem eventKey="1" onClick={() => {
                    onApproveClick(demand)
                }}>Mark as completed</MenuItem>
                <MenuItem eventKey="2" onClick={() => {
                    onEditBtnClick(demand)
                }}>Edit</MenuItem>
                <MenuItem divider/>
                <MenuItem eventKey="3" onClick={() => {
                    onDeleteBtnClick(demand)
                }} className="more-dropdown-delete">Delete</MenuItem>
            </DropdownButton>

            }

            {ongoing && (isYou || (isFollower && !isToYou) || isDG) && !demand.parent &&
            <DropdownButton title="..." id="bg-nested-dropdown" className="more-dropdown">
                <MenuItem eventKey="1" onClick={() => {
                    onApproveClick(demand)
                }}>Mark as completed</MenuItem>
                <MenuItem eventKey="2" onClick={() => {
                    onEditBtnClick(demand)
                }}>Edit</MenuItem>
                <MenuItem divider/>
                <MenuItem eventKey="3" onClick={() => {
                    onDeleteBtnClick(demand)
                }} className="more-dropdown-delete">Delete</MenuItem>
            </DropdownButton>}

            {draft && this._isValidDraft(demand) && isYou && !demand.parent_id &&
            <Button
                type="button"
                bsStyle="primary"
                className="margin-right"
                onClick={() => onSendDraftClick(demand)}
            > Submit </Button>}

            {draft && (isDirector || isFollower || isDG) &&
            <Button
                type="button"
                bsStyle={"default"}
                className="margin-right"
                onClick={() => onEditBtnClick(demand)}
            > Edit </Button>}

            {draft && (isDirector || isFollower || isDG) && !demand.parent_id &&
            <DropdownButton title="..." id="bg-nested-dropdown" className="more-dropdown">
                <MenuItem eventKey="1" onClick={() => {
                    onEditBtnClick(demand)
                }}>Edit</MenuItem>
                <MenuItem divider/>
                <MenuItem eventKey="2" onClick={() => {
                    onDeleteBtnClick(demand)
                }} className="more-dropdown-delete">Delete</MenuItem>
            </DropdownButton>}

            {!demand.parent && completed && (isDirector || isFollower || isDG) &&
            <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                id="reopen-demand-btn"
                onClick={() => onReopenClick(demand)}
            > Reopen </Button>}

            {completed && (isYou || (isFollower && !isToYou) || isDG) &&
            <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                id="archive-demand-btn"
                onClick={() => onArchiveBtnClick(demand)}
            > Archive </Button>
            }

            {deleted && (isYou || (isFollower && !isToYou) || isDG) && <span>
                <Button
                    type="button"
                    bsStyle="danger"
                    className="margin-right"
                    onClick={() => onDeletePermanentlyBtnClick(demand)}
                > Delete permanently </Button> <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                id="undelete-demand-btn"
                onClick={() => onUndeleteBtnClick(demand)}
            > Restore </Button> </span>}

            {archived && (isYou || (isFollower && !isToYou) || isDG) && <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                id="unarchive-demand-btn"
                onClick={() => onUnarchiveBtnClick(demand)}
            > Unarchive </Button>}

        </div>);
    }

    componentDidMount() {
        this._ismounted = true;
    }

    componentWillUnmount() {
        this._ismounted = false;
    }

    _wrapToDisableButtonsOnExecution(promise) {

        this.setState({
            disabledButtons: true
        });

        return promise.then(() => {

            if (this._ismounted) {
                this.setState({
                    disabledButtons: false
                });
            }
        })
    }

}

Toolbar = connect()(Toolbar);

export default Toolbar;
