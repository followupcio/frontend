import React, {PureComponent} from 'react';
import {ButtonToolbar, Col, Dropdown, Row} from 'react-bootstrap';
import {TabContent, TabLink, Tabs} from 'react-tabs-redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {getDemand} from '../../../selectors/entities/demands';
import {getAuthUser} from '../../../selectors/authentication';
import * as actions from '../../../actions/index';
import Timeline from './History'
import Toolbar from "./Toolbar"
import Badge from './Badge'
import Moment from 'moment';
import CommentForm from '../../Modals/Comments/Create/index';
import {DATE_FORMAT, DATE_FORMAT_DEADLINE} from '../../../conf/index'
import * as ROLES from '../../../conf/roles'
import Table from '../../Tables/Demands/Table'
import {getUsers} from '../../../selectors/entities/users';
import Iterations from './Iterations'
import reduce from 'lodash/reduce';

class DemandDetail extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
            disabledButtons: false
        };
    }

    componentDidMount() {
        const {authUser} = this.props;

        this._ismounted = true;

        if (!authUser.role) {
            return
        }

        this.fetchDemand();
        this.fetchUsers();
    }

    componentDidUpdate() {


        const {allUsers, demand = {}, authUser, id, parent} = this.props;


        if (!authUser.role) {
            return
        }

        if (!demand || demand.id !== id) {
            this.fetchDemand();
        }

        if (!!demand && demand.parent_id && !parent) {
            this.fetchParentDemand()
        }


        if (!allUsers) {
            this.fetchUsers();
        }

    }

    fetchUsers() {
        const {fetchUsers} = this.props;
        fetchUsers();
    }

    fetchDemand() {
        const {fetchDemand, id} = this.props;
        fetchDemand(id);
    }

    fetchParentDemand() {
        const {fetchDemand, demand} = this.props;
        fetchDemand(demand.parent_id);
    }

    _onReopenBtnClick(demand) {

        const {onReopenBtnClick} = this.props;

        return this._wrapToDisableButtonsOnExecution(onReopenBtnClick({demand}));
    }

    onSubmitBtnClick(demand) {
        const {onSubmitBtnClick, showModal} = this.props;
        onSubmitBtnClick({demand, showModal});
    }

    _onApproveBtnClick(demand) {
        const {onApproveBtnClick, showModal} = this.props;
        onApproveBtnClick({demand, showModal});
    }

    _onRejectBtnClick(demand) {
        const {onRejectBtnClick, showModal} = this.props;
        return onRejectBtnClick({demand, showModal});
    }

    _onSendDraftClick(demand) {
        const {onSendDraftClick, showModal} = this.props;
        return onSendDraftClick({demand, showModal});
    }

    _onEditBtnClick(demand) {
        const {onEditBtnClick, showModal} = this.props;
        return onEditBtnClick({demand, showModal});
    }

    _onDeleteBtnClick(demand) {
        const {onDeleteBtnClick, showModal} = this.props;
        return onDeleteBtnClick({demand, showModal});
    }

    _onUndeleteBtnClick(demand) {
        const {onUndeleteBtnClick, showModal} = this.props;
        return onUndeleteBtnClick({demand, showModal});
    }

    _onDeletePermanentlyBtnClick(demand) {
        const {onDeletePermanentlyBtnClick, showModal} = this.props;
        return onDeletePermanentlyBtnClick({demand, showModal});
    }

    _onArchiveBtnClick(demand) {
        const {onArchiveBtnClick, showModal} = this.props;
        return onArchiveBtnClick({demand, showModal});
    }

    _onUnarchiveBtnClick(demand) {
        const {onUnarchiveBtnClick, showModal} = this.props;
        return onUnarchiveBtnClick({demand, showModal});
    }

    //Iterations

    _onIterationSubmit(iteration) {
        const {onIterationSubmit, showModal} = this.props;
        const promise = onIterationSubmit({iteration, showModal});
        return promise
    }

    _onIterationReopen(iteration) {
        const {onIterationReopen} = this.props;
        const promise = onIterationReopen({iteration});
        return promise
    }

    _onIterationApprove(iteration) {
        const {onIterationApprove, showModal} = this.props;
        return onIterationApprove({iteration, showModal});
    }

    _onIterationReject(iteration) {
        const {onIterationReject, showModal} = this.props;
        return onIterationReject({iteration, showModal});
    }

    componentWillUnmount() {
        this._ismounted = false;
    }

    _wrapToDisableButtonsOnExecution(promise) {

        this.setState({
            disabledButtons: true
        });

        return promise.then(() => {

            if (this._ismounted) {
                this.setState({
                    disabledButtons: false
                });
            }
        })
    }

    render() {
        const {demand, authUser, onRemoveEvent, onEditEvent, parent} = this.props;

        if (!authUser.role) {
            return <span>Loading</span>
        }
        const role = authUser.role || [];
        const authUserId = authUser.id;
        const isDirector = role.indexOf(ROLES.DIRECTOR) > -1;
        const isDG = role.indexOf(ROLES.DG) > -1;
        const isFollower = role.indexOf(ROLES.FOLLOWER) > -1;
        const isStaff = role.indexOf(ROLES.STAFF) > -1;
        const isYou = demand && typeof demand === "object" ? authUserId === demand.created_by : false;
        const isDelegatedToYou = demand && typeof demand === "object" ? authUserId === demand.assigned_by : false;
        const isToYou = demand && typeof demand === "object" ? authUserId === demand.assigned_to : false;
        const isCreatedByMe = demand && typeof demand === "object" ? authUserId === demand.created_by : false;
        const isAssignedByMe = demand && typeof demand === "object" ? authUserId === demand.assigned_by : false;

        if (!demand) {
            return (<span>Assignment not found</span>);
        }
        const submitted = !!demand.submitted;
        const isDraft = demand.status && demand.status.toUpperCase() === "DRAFT";
        const completed = demand.status.toLowerCase() === "completed";
        const ongoing = demand.status.toLowerCase() === "ongoing";
        const draft = demand.status.toLowerCase() === "draft";
        const deleted = demand.status.toLowerCase() === "deleted";
        const archived = demand.status.toLowerCase() === "archived";
        const isCompleted = demand.children ? demand.children.reduce((acc, value) => acc && value.status === "COMPLETED", true) : false;
        const isMultiple = !!demand.parent || (isDraft && Array.isArray(demand.assigned_to) && demand.assigned_to.length > 1);
        const {visibleToOtherAssignees} = demand;

        const receiver = demand.receiver; // || !isMultiple ? trustedUsers.find(u => u.id === demand.assigned_to) : {};
        const canEditDemand = (isDirector || isDG || isFollower);
        const showDelegate = demand.issuer && demand.issuer.id && demand.requester && demand.requester.id && demand.issuer.id !== demand.requester.id && canEditDemand;

        const childrenDemands = parent && Array.isArray(parent.children) ? parent.children : demand.children;

        let iterations = demand.iterations || [];
        iterations = iterations.sort((a, b) => new Date(a.deadline) - new Date(b.deadline));
        const hasRecurring = iterations.length > 0;
        /*
         * If is recurring demand allow submit only if the
         * deadline of the last iteration is expired or each iteration
         * is completed
         * */
        let allowSubmit;
        if (hasRecurring) {
            const allIterationAreCompleted = reduce(iterations, (acc, i) => acc && (i.status && i.status.toUpperCase() === "COMPLETED"), true);
            if (!allIterationAreCompleted) {
                const lastIterationDeadline = iterations[iterations.length - 1].deadline;
                allowSubmit = Moment().diff(lastIterationDeadline) > 0;
            } else {
                allowSubmit = true
            }

        } else {
            allowSubmit = true;
        }


        const isVisibleToMe = demand.visibleToOtherAssignees && !!childrenDemands.find(d => {
            return !!d && !!d.receiver && d.receiver.id === authUserId
        });

        if (!isDG && !isFollower && !isCreatedByMe && !isAssignedByMe && !isToYou && !isVisibleToMe) {
            return (<span>Assignment not found</span>);
        }

        return (<Row className="demand-details-holder">

                <Col xs={12} className="demand-details-col">

                    <h4 className="main-section-title">Assignment Details</h4>

                    <div className="demand-info">

                        <div className="demand-details-title">

                            <div className="demand-details-subtitle">
                                <div className="demand-details-subtitle-item">
                                    <Badge
                                        isCompleted={isCompleted}
                                        demand={demand}
                                    />

                                </div>
                                <div className="demand-details-subtitle-item">

                                    <span className="small-title">Assignee{isMultiple ? "s" : ""}:</span>

                                    {isMultiple ? <span className="multiple-ass-user">
                                            <i className="material-icons">group</i> Multiple assignees
                                            </span> : receiver ?
                                        <Link to={`/people/${receiver.id}`}>
                                        <span className="user-photo">
                                            <img src={receiver.image_url} alt=""/>
                                        </span>
                                            {receiver.last_name}, {receiver.first_name} &nbsp;
                                            ({receiver.division})
                                        </Link> :
                                        <span>Not assigned yet</span>
                                    }

                                </div>

                                <div
                                    className={"demand-details-subtitle-item demand-details-deadline " + demand.timeframe}>

                                    <span className="small-title"> Deadline </span>
                                    {Moment(demand.deadline).format(DATE_FORMAT_DEADLINE)}
                                </div>
                                {demand.parent_id && (((isDG || isFollower) && demand.parent_id) || (visibleToOtherAssignees && !demand.parent) || isAssignedByMe) &&
                                <div className="demand-details-subtitle-item right">
                                    <Link id="link-to-parent-demand" className="parent-link"
                                          to={`/demands/${demand.parent_id}`}>
                                        Go to parent assignment <i className="material-icons">keyboard_arrow_up</i>
                                    </Link>
                                </div>}
                            </div>

                            <h2>

                                <span className="title-text">{submitted && <span
                                    className={"label label-success to-be-approved"}>Submitted for review</span>} {demand.title}</span>

                                <span className="title-description">{demand.description}</span>

                            </h2>

                            <div className="demand-details-second-line">

                                <div className="demand-details-subtitle-item">
                                    <span className="small-title"> Created by </span>
                                    <Link to={`/people/${demand.issuer.id}`}>
                                        <span
                                            className="user-name">{demand.issuer.last_name}, {demand.issuer.first_name}&nbsp;
                                            ({demand.issuer.division}) </span>&nbsp;

                                    </Link>
                                    on {Moment(demand.created_at).format(DATE_FORMAT)}
                                </div>

                                <div className="demand-details-subtitle-item">
                                    <span className="small-title"> Source </span>
                                    <div>{!!visibleToOtherAssignees ?
                                        <ButtonToolbar>
                                            <Dropdown className="visible-assignees-dropdown"
                                                      id="visible-assignees-dropdown">
                                                <Dropdown.Toggle>
                                                    <i className="material-icons"
                                                       title="Click to show other assignees">visibility</i>
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu>
                                                    {childrenDemands.map((c, i) => {
                                                        return <li key={i}>
                                                            <Link to={`/demands/${c.id}`}>
                                                                <span
                                                                    className="user-name">{c.receiver.last_name}, {c.receiver.first_name}&nbsp;
                                                                    ({c.receiver.division}) </span>&nbsp;
                                                            </Link>
                                                        </li>

                                                    })}
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </ButtonToolbar>
                                        : (!!demand.parent_id && ((isDG || isFollower) || visibleToOtherAssignees)) ?
                                            <Link className="parent-link source" to={`/demands/${demand.parent_id}`}>
                                                <span className="multiple-ass-user"><i
                                                    className="material-icons">group</i>Multiple assignment</span>
                                            </Link>
                                            : demand.source}</div>
                                </div>

                                <div className="demand-details-subtitle-item">
                                    <span className="small-title"> Category </span>
                                    <span
                                        className="category-name">{demand.category && demand.category.label ? demand.category.label : ""}&nbsp;</span>
                                    <span
                                        className="category-name"> {demand.subcategory && demand.subcategory.label ? " - " + demand.subcategory.label : ""}</span>
                                </div>

                            </div>

                            {showDelegate && <div className="demand-details-second-line">

                                <div className="demand-details-subtitle-item">
                                    <span className="small-title"> Delegated to </span>
                                    <Link to={`/people/${demand.requester.id}`}>
                                        <span
                                            className="user-name">{demand.requester.last_name}, {demand.requester.first_name}&nbsp;
                                            ({demand.requester.division}) </span>&nbsp;

                                    </Link>
                                </div>

                            </div>}

                            <Toolbar
                                isCreatedByMe={isCreatedByMe}
                                isAssignedByMe={isAssignedByMe}
                                isDirector={isDirector}
                                isDG={isDG}
                                isFollower={isFollower}
                                isStaff={isStaff}
                                isYou={isYou}
                                isToYou={isToYou}
                                isDelegatedToYou={isDelegatedToYou}

                                submitted={submitted}
                                ongoing={ongoing}
                                completed={completed}
                                draft={draft}
                                deleted={deleted}
                                archived={archived}
                                allowSubmit={allowSubmit}

                                onReopenClick={this._onReopenBtnClick.bind(this)}
                                onSubmitBtnClick={this.onSubmitBtnClick.bind(this)}
                                onRejectClick={this._onRejectBtnClick.bind(this)}
                                onApproveClick={this._onApproveBtnClick.bind(this)}
                                onSendDraftClick={this._onSendDraftClick.bind(this)}
                                onEditBtnClick={this._onEditBtnClick.bind(this)}
                                onDeleteBtnClick={this._onDeleteBtnClick.bind(this)}
                                onUndeleteBtnClick={this._onUndeleteBtnClick.bind(this)}
                                onDeletePermanentlyBtnClick={this._onDeletePermanentlyBtnClick.bind(this)}
                                onArchiveBtnClick={this._onArchiveBtnClick.bind(this)}
                                onUnarchiveBtnClick={this._onUnarchiveBtnClick.bind(this)}

                                demands={demand.children}
                                demand={demand}
                            />

                        </div>

                    </div>

                    <div className="demand-body">

                        {Array.isArray(iterations) && iterations.length > 0 && <Iterations
                            isDirector={isDirector}
                            isDG={isDG}
                            isFollower={isFollower}
                            isStaff={isStaff}
                            isYou={isYou}
                            isToYou={isToYou}

                            draft={draft}
                            completed={completed}
                            archived={archived}
                            deleted={deleted}

                            onSubmitBtnClick={this._onIterationSubmit.bind(this)}
                            onReopenClick={this._onIterationReopen.bind(this)}
                            onApproveClick={this._onIterationApprove.bind(this)}
                            onRejectClick={this._onIterationReject.bind(this)}

                            demand={demand}
                            iterations={iterations}
                        />}

                        <Tabs
                            renderActiveTabContentOnly={true}
                            name="page-demand"
                            id="detail-tabs"
                            className="tabs demand-details-tabs"
                            selectedTab={(!isMultiple || isDraft) ? "history" : "related-demands"}
                        >

                            <div className="tabs-list">
                                {isMultiple && !isDraft && <TabLink to="related-demands">Related assignments</TabLink>}
                                {(!isMultiple || isDraft) && <TabLink to="history">History</TabLink>}
                            </div>


                            {isMultiple && !isDraft && <TabContent for="related-demands">


                                <div className="detailBox multiple-detail-box">

                                    <Table
                                        demands={demand.children}
                                        isInMultipleDemandPage={true}
                                        authUser={authUser}
                                    />

                                </div>


                            </TabContent>}

                            <TabContent for="history">
                                <div className="detailBox">

                                    <div className="actionBox">

                                        <CommentForm demand={demand} draft={draft}/>

                                        <Timeline
                                            demand={demand}
                                            isYou={isYou}
                                            isToYou={isToYou}
                                            authUser={authUser}
                                            onRemoveEvent={onRemoveEvent}
                                            onEditEvent={onEditEvent}
                                            actions={demand.events.sort((a, b) => {
                                                if (a.created_at > b.created_at)
                                                    return -1;
                                                if (a.created_at < b.created_at)
                                                    return 1;
                                                return 0;

                                            })}/>

                                    </div>
                                </div>


                            </TabContent>

                        </Tabs>

                    </div>


                </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state, params) => {
    const id = params.match && params.match.params ? params.match.params.id : null;
    const demand = getDemand(state, id);

    return {
        id,
        allUsers: getUsers(state),
        authUser: getAuthUser(state),
        demand,
        parent: demand && demand.parent_id && getDemand(state, demand.parent_id)
    };
};

// Iterations

const onIterationReopen = ({iteration}) => (dispatch) => {

    const payload = {
        type: "reopen"
    };

    return actions.reopenIteration(payload, iteration, dispatch);
};

const onIterationSubmit = ({iteration, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'SUBMIT_ITERATION',
        props: {
            iteration,
        },
    }));
};

const onIterationApprove = ({iteration, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'APPROVE_ITERATION',
        props: {
            iteration,
        },
    }));
};

const onIterationReject = ({iteration, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'REJECT_ITERATION',
        props: {
            iteration,
        },
    }));
};

// Demands

const onReopenBtnClick = ({demand}) => (dispatch) => {

    const payload = {
        type: "reopen"
    };

    const promise = actions.reopenDemand(payload, demand, dispatch);
    return promise
};

const onSubmitBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'SUBMIT_DEMAND',
        props: {
            demand,
        },
    }));
};

const onApproveBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'APPROVE_DEMAND',
        props: {
            demand,
        },
    }));
};

const onRejectBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'REJECT_DEMAND',
        props: {
            demand,
        },
    }));
};

const onSendDraftClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'SEND_DRAFT_DEMAND',
        props: {
            demand,
        },
    }));
};


const onDeleteBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'DELETE_DEMANDS',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onUndeleteBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'UNDELETE_DEMANDS',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onArchiveBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'ARCHIVE_DEMANDS',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onUnarchiveBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'UNARCHIVE_DEMANDS',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onDeletePermanentlyBtnClick = ({demand, showModal}) => (dispatch) => {
    dispatch(showModal({
        type: 'DELETE_PERMANENTLY_DEMANDS',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onEditBtnClick = ({demand, showModal}) => (dispatch) => {

    dispatch(showModal({
        type: 'EDIT_DEMAND',
        props: {
            demand,
            multiple: !!demand.parent
        },
    }))
};

const onRemoveEvent = ({id, demand}) => (dispatch) => {

    dispatch(actions.showModal({
        type: 'REMOVE_COMMENT',
        props: {
            id,
            demand
        },
    }))
};

const onEditEvent = ({id, event, demand}) => (dispatch) => {

    dispatch(actions.showModal({
        type: 'EDIT_COMMENT',
        props: {
            editMode: true,
            id,
            event,
            demand
        },
    }))
};


DemandDetail = connect(mapStateToProps, {
    ...actions,
    onSubmitBtnClick,
    onApproveBtnClick,
    onRejectBtnClick,
    onReopenBtnClick,
    onSendDraftClick,
    onArchiveBtnClick,
    onUnarchiveBtnClick,
    onEditBtnClick,
    onDeleteBtnClick,
    onUndeleteBtnClick,
    onDeletePermanentlyBtnClick,
    onRemoveEvent,
    onEditEvent,
    onIterationReopen,
    onIterationSubmit,
    onIterationApprove,
    onIterationReject
})(DemandDetail);

export default DemandDetail;
