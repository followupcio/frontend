import React, {PureComponent} from 'react';
import Moment from 'moment';
import {DATE_FORMAT_DEADLINE} from '../../../conf/index'
import Badge from './Badge'
import {Button} from 'react-bootstrap';

class Iterations extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            disabledButtons: false,
            page: 1,
            page_size: 10
        };
    }

    _onReopenClick(i) {
        const {onReopenClick} = this.props;
        const promise = onReopenClick(i);
        return this._wrapToDisableButtonsOnExecution(promise);
    }

    componentDidMount() {
        this._ismounted = true;
    }

    componentWillUnmount() {
        this._ismounted = false;
    }

    _wrapToDisableButtonsOnExecution(promise) {

        this.setState({
            disabledButtons: true
        });

        return promise.then(() => {

            if (this._ismounted) {
                this.setState({
                    disabledButtons: false
                });
            }
        })
    }

    onNextPageButtonClick() {

        const {page} = this.state;

        this.setState({
            page: page + 1
        })

    }

    calculateIterationsToRender(iterations) {

        const {page, page_size} = this.state;

        return iterations.slice(0, page * page_size);

    }

    getIteration(i, completeDemand) {

        const {isToYou, isDirector, isFollower, isDG, draft, onSubmitBtnClick, onApproveClick, onRejectClick} = this.props;
        const submitted = !!i.submitted;
        const ongoing = i.status && i.status.toUpperCase() === "ONGOING";
        const completed = i.status && i.status.toUpperCase() === "COMPLETED";

        return (<li key={i.id}>

            <Badge
                isCompleted={completed}
                demand={i}
            />
            {submitted && <span className={"label label-success to-be-approved"}>Submitted</span>}
            <div
                className={"demand-details-subtitle-item demand-details-deadline " + i.timeframe}>
                <span className="small-title"> Deadline </span>
                {Moment(i.deadline).format(DATE_FORMAT_DEADLINE)}
            </div>

            {!completeDemand && !draft && !submitted && ongoing && isToYou && <Button
                type="button" bsStyle="default" className="margin-right"
                onClick={() => onSubmitBtnClick(i)}
            >Submit for review</Button>}

            {!completeDemand && !draft && ((submitted && isToYou) || (completed && (isDirector || isFollower || isDG))) &&
            <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                //disabled={disabledButtons}
                onClick={() => this._onReopenClick(i)}
            > Reopen </Button>}

            {!completeDemand && !draft && !completed && (isDirector || isFollower || isDG) && <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                onClick={() => onApproveClick(i)}
            > Approve </Button>}

            {!completeDemand && !draft && submitted && (isDirector || isFollower || isDG) && <Button
                type="button"
                bsStyle="default"
                className="margin-right"
                onClick={() => onRejectClick(i)}
            > Return to staff </Button>}

        </li>);

    }

    isMoreButtonVisible(iterations) {

        const {page_size, page} = this.state;
        // return true, button has to be shown, only if
        // the number of shown iteration is
        // equal to the page * page_size, the max showable items
        // considering the lazy rendering
        return iterations.length === page * page_size;
    }

    render() {
        const {iterations = [], completed, archived, deleted} = this.props;

        //const iterationsToRender = this.calculateIterationsToRender(iterations);
        //const showMoreButton = this.isMoreButtonVisible(iterationsToRender);

        return (
            <div className="iterations-list">
                {(completed || archived || deleted) && <span className="freezer"/>}
                <h6>RECURRING DELIVERY</h6>
                <ol className=" list-unstyled">

                    {iterations.map(i =>
                        this.getIteration(i, completed))}
                </ol>

                {/*<button*/}
                {/*type="button"*/}
                {/*disabled={!showMoreButton}*/}
                {/*className="btn show-more-btn"*/}
                {/*onClick={this.onNextPageButtonClick.bind(this)}*/}
                {/*> Show more*/}
                {/*</button>*/}

            </div>
        )
    }
}

export default Iterations;
