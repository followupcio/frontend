import React, {PureComponent} from 'react';

class Badge extends PureComponent {

    _getStatusBadge(demand) {

        const status = demand.status;
        const submitted = !!demand.submitted;

        if (submitted) {

            switch (demand.timeframe) {
                case 'ON_TIME':
                    return <span className={"label label-success"}>On time</span>;
                case 'SOON_TO_EXPIRE':
                    return <span className={"label label-warning"}>Soon to expire</span>;
                case 'EXPIRED':
                    return <span className={"label label-danger"}>Expired</span>;
                default:
                    return <span className={"label label-default"}/>;
            }
        }

        if (status.toLowerCase() === "ongoing") {

            switch (demand.timeframe) {
                case 'ON_TIME':
                    return <span className={"label label-success"}>On time</span>;
                case 'SOON_TO_EXPIRE':
                    return <span className={"label label-warning"}>Soon to expire</span>;
                case 'EXPIRED':
                    return <span className={"label label-danger"}>Expired</span>;
                default:
                    return <span className={"label label-default"}/>;
            }
        }

        if (status.toLowerCase() === "completed") {

            return <span className={"label label-default"}>Done</span>;

        }

        if (status.toLowerCase() === "draft") {
            return <span className={"label label-default label-draft"}>Draft</span>;
        }

        if (status.toLowerCase() === "archived") {
            return <span className={"label label-default label-draft"}>Archived</span>;
        }

        if (status.toLowerCase() === "deleted") {
            return <span className={"label label-default label-draft"}>Deleted</span>;
        }


    }

    render() {
        const {demand} = this.props;

        return (<span>{this._getStatusBadge(demand)}</span>);
    }
}

export default Badge;
