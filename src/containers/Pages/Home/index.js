/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
//import {FormattedMessage} from 'react-intl';
//import messages from './messages';
import {connect} from 'react-redux';
import {Col, Row} from 'react-bootstrap';
import AddDemand from '../../Buttons/AddDemand/index';
import TrustedUsersList from '../../MyStaff/index';
import Chart from '../../Charts/Demands/Timeframes/index';
import DemandsTabs from '../../Tabs/Demands/index';
import {getAuthUser, hasRole} from '../../../selectors/authentication';
import {
    getDrawerDemandStatus,
    getHomeCompletedDemandsFilters,
    getHomeOngoingDemandsFilters
} from '../../../selectors/ui/home';
import {setHomeCompletedDemandsFilters, setHomeOngoingDemandsFilters, toggleDrawer} from '../../../actions/index';
import * as ROLES from '../../../conf/roles'

/* to user i18n text   <FormattedMessage {...messages.header} />*/

class Home extends React.PureComponent {

    render() {

        const {authUser, isDirector, isDG, toggleDrawer, isDrawerClosed} = this.props;

        return (<div className={"home-container " + (isDrawerClosed ? "" : "drawer-open")}>
            <h4 className="main-section-title">My assignments</h4>

            <Row className="home-row-main">

                <button onClick={toggleDrawer} className="btn btn-default drawer-toggle">
                    {isDrawerClosed ?
                        <i className="material-icons">chevron_right</i> :
                        <i className="material-icons">chevron_left</i>}
                </button>

                <Col md={9} lg={9} className="home-col-left">


                    {(isDirector || isDG) ? <div className="main-btns">
                        <AddDemand/>
                    </div> : <h4>View as {authUser.display_name}</h4>}

                    <DemandsTabs
                        isHome={true}
                        setCompletedDemandsFilters={setHomeCompletedDemandsFilters}
                        setOngoingDemandsFilters={setHomeOngoingDemandsFilters}
                        getCompletedDemandsFilters={getHomeCompletedDemandsFilters}
                        getOngoingDemandsFilters={getHomeOngoingDemandsFilters}
                    />

                </Col>

                <Col md={3} lg={3} className="no-gutter-left home-col-right">

                    <TrustedUsersList/>

                    <div className="chart">

                        <h4 className="home-small-title">Assignments by expiration date</h4>

                        <Chart
                            getCompletedDemandsFilters={getHomeCompletedDemandsFilters}
                            getOngoingDemandsFilters={getHomeOngoingDemandsFilters}
                        />

                        {/*  <div className="more-info-holder">
                         <Link to="/dashboard">
                         <Button bsStyle="default" className="pull-right">
                         More info
                         </Button>
                         </Link>
                         </div>*/}

                        <div className="legenda-holder">
                            <ul className="list-unstyled">
                                <li className="small-title"><span className="leg ontime"/>On time</li>
                                <li className="small-title"><span className="leg soon"/>Soon to expire</li>
                                <li className="small-title"><span className="leg expired"/>Expired</li>
                            </ul>
                        </div>
                        <div className="legenda-holder-right">
                            <ul className="list-unstyled">
                                <li className="small-title"><span className="leg submitted"/>Submitted</li>
                            </ul>
                        </div>
                    </div>

                </Col>

            </Row>

        </div>);
    }
}

const mapStateToProps = (state) => {

    return {
        authUser: getAuthUser(state),
        isDG: hasRole(state, ROLES.DG),
        isDirector: hasRole(state, ROLES.DIRECTOR),
        isDrawerClosed: getDrawerDemandStatus(state)
    };
};

const mapDispatchToState = (dispatch) => ({
    toggleDrawer: () => {
        dispatch(toggleDrawer())
    }
});


Home = connect(mapStateToProps, mapDispatchToState)(Home);

export default Home;