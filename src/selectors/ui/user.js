export const getUserOngoingDemandsFilters = (state) =>
    state.ui.user.ongoing.filters;

export const getUserCompletedDemandsFilters = (state) =>
    state.ui.user.completed.filters;
