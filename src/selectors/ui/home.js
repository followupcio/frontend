export const getHomeOngoingDemandsFilters = (state) =>
    state.ui.home.ongoing.filters;

export const getHomeCompletedDemandsFilters = (state) =>
    state.ui.home.completed.filters;

export const getChartDemandStatus = (state) =>
    state.ui.home.chart.stats;
export const getChartDemandStatusIsFetching = (state) =>
    state.ui.home.chart.isFetching;
export const getChartDemandStatusErrorMessage = (state) =>
    state.ui.home.chart.errorMessage;

export const getDrawerDemandStatus = (state) =>
    state.ui.home.drawer;
