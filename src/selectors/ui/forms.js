export const getDemandFormValues = (state) =>
    state.form.demands ? state.form.demands.values : {};

export const getSearchDemandFormValues = (state) =>
    state.form['search-demand'] ? state.form['search-demand'].values : {};
