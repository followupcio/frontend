export const getSelectedTab = (state, namespace) =>
    state.ui.tabs[namespace];
