export const getIds = (state) =>
    state.entities.highPriorities.list.ids;

export const getIsFetching = (state) =>
    state.entities.highPriorities.list.isFetching;

export const getErrorMessage = (state) =>
    state.entities.highPriorities.list.errorMessage;

export const getHighPriority = (state, id) =>
    state.entities.highPriorities.byId[id];

export const getHighPriorities = (state) =>
    state.entities.highPriorities.list.ids && state.entities.highPriorities.list.ids.map((id) => getHighPriority(state, id));
