// ongoing

export const getIsFetching = (state) =>
    state.entities.timeFrames.list.isFetching;

export const getErrorMessageOngoing = (state) =>
    state.entities.timeFrames.list.errorMessage;

export const getTimeframe = (state, id) =>
    state.entities.timeFrames.byId[id];

export const getTimeframes = (state) =>
    state.entities.timeFrames.list.ids && state.entities.timeFrames.list.ids.map((id) => getTimeframe(state, id));

export const getOngoingTimeFrames = (state) =>
    filterByStatus(getTimeframes(state), "ONGOING-TIMEFRAME");

export const getCompletedTimeFrames = (state) =>
    filterByStatus(getTimeframes(state), "COMPLETED-TIMEFRAME");

export const getAllTimeFrames = (state) =>
    [...getCompletedTimeFrames(state), ...getOngoingTimeFrames(state)];

const filterByStatus = (parents, status) =>
    parents && parents.length ? parents[0].children.filter(t => t.resource === status) : [];
