export const getIsFetching = (state, status) =>
    state.entities.demands.list.isFetching;

export const getErrorMessage = (state, status) =>
    state.entities.demands.list.errorMessage;

export const getDemand = (state, id) =>
    state.entities.demands.byId[id];

export const getDemands = (state) =>
    state.entities.demands.list.ids.map(id => getDemand(state, id));

export const getStats = (state) =>
    state.entities.demands.list.stats;



