// ongoing

export const getOngoingIds = (state) =>
    state.entities.timeFrames.listByStatus.ongoing.ids;

export const getIsFetching = (state) =>
    state.entities.timeFrames.listByStatus.ongoing.isFetching;

export const getErrorMessageOngoing = (state) =>
    state.entities.timeFrames.listByStatus.ongoing.errorMessage;

export const getOngoingTimeframe = (state, id) =>
    state.entities.timeFrames.byId[id];

export const getOngoingTimeFrames = (state) =>
    state.entities.timeFrames.listByStatus.ongoing.ids && state.entities.timeFrames.listByStatus.ongoing.ids.map((id) => getOngoingTimeframe(state, id));

// completed

export const getCompletedIds = (state) =>
    state.entities.timeFrames.listByStatus.completed.ids;

export const getIsFetchingCompleted = (state) =>
    state.entities.timeFrames.listByStatus.completed.isFetching;

export const getErrorMessageCompleted = (state) =>
    state.entities.timeFrames.listByStatus.completed.errorMessage;

export const getCompletedTimeframe = (state, id) =>
    state.entities.timeFrames.byId[id];

export const getCompletedTimeframeses = (state) =>
    state.entities.timeFrames.listByStatus.completed.ids && state.entities.timeFrames.listByStatus.completed.ids.map((id) => getCompletedTimeframe(state, id));

