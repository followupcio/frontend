export const getIds = (state) =>
    state.entities.categories.list.ids;

export const getIsFetching = (state) =>
    state.entities.categories.list.isFetching;

export const getErrorMessage = (state) =>
    state.entities.categories.list.errorMessage;

export const getCategory = (state, id) =>
    state.entities.categories.byId[id];

export const getCategories = (state) =>
    state.entities.categories.list.ids && state.entities.categories.list.ids.map((id) => getCategory(state, id));
