import {getAuthUser} from '../../selectors/authentication';

export const getDemands = (state, {status, filters: {filter = 'all', q, from, to, risk, country, category, assigned_to, subcategory, ...rest}, userId, includeChildrenDemand}) => {

    const authUser = getAuthUser(state);

    if (!authUser.role) {
        return [];
    }

    const authUserId = authUser.id;
    const isDirector = authUser.role.indexOf("Director") > -1;
    const isDG = authUser.role.indexOf("DirectorGeneral") > -1;
    const isFollower = authUser.role.indexOf("Follower") > -1;
    const isManager = authUser.role.indexOf("Manager") > -1;

    filter = filter || 'all';

    let demands;


    demands = getDemandsByStatusAndFilter(state, status, filter) || [];
    demands = demands.ids ? demands.ids.map((id) => getDemand(state, id)) : [];


    if (!isManager) {
        demands = demands.filter(d => !d.parentid || d.assigned_to === authUserId);
    }

    //demands = !!includeChildrenDemand ? demands : demands.filter( d => d.parent || (!d.parent && !d.link) || (d.link && d.assignedTo._id === authUser.get("_id")));

    if (authUser.id === userId) {
        userId = undefined;
    }

    if (userId && !isDG && !isFollower) {
        demands = demands.filter((d) => d.assigned_to === userId);
    }

    if (country) {
        demands = demands.filter((d) => d.country === country);
    }

    if (category) {
        demands = demands.filter((d) => d.category_id === category);
    }

    if (subcategory) {
        demands = demands.filter((d) => d.subcategory_id === subcategory);
    }

    if (assigned_to) {
        demands = demands.filter((d) => d.assigned_to === assigned_to);
    }

    if (q) {
        q = q.toLowerCase();
        demands = demands.filter((demand) => (demand.title && demand.title.toLowerCase().indexOf(q) >= 0) ||
            (typeof demand.assigned_to === "object" && demand.assigned_to.display_name && demand.assigned_to.display_name.toLowerCase().indexOf(q) >= 0) ||
            (typeof demand.created_by === "object" && demand.created_by.display_name && demand.created_by.display_name.toLowerCase().indexOf(q) >= 0) ||
            (typeof demand.category === "object" && demand.category.label && demand.category.label.toLowerCase().indexOf(q) >= 0)
            //(demand.relatedThemes && demand.relatedThemes.map((t) => t.toLowerCase()).find((t) => t === q)
        );
    }

    demands = demands.filter(function (item, pos) {
        return demands.indexOf(item) === pos;
    });


    return demands
};

export const getAllDemands = (state, includeChildrenDemand) => {
    const ongoing = getDemands(state, {
        status: 'ongoing',
        includeChildrenDemand: includeChildrenDemand
    });
    const completed = getDemands(state, {
        status: 'completed',
        includeChildrenDemand: includeChildrenDemand
    });
    return [...ongoing, ...completed];
};

export const getRelatedDemands = (state, link) => {

    if (!link) {
        return [];
    }

    let demands = getAllDemands(state, true);
    demands = demands.filter(d => d.link === link);

    demands = demands.filter(function (item, pos) {
        return demands.indexOf(item) === pos;
    });
    return demands;
};

const getDemandsByStatus = (state, status) =>
    state.entities.demands.listByStatus[status];

const getDemandsByStatusAndFilter = (state, status, filter) =>
    getDemandsByStatus(state, status)[filter];

export const getDemand = (state, id) =>
    state.entities.demands.byId[id];

