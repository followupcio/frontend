export const getSearchUsersQuery = (state) =>
    state.entities.searchUsers.list.query;

export const getIsFetching = (state) =>
    state.entities.searchUsers.list.isFetching;

export const getSearchErrorMessage = (state) =>
    state.entities.searchUsers.list.errorMessage;

export const getSearchUser = (state, id) =>
    state.entities.searchUsers.byId[id];

export const getSearchUsers = (state) =>
    state.entities.searchUsers.list.ids && state.entities.searchUsers.list.ids.map((id) => getSearchUser(state, id));
