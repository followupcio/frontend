export const getSdgModel = (state, id) =>
    state.entities.sdg.byId[id];

export const getSdg = (state) =>
    state.entities.sdg.list.ids && state.entities.sdg.list.ids.map((id) => getSdgModel(state, id));

export const getSpModel = (state, id) =>
    state.entities.sp.byId[id];

export const getSp = (state) =>
    state.entities.sp.list.ids && state.entities.sp.list.ids.map((id) => getSpModel(state, id));

export const getCabinetModel = (state, id) =>
    state.entities.cabinet.byId[id];

export const getCabinet = (state) =>
    state.entities.cabinet.list.ids && state.entities.cabinet.list.ids.map((id) => getCabinetModel(state, id));

export const getDdoModel = (state, id) =>
    state.entities.ddo.byId[id];

export const getDdo = (state) =>
    state.entities.ddo.list.ids && state.entities.ddo.list.ids.map((id) => getDdoModel(state, id));

export const getDdnModel = (state, id) =>
    state.entities.ddn.byId[id];

export const getDdn = (state) =>
    state.entities.ddn.list.ids && state.entities.ddn.list.ids.map((id) => getDdnModel(state, id));

export const getDdpModel = (state, id) =>
    state.entities.ddp.byId[id];

export const getDdp = (state) =>
    state.entities.ddp.list.ids && state.entities.ddp.list.ids.map((id) => getDdpModel(state, id));

export const getEsModel = (state, id) =>
    state.entities.es.byId[id];

export const getEs = (state) =>
    state.entities.es.list.ids && state.entities.es.list.ids.map((id) => getEsModel(state, id));

export const getSp1Model = (state, id) =>
    state.entities.sp1.byId[id];

export const getSp1 = (state) =>
    state.entities.sp1.list.ids && state.entities.sp1.list.ids.map((id) => getSp1Model(state, id));

export const getSp2Model = (state, id) =>
    state.entities.sp2.byId[id];

export const getSp2 = (state) =>
    state.entities.sp2.list.ids && state.entities.sp2.list.ids.map((id) => getSp2Model(state, id));

export const getSp3Model = (state, id) =>
    state.entities.sp3.byId[id];

export const getSp3 = (state) =>
    state.entities.sp3.list.ids && state.entities.sp3.list.ids.map((id) => getSp3Model(state, id));

export const getSp4Model = (state, id) =>
    state.entities.sp4.byId[id];

export const getSp4 = (state) =>
    state.entities.sp4.list.ids && state.entities.sp4.list.ids.map((id) => getSp4Model(state, id));

export const getSp5Model = (state, id) =>
    state.entities.sp5.byId[id];

export const getSp5 = (state) =>
    state.entities.sp5.list.ids && state.entities.sp5.list.ids.map((id) => getSp5Model(state, id));