export const getIds = (state) =>
    state.entities.countries.list.ids;

export const getIsFetching = (state) =>
    state.entities.countries.list.isFetching;

export const getErrorMessage = (state) =>
    state.entities.countries.list.errorMessage;

export const getCountry = (state, id) =>
    state.entities.countries.byId[id];

export const getCountries = (state) =>
    state.entities.countries.list.ids && state.entities.countries.list.ids.map((id) => getCountry(state, id));
