export const getIsFetching = (state) =>
    state.entities.users.list.isFetching;

export const getErrorMessage = (state) =>
    state.entities.users.list.errorMessage;

export const getUser = (state, id) =>
    state.entities.users.byId[id];

export const getUsers = (state) =>
    state.entities.users.list.ids && state.entities.users.list.ids.map((id) => getUser(state, id));
