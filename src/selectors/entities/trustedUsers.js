export const getIsFetching = (state) =>
    state.entities.trustedUsers.list.isFetching;

export const getErrorMessage = (state) =>
    state.entities.trustedUsers.list.errorMessage;

export const getTrustedUser = (state, id) =>
    state.entities.trustedUsers.byId[id];

export const getTrustedUsers = (state) =>
    state.entities.trustedUsers.list.ids && state.entities.trustedUsers.list.ids.map((id) => getTrustedUser(state, id));
