import {LOGIN_ERROR, LOGIN_SUCCESS} from '../containers/Pages/Login/constants';
import {LOGOUT_SUCCESS} from '../containers/Pages/Logout/constants';

const initialState = {};

function tokenReducer(state = initialState, action) {

    switch (action.type) {
        case LOGIN_SUCCESS:
            return {...state, ...action.response};
        case LOGIN_ERROR:
            return {error: action.message};
        case LOGOUT_SUCCESS:
            return initialState;
        default:
            return state;
    }
}

export default tokenReducer;