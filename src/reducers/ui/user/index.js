import {combineReducers} from 'redux';

const home = () => {
    const completedStatus = (state = {}, action) => {
        switch (action.type) {
            case 'SET_USER_COMPLETED_DEMANDS_FILTERS':
                return action.filters;
            default:
                return state;
        }
    };

    const ongoingStatus = (state = {}, action) => {
        switch (action.type) {
            case 'SET_USER_ONGOING_DEMANDS_FILTERS':
                return action.filters;
            default:
                return state;
        }
    };

    return combineReducers({
        ongoing: combineReducers({
            filters: ongoingStatus,
        }),
        completed: combineReducers({
            filters: completedStatus,
        }),
    });
};

export default home();
