import {combineReducers} from 'redux';

const search = () => {
    const demand = (state = {}, action) => {
        switch (action.type) {
            case 'SET_SEARCH_DEMAND_FILTERS':
                return action.filters;
            default:
                return state;
        }
    };

    return combineReducers({
        demand: combineReducers({
            filters: demand,
        }),
    });
};

export default search();
