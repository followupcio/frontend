import {combineReducers} from 'redux';

const app = () => {
    const style = (state = 'table', action) => {
        switch (action.type) {
            case 'SET_APP_STYLE':
                return action.style;
            default:
                return state;
        }
    };

    return combineReducers({
        style,
    });
};

export default app();
