const initialState = {
    demands: null
};

const tabsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_SELECTED_TAB':
            return {
                ...state,
                [action.namespace]: action.tab
            };

        default:
            return state;
    }
};

export default tabsReducer;