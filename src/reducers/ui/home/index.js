import {combineReducers} from 'redux';
import LocalStorage from 'localStorage'
import chart from './chart'


const home = () => {
    const completedStatus = (state = {}, action) => {
        switch (action.type) {
            case 'SET_HOME_COMPLETED_DEMANDS_FILTERS':
                return action.filters;
            default:
                return state;
        }
    };

    const ongoingStatus = (state = {}, action) => {
        switch (action.type) {
            case 'SET_HOME_ONGOING_DEMANDS_FILTERS':
                return action.filters;
            default:
                return state;
        }
    };

    const drawer = (state = LocalStorage.getItem("drawer") !== "closed", action) => {
        switch (action.type) {
            case 'TOGGLE_DRAWER':
                return !state;
            default:
                return state;
        }
    };

    return combineReducers({
        ongoing: combineReducers({
            filters: ongoingStatus,
        }),
        completed: combineReducers({
            filters: completedStatus,
        }),
        chart: chart(),
        drawer
    });
};

export default home();
