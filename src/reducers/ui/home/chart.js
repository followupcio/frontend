import {combineReducers} from 'redux';

const chart = () => {
    const stats = (state = null, action) => {
        switch (action.type) {
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_SUCCESS' :
                return action.response;
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_REQUEST' :
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_FAILURE' :
                return null;
            case 'ADD_DEMAND_SUCCESS' :
                return {};
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        switch (action.type) {
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_FAILURE' :
                return action.message;
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_REQUEST' :
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        switch (action.type) {
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_REQUEST':
                return true;
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_SUCCESS':
            case 'FETCH_HOME_DEMAND_STATUS_VALUES_FAILURE':
                return false;
            default:
                return state;
        }
    };

    return combineReducers({
        stats,
        isFetching,
        errorMessage,
    });
};

export default chart;
