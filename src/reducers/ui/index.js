import {combineReducers} from 'redux';

import app from './app/';
import home from './home/';
import search from './search/';
import user from './user/';
import modal from './modal';
import tabs from './tabs';

const state = combineReducers({
    modal,
    home,
    user,
    app,
    search,
    tabs
});

export default state;
