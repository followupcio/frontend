const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_CATEGORIES_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.categories,
            };

        default:
            return state;
    }
};

export default byId;
