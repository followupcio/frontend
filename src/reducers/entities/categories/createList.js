import {combineReducers} from 'redux';

const createList = () => {
    const ids = (state = [], action) => {
        switch (action.type) {
            case 'FETCH_CATEGORIES_SUCCESS' :
                return action.response.result;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        switch (action.type) {
            case 'FETCH_CATEGORIES_FAILURE' :
                return action.message;
            case 'FETCH_CATEGORIES_REQUEST' :
            case 'FETCH_CATEGORIES_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        switch (action.type) {
            case 'FETCH_CATEGORIES_REQUEST':
                return true;
            case 'FETCH_CATEGORIES_SUCCESS':
            case 'FETCH_CATEGORIES_FAILURE':
                return false;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
    });
};

export default createList;
