import {combineReducers} from 'redux';

const createDemandList = () => {
    const ids = (state = [], action) => {

        switch (action.type) {
            case 'FETCH_DEMANDS_SUCCESS' :
                return action.response.result;
            case 'ADD_DEMAND_SUCCESS' :
                return [action.response.result, ...state];
            case 'EDIT_DEMAND_SUCCESS' :
                let id = action.response.result;
                return state.indexOf(id) < 0 ? [id, ...state] : state;
            case 'DELETE_DEMAND_SUCCESS' :
                return [];
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {

        switch (action.type) {
            case 'FETCH_DEMANDS_FAILURE' :
                return action.message;
            case 'FETCH_DEMANDS_REQUEST' :
            case 'FETCH_DEMANDS_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {

        switch (action.type) {
            case 'FETCH_DEMANDS_REQUEST':
                return true;
            case 'FETCH_DEMANDS_SUCCESS':
            case 'FETCH_DEMANDS_FAILURE':
                return false;
            default:
                return state;
        }
    };

    function stats(state = {}, action) {
        switch (action.type) {
            case 'FETCH_DEMANDS_STATS_SUCCESS':
                return action.response;
            default:
                return state;
        }
    }

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
        stats
    });
};

export default createDemandList;