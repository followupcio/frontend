const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_DEMANDS_SUCCESS' :
        case 'FETCH_DEMAND_SUCCESS' :
        case 'FETCH_CHILDREN_DEMAND_SUCCESS' :
        case 'TOGGLE_DEMAND_STATUS_SUCCESS' :
        case 'TOGGLE_DEMAND_FINAL_APPROVAL_SUCCESS' :
        case 'ADD_ACTION_TAKEN_SUCCESS':
        case 'REJECT_DEMAND_SUCCESS':
        case 'APPROVE_DEMAND_SUCCESS':
        case 'REOPEN_DEMAND_SUCCESS':
        case 'EDIT_DEMAND_SUCCESS':
        case 'ARCHIVE_DEMAND_SUCCESS':
        case 'UNARCHIVE_DEMAND_SUCCESS':
        case 'READ_DEMAND_SUCCESS':
        case 'SUBMIT_DEMAND_SUCCESS':
        case 'DRAFT_SUBMIT_DEMAND_SUCCESS':
        case 'COMMENT_DEMAND_SUCCESS':
        case 'REMOVE_EVENT_SUCCESS':
        case 'EDIT_EVENT_SUCCESS':
        case 'ADD_DEMAND_SUCCESS' :
        case 'UNDELETE_DEMAND_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.demands,
            };
        case 'DELETE_DEMAND_SUCCESS':
            return {};
        default:
            return state;
    }
};

export default byId;
