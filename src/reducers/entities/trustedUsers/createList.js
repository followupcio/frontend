import {combineReducers} from 'redux';

const createList = () => {
    const ids = (state = null, action) => {
        switch (action.type) {
            case 'FETCH_TRUSTED_USERS_SUCCESS' :
                return action.response.result;
            case 'ADD_DEMAND_SUCCESS' :
            case 'DELETE_DEMAND_SUCCESS' :
            case 'UNDELETE_DEMAND_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        switch (action.type) {
            case 'FETCH_TRUSTED_USERS_FAILURE' :
                return action.message;
            case 'FETCH_TRUSTED_USERS_REQUEST' :
            case 'FETCH_TRUSTED_USERS_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        switch (action.type) {
            case 'FETCH_TRUSTED_USERS_REQUEST':
                return true;
            case 'FETCH_TRUSTED_USERS_SUCCESS':
            case 'FETCH_TRUSTED_USERS_FAILURE':
                return false;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
    });
};

export default createList;
