const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_TRUSTED_USERS_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.trustedUsers,
            };

        default:
            return state;
    }
};

export default byId;
