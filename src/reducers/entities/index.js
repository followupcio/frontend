import {combineReducers} from 'redux';

import demandsById from './demands/byId.js';
import createDemandLists from './demands/createList.js';

import usersById from './users/byId.js';
import createUserList from './users/createList.js';

import searchUsersById from './searchUsers/byId.js';
import createSearchUserList from './searchUsers/createList.js';

import categoriesById from './categories/byId.js';
import createCategoryList from './categories/createList.js';

import pinDraft from './draft/pinDraft.js';

import timeframeById from './timeFrame/byId.js';
import createTimeframeList from './timeFrame/createList.js';

import trustedUsersById from './trustedUsers/byId.js';
import createTrustedUsersList from './trustedUsers/createList.js';

const state = combineReducers({

    trustedUsers: combineReducers({
        byId: trustedUsersById,
        list: createTrustedUsersList(),
    }),
    timeFrames: combineReducers({
        byId: timeframeById,
        list: createTimeframeList(),
    }),
    demands: combineReducers({
        list: createDemandLists(),
        byId: demandsById,
    }),
    users: combineReducers({
        list: createUserList(),
        byId: usersById,
    }),

    searchUsers: combineReducers({
        list: createSearchUserList(),
        byId: searchUsersById,
    }),
    /* countries: combineReducers({
     list: createCountryList(),
     byId: countriesById,
     }),*/
    categories: combineReducers({
        list: createCategoryList(),
        byId: categoriesById,
    }),
    draft: pinDraft
});

export default state;
