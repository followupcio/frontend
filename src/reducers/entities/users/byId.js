const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_USERS_SUCCESS' :
        case 'FETCH_USER_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.users,
            };

        default:
            return state;
    }
};

export default byId;
