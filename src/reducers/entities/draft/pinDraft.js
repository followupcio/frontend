const pinDraft = (state = null, action) => {

    switch (action.type) {
        case 'PIN_DRAFT':
            return action.id;
        case 'UNPIN_DRAFT':
            return null;
        default:
            return state;
    }
};

export default pinDraft;
