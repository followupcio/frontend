const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_COUNTRIES_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.countries,
            };

        default:
            return state;
    }
};

export default byId;
