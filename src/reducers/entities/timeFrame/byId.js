const byId = (state = {}, action) => {
    switch (action.type) {
        case 'FETCH_TIME_FRAMES_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.timeFrames,
            };

        default:
            return state;
    }
};

export default byId;
