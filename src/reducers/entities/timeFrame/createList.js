import {combineReducers} from 'redux';

const createList = () => {
    const ids = (state = [], action) => {

        switch (action.type) {
            case 'FETCH_TIME_FRAMES_SUCCESS' :
                return [...state, ...action.response.result];
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {

        switch (action.type) {
            case 'FETCH_TIME_FRAMES_FAILURE' :
                return action.message;
            case 'FETCH_TIME_FRAMES_REQUEST' :
            case 'FETCH_TIME_FRAMES_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {

        switch (action.type) {
            case 'FETCH_TIME_FRAMES_REQUEST':
                return true;
            case 'FETCH_TIME_FRAMES_SUCCESS':
            case 'FETCH_TIME_FRAMES_FAILURE':
                return false;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
    });
};

export default createList;
