const byId = (state = {}, action) => {
    switch (action.type) {
        case 'SEARCH_USERS_SUCCESS' :
        case 'SEARCH_USER_SUCCESS' :
            return {
                ...state,
                ...action.response.entities.searchUsers,
            };

        default:
            return state;
    }
};

export default byId;
