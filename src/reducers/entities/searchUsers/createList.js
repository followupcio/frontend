import {combineReducers} from 'redux';

const createList = () => {
    const query = (state = "", action) => {
        switch (action.type) {
            case 'SET_SEARCH_USERS_QUERY' :
                return action.query;
            default:
                return state;
        }
    };

    const ids = (state = null, action) => {
        switch (action.type) {
            case 'SEARCH_USERS_SUCCESS' :
                return action.response.result;
            default:
                return state;
        }
    };

    const errorMessage = (state = null, action) => {
        switch (action.type) {
            case 'SEARCH_USERS_FAILURE' :
                return action.message;
            case 'SEARCH_USERS_REQUEST' :
            case 'SEARCH_USERS_SUCCESS' :
                return null;
            default:
                return state;
        }
    };

    const isFetching = (state = false, action) => {
        switch (action.type) {
            case 'SEARCH_USERS_REQUEST':
                return true;
            case 'SEARCH_USERS_SUCCESS':
            case 'SEARCH_USERS_FAILURE':
                return false;
            default:
                return state;
        }
    };

    return combineReducers({
        ids,
        isFetching,
        errorMessage,
        query,
    });
};

export default createList;
