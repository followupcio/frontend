/*
 *  In Redux, all reducers get called regardless of the action, so inside each one
 *  return the original state if the action is not applicable.
 *
 *  Actions describe the fact that something happened, but don't specify how the application's state changes in response.
 *  This is the job of reducers.
 * */
import {combineReducers} from 'redux'
import ui from './ui/';
import entities from './entities/';
import token from './token';
import user from './user';
import {reducer as formReducer} from 'redux-form';
import {reducer as notifications} from 'react-notification-system-redux';

import {connectRouter} from 'connected-react-router'

export default (history) => combineReducers({

    // Routing
    router: connectRouter(history),

    // Notifications
    notifications,

    login: (state = {}) => state,

    ui,

    form: formReducer,

    entities,

    token,

    user
});

