import {LOGOUT_SUCCESS} from '../containers/Pages/Logout/constants';

const initialState = {};

function userReducer(state = initialState, action) {

    switch (action.type) {
        case "FETCH_AUTH_USER_SUCCESS":
            const entities = action.response.entities;
            const id = action.response.result;
            return entities.users[id];
        case "FETCH_AUTH_USER_ERROR":
            return {error: action.message};
        case LOGOUT_SUCCESS:
            return initialState;
        default:
            return state;
    }
}

export default userReducer;