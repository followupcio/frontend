import {applyMiddleware, compose, createStore} from 'redux'
import {routerMiddleware} from 'connected-react-router'
import thunk from 'redux-thunk'
import createBrowserHistory from 'history/createBrowserHistory'
import rootReducer from './reducers/'
import analytics from './middlewares/analytics/index'

const createHistory = createBrowserHistory;

export const history = createHistory({
    basename: "/"
});

const initialState = {};
const enhancers = [];
const middleware = [
    thunk,
    analytics,
    routerMiddleware(history)
];

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
}

const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

const store = createStore(rootReducer(history),
//persistedReducer,
    initialState, composedEnhancers);

export default store;
