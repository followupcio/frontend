import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router'

import {setupInterceptors} from './conf/axios'
import store, {history} from './store'

import App from './containers/App';
// CSS
import 'bootstrap/dist/css/bootstrap.css';
import 'react-table/react-table.css';
import './css/index.css';

import {unregister} from './serviceWorker';

setupInterceptors(store);

const render = () => {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App/>
            </ConnectedRouter>
        </Provider>,
        document.getElementById('follow-up-main')
    );
};

render();

unregister();