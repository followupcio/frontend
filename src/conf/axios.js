import axios from 'axios'
import LocalStorage from 'localStorage';
import {history} from "../store";
import * as ROUTES from './routes'
import {push} from 'react-router-redux'
import {BACKEND_ENDPOINT, API_PREFIX} from "./index";

export const BACKEND_BASE_URL = BACKEND_ENDPOINT;
export const API_BASE_URL = BACKEND_ENDPOINT + API_PREFIX;

let _store;

/**
 * Sets the default URL for API Calls
 * and sets fixed headers for JWT Auth
 * @type {[type]}
 */
const axiosInstance = axios.create({
    baseURL: API_BASE_URL,
    headers: {
        'Accept': 'application/json',
        'Authorization': ''
    }
});

export const setupInterceptors = (store) => {

    /**
     * Intercepts every request, before it goes out
     * @param  {[type]} config) {} [description]
     * @return {[type]}         [description]
     */
    axiosInstance.interceptors.request.use(function (config) {

        const token = LocalStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = token
        }

        return config
    });

    axiosInstance.interceptors.response.use(function (response) {
        return response;
    }, function (error) {

        if (error.response.status === 401) {
            LocalStorage.clear();

            /*
            * If you are in the login page, do not logout.
            * This will preserve the error message of the login form
            * */
            if (window.location.pathname.indexOf(ROUTES.LOGIN) < 0) {
                store.dispatch({
                    type: "HIDE_MODAL"
                });
                store.dispatch(push(ROUTES.LOGOUT));
            }

        }

        // Do something with response error
        return Promise.reject(error);
    });

};

export const forceLogout = () => {
    if (!!_store && typeof _store === "function") {
        _store.dispatch({
            type: "HIDE_MODAL"
        });
        _store.dispatch(push(ROUTES.LOGOUT))
    } else {
        history.push(ROUTES.LOGOUT);
    }

};

export default axiosInstance;
