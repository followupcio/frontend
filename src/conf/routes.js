/**
 * Router's routes
 */
export const HOME = '/';
export const MY_PAGE = '/my-page';
export const DEMAND_DETAILS = '/demands/:id';
export const MULTIPLE_DEMAND_DETAILS = '/demands/:id/multiple';
export const SEARCH = '/search';
export const DASHBOARD = '/dashboard';
export const USER_DETAILS = '/people/:user';
export const LOGIN = '/login';
export const LOGOUT = '/logout';
export const ANY = '/logout';