import axios, {API_BASE_URL, forceLogout} from '../conf/axios';
import Axios, {CancelToken} from 'axios';
import LocalStorage from 'localStorage'
import * as ROLES from '../conf/roles'
import Moment from 'moment'
import {getMostRecentEvent} from '../utils'
import values from 'lodash/values';

/*
* To review
* - logic in fetchDemand() and addDemand() to convert draft_assigned_to to assigned_to
* */


// ============== Common

const processAttachments = ({demand_id, attachments = [], event}) => {

    const attachmentsToUpload = attachments.filter(a => !a.id);
    const attachmentsToLink = attachments.filter(a => !!a.id);

    return addAttachments({event, attachments: attachmentsToUpload})
        .then(() => {
            return linkAttachments({event, attachments: attachmentsToLink})
                .then(() => {
                    return fetchDemand(demand_id)
                })
        });
};

// ============== Authentication
export const login = (credentials) =>
    axios.post('employees/login', credentials);

export const logout = () =>
    axios.post('employees/logout');

export const fetchAuthUser = () =>
    fetchUser(LocalStorage.getItem("userId"))
        .catch(forceLogout);

export const ping = () => {
    //return axios.get('ping/')
    return fetchCountries()
};

// ============== Resources

export const fetchCountries = () => {
    return fetchResource("COUNTRY", undefined);
};

export const fetchCategories = () => {
    return fetchResource("CATEGORY", {nlike: ""}, ["children"]);
};

export const fetchTimeFrames = () => {
    return fetchResource("TIMEFRAME", {nlike: ""}, ["children"]);
};

const fetchResource = (resource, parent_id, include) => {

    let where = {resource};
    if (parent_id) {
        where = {...where, parent_id}
    }
    const filter = {
        where,
        order: "label ASC",
        include
    };

    return axios.get('resources?filter=' + JSON.stringify(filter))
        .then((response) => {
            return response.data
        })

};

// ============== Demands
const includeWithDemand = [
    {relation: "issuer"},
    {relation: "receiver"},
    {relation: "readBy"},
    {relation: "iterations"},
    //{relation: "attachments"},
    {
        relation: "children", scope: {
            include: [
                //{relation: "attachments"},
                {relation: "issuer"},
                {relation: "iterations"},
                {relation: "readBy"},
                {relation: "receiver"},
                {relation: "children"},
                {relation: "requester"},
                {relation: "category"},
                {relation: "subcategory"},
                {relation: "events", scope: {include: ["author", "attachments"]}},
            ]
        }
    },
    {relation: "requester"},
    {relation: "category"},
    {relation: "subcategory"},
    {relation: "events", scope: {include: ["author", "attachments", "iteration"]}},
];
const normalizeDemand = (dirty) => {

    let model = {
        id: dirty.id,
        title: dirty.title,
        description: dirty.description,
        assigned_to: dirty.assigned_to,
        deadline: dirty.deadline,
        category_id: dirty.category_id,
        subcategory_id: dirty.subcategory_id,
        created_by: dirty.created_by,
        assigned_by: dirty.assigned_by,
        parent: dirty.parent,
        parent_id: dirty.parent_id,
        status: dirty.status,
        draft_assigned_to: dirty.draft_assigned_to,
        repeat: dirty.repeat,
        weeklyFrequency: dirty.weeklyFrequency,
        monthlyFrequency: dirty.monthlyFrequency,
        repeatDates: dirty.repeatDates,
        visibleToOtherAssignees: dirty.visibleToOtherAssignees
    };

    return model;
};
const generateFilter = (status, filters = {}, authUser, user, skip, limit, order) => {
    const isDirector = authUser.role.indexOf(ROLES.DIRECTOR) > -1;
    const isFollower = authUser.role.indexOf(ROLES.FOLLOWER) > -1;
    const isDG = authUser.role.indexOf(ROLES.DG) > -1;
    const authUserId = authUser.id;

    const isFiltered = !!values(filters).filter(v => !!v).length;
    let and;

    let filter = {};
    let where = {};

    //My page
    if (user === authUserId) {
        if (isDirector) {
            and = [
                {
                    or: [{assigned_to: authUserId},
                        {assigned_by: authUserId},
                        {created_by: authUserId}]
                }
            ];

            if (!isFiltered) {
                and.push({
                    or: [
                        {parent: true},
                        {parent_id: {nlike: ""}}
                    ]
                })
            }

            where.and = and;
        } else if (isFollower || isDG) {
            where = {
                or: [
                    {parent: true},
                    {parent_id: {nlike: ""}},
                ]
            };
        } else {
            where.assigned_to = authUserId;
        }
    } else {

        // Home page
        if (!user) {
            if (isDirector) {

                and = [
                    {
                        or: [{assigned_to: authUserId},
                            {assigned_by: authUserId},
                            {created_by: authUserId}]
                    }

                ];

                if (!isFiltered) {
                    and.push({
                        or: [
                            {parent: true},
                            {
                                or: [
                                    {parent_id: {nlike: ""}},
                                    {assigned_to: authUserId}
                                ]
                            }
                        ]
                    })
                }


                where.and = and
            } else if (isFollower || isDG) {

                if (filters.assigned_to) {
                    where.assigned_to = filters.assigned_to
                } else {

                    where = isFiltered ? {} : {
                        or: [
                            {parent: true},
                            {parent_id: {nlike: ""}},
                        ]
                    };
                }

            } else {
                where.assigned_to = authUserId;
            }
        } else {
            // people page
            if (isDirector) {
                where.assigned_by = authUserId;
                where.assigned_to = user;
            } else if (isFollower || isDG) {
                where.assigned_to = user;
            } else {
                where.assigned_by = authUserId;
                where.assigned_to = user;
            }
        }
    }

    where.status = status && String(status).toUpperCase();
    where.timeframe = filters.timeFrame;
    where.category_id = filters.category;
    where.subcategory_id = filters.subcategory;

    if (filters.from || filters.to) {
        const between = [];

        between.push(filters.from ? Moment(filters.from).startOf('day').toISOString() : Moment(new Date(1960, 0, 1, 0, 0, 0, 0)).toISOString());
        between.push(filters.to ? Moment(filters.to).startOf('day').add(24, 'hours').toISOString() : Moment(new Date(2045, 0, 1, 0, 0, 0, 0)).toISOString());

        where.deadline = {between};
    }

    where.q = filters.q;

    filter.where = where;

    filter.include = includeWithDemand;
    filter.skip = skip;
    filter.limit = limit;

    filter.order = !!order ? order : "created_at DESC";

    return filter;

};
const removeAttachments = ({event, attachments}) => {
    return axios.delete('events/' + event.id + '/attachments/')
};
const addAttachments = ({demand = {}, event = {}, attachments}) => {

    if (!attachments || (Array.isArray(attachments) && attachments.length === 0)) {
        return new Promise((resolve) => resolve(demand));
    }

    let promises = [];

    attachments.forEach(a => {

        const data = new FormData();
        data.append('file', a);
        let event_id;
        if (demand && Array.isArray(demand.events)) {
            let creation = getMostRecentEvent({
                demand,
                types: ["DRAFT_SUBMIT", "SUBMIT", "DRAFT_UPDATE", "DRAFT", "CREATE"]
            }).event;
            event_id = creation.id
        }

        if (event && event.id) {
            event_id = event.id
        }
        data.append("event_id", event_id);

        promises.push(axios.post('attachments/upload', data));

    });

    return Axios.all(promises)
};
const linkAttachments = ({event = {}, attachments = {}, demand}) => {
    if (!attachments || (Array.isArray(attachments) && attachments.length === 0)) {
        return new Promise((resolve) => resolve(demand));
    }

    let promises = [];
    attachments.forEach(a => {

        let event_id;

        if (demand && Array.isArray(demand.events)) {

            let creation = getMostRecentEvent({
                demand,
                types: ["DRAFT_SUBMIT", "SUBMIT", "DRAFT_UPDATE", "DRAFT", "CREATE"]
            }).event;

            event_id = creation.id;
        }

        if (event && event.id) {
            event_id = event.id;
        }

        const data = {
            ...a,
            event_id
        };

        delete data.id;

        promises.push(axios.post('attachments/', data));

    });

    return Axios.all(promises)
};
const createDemandPromise = ({model}) => {

    return axios.post('demands/create', normalizeDemand(model))
        .then((response) => {
            return response.data;
        })
};

export const activateDraftDemand = (event, demand) => {

    /*
     * If demand contains attachments the activation comes from the modal
     * so the most updated set of attachments is that one
     * */
    let attachments = Array.isArray(demand.attachments) ?
        demand.attachments :
        getMostRecentEvent({
            demand,
            types: ["DRAFT", "DRAFT_UPDATE"]
        }).attachments;

    //harmonize different format if model comes from the submit button or from the modal
    demand.assigned_to = demand.assigned_to.map(a => typeof a === "object" ? a.id : a);

    return axios.patch('demands/' + demand.id + "/activateDraft", normalizeDemand(demand))
        .then(response => {
            const d = response.data;

            const createEvent = d.events.find(e => e.type === "CREATE");

            return processAttachments({demand_id: d.id, attachments, event: createEvent});
        })
};

export const editDemand = (demand) => {

    if (demand.status && demand.status.toUpperCase() === "DRAFT") {
        demand.draft_assigned_to = demand.assigned_to;
        delete demand.assigned_to;
    }

    if (Array.isArray(demand.assigned_to)) {
        demand.assigned_to = demand.assigned_to[0];
    }

    if (!!demand.parent) {
        demand.assigned_to = null;
    }

    const attachments = demand.attachments || [];
    delete demand.attachments;
    const id = demand.id;
    delete demand.id;

    return axios.patch('demands/' + id + '/update', normalizeDemand(demand))
        .then(response => {
            return fetchDemand(response.data.id)
                .then(fetched => {
                    return processAttachments({demand_id: fetched.id, attachments, event: fetched.events[0]})
                })
        });
};

export const addDemand = (model) => {

    if (model.status && model.status.toUpperCase() === "DRAFT") {
        model.draft_assigned_to = model.assigned_to.slice(0);
        model.assigned_to = null;
    }

    const attachments = model.attachments;
    delete model.attachments;

    return createDemandPromise({model})
        .then(demand => {
            return addAttachments({demand: demand, attachments})
                .then(() => {
                    return fetchDemand(demand.id);
                });
        })

};

export const exportDemands = (status, filters = {}, authUser, user) => {

    const filter = generateFilter(status, filters, authUser, user);
    const URL = 'demands/download?type=excel&filter=' + JSON.stringify(filter);

    return axios.get(URL)
        .then((response) => {

            if (response.data && response.data.url) {
                response.data.url = API_BASE_URL + response.data.url
            }

            return response.data;
        })
};

export const fetchDemandsStats = (authUser, user) => {

    const filter = generateFilter(undefined, {}, authUser, user).where;

    return axios.get('demands/stats?where=' + JSON.stringify(filter))
        .then((response) => {
            return response.data;
        })
};

export const fetchDemand = (id) =>
    axios.get('demands/' + id + '?filter=' + JSON.stringify({include: includeWithDemand}))
        .then((response) => {

            const model = response.data;
            if (model.status && model.status.toUpperCase() === "DRAFT") {
                model.assigned_to = Array.isArray(model.draft_assigned_to) && model.draft_assigned_to.length === 1 ? model.draft_assigned_to[0] : model.draft_assigned_to;
                delete model.draft_assigned_to;
            }
            return model
        });

export const fetchDemands = (status, filters = {}, authUser, user, skip, limit, order) => {

    const filter = generateFilter(status, filters, authUser, user, skip, limit, order);
    const URL = 'demands?filter=' + JSON.stringify(filter);
    const source = CancelToken.source();

    return {
        promise: axios.get(URL, {
            cancelToken: source.token
        }).then((response) => {
            return response.data
        }),
        source: source
    }


};

export const deletePermanentlyDemand = (event, demand) =>
    axios.delete('demands/' + demand.id, event);

export const deleteDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/delete", event);

export const undeleteDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/undelete", event);

export const archiveDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/archive", event);

export const unarchiveDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/unarchive", event);

// ============== Users
export const fetchUserStats = (authUser) => {
    const filter = generateFilter("ONGOING", {}, authUser, undefined, undefined, undefined);
    const URL = 'employees/' + authUser.id + '/stats?filter=' + JSON.stringify(filter);
    return axios.get(URL)
        .then((response) => {
            return response.data
        });
};

export const searchUsers = (query) =>
    axios.get('employees/search?last_name=' + encodeURI(query))
        .then((response) => {
            return response.data;
        });

export const fetchUsers = () =>
    axios.get('employees')
        .then((response) => {
            return response.data;
        });

export const fetchUser = (id) =>
    axios.get('employees/' + id)
        .then((response) => {
            return response.data;
        });

// ============== Trusted users
const includeWithTrustedUsers = {
    include: {
        relation: "received",
        scope: {where: {status: "ONGOING"}} //need by server side filtering for stats
    }
};

export const fetchTrustedUsers = (id) =>
    axios.get('employees/' + id + "/trustedList?filter=" + JSON.stringify(includeWithTrustedUsers))
        .then((response) => {
            return response.data;
        });

export const addTrustedUser = (email) =>
    axios.post('employees/add', {email})
        .then((response) => {
            return response.data;
        });

export const removeTrustedUser = (authUserId, id) =>
    axios.delete(`employees/${id}/trustedBy/rel/${authUserId}`)
        .then((response) => {
            return response.data;
        });

// ============== Events

export const addComment = (event, demand) => {

    return axios.patch('demands/' + demand.id + "/comment", event)
        .then((response) => {
            const d = response.data;

            let events = d.events;

            const attachments = event.attachments;

            return addAttachments({event: events[0], attachments})
                .then(() => {
                    return fetchDemand(demand.id)
                });
        });
};

export const removeEvent = (id, demand) =>
    axios.put('demands/' + demand.id + "/events/" + id, {removed: true})
        .then(() => {
            return fetchDemand(demand.id)
        });

export const approveDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/approve", event);

export const rejectDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/reject", event);

export const reopenDemand = (event, demand) =>
    axios.patch('demands/' + demand.id + "/reopen", event);

export const editEvent = (event, demand) => {
    const model = {description: event.description, edited: true};
    const attachments = event.attachments;

    return axios.put('demands/' + demand.id + "/events/" + event.id, model)
        .then((response) => {
            const e = {...response.data};
            return removeAttachments({event: e, attachments})
                .then(() => {
                    return processAttachments({demand_id: demand.id, attachments, event: e})
                })
        });
};

export const submitDemand = (event, demand) => {
    return axios.patch('demands/' + demand.id + "/submit", event)
        .then(response => {
            const d = response.data;
            return processAttachments({demand_id: d.id, attachments: event.attachments, event: d.events[0]})
        });
};

export const draftSubmitDemand = (event, demand) => {
    return axios.patch('demands/' + demand.id + "/submit/draft", event)
        .then(response => {
            const d = response.data;
            return processAttachments({demand_id: d.id, attachments: event.attachments, event: d.events[0]})
        });
};

// ============== Iterations

export const approveIteration = (event, iteration) => {
    return axios.patch('iterations/' + iteration.id + "/approve", event)
        .then((response) => {
            return fetchDemand(response.data.demand_id)
        });
};

export const rejectIteration = (event, iteration) =>
    axios.patch('iterations/' + iteration.id + "/reject", event)
        .then((response) => {
            return fetchDemand(response.data.demand_id)
        });

export const reopenIteration = (event, iteration) =>
    axios.patch('iterations/' + iteration.id + "/reopen", event)
        .then((response) => {
            return fetchDemand(response.data.demand_id)
        });

export const submitIteration = (event, iteration) => {

    return axios.patch('iterations/' + iteration.id + "/submit", event)
        .then((response) => {
            const demand_id = response.data.demand_id;
            return fetchDemand(demand_id)
                .then(demand => processAttachments({
                    demand_id: demand.id,
                    attachments: event.attachments,
                    event: demand.events[0]
                }));
        })
};
