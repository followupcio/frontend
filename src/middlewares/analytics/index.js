import GAEventTracker from "./GAEventTracker"

function createAnalyticsMiddleware(extraArgument) {

    const EventTracker = new GAEventTracker();

    return ({dispatch, getState}) => next => action => {
        if (typeof action === 'function') {
            return action(dispatch, getState, extraArgument);
        }

        switch (action && action.type && action.type.toUpperCase()) {
            case "@@ROUTER/LOCATION_CHANGE" :
                EventTracker.pageview(window.location.pathname + window.location.search);
                break;
            case "FETCH_DEMANDS_SUCCESS" :
                EventTracker.timing({
                    category: 'Assignments',
                    variable: 'load',
                    value: Math.round(action.timing), // in milliseconds
                });
                break;
            default:
                break;
        }

        return next(action);
    };
}

const analytics = createAnalyticsMiddleware();
analytics.withExtraArgument = createAnalyticsMiddleware;

export default analytics;



