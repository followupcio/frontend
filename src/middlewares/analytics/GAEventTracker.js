import ReactGA from 'react-ga';
import {GOOGLE_ANALYTICS_TRACKING_ID} from "../../conf";

export default class GAEventTracker {
    constructor() {
        ReactGA.initialize(GOOGLE_ANALYTICS_TRACKING_ID);
    }

    set(opts) {
        ReactGA.set(opts)

    }

    track(action, label) {
        ReactGA('send', 'event', 'Catalog', action, label);
    }

    pageview(location) {
        ReactGA.pageview(location);
    }

    timing(opts) {
        ReactGA.timing(opts);
    }
}