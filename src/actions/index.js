// event creators are functions that return an event
import * as fromDemands from './entities/demands/';
import * as fromIterations from './entities/iterations';
import * as fromUsers from './entities/users/';
import * as fromSearchUsers from './entities/searchUsers/';
import * as fromCategories from './entities/categories/';
import * as fromStatuses from './entities/timeFrames/';
import * as fromCountries from './entities/countries/';
import * as fromTrustedUsers from './entities/trustedUsers/';
import * as fromModal from './ui/modal';
import * as fromHome from './ui/home';
import * as fromTabs from './ui/tabs';
import LocalStorage from 'localStorage'
import Notifications from 'react-notification-system-redux';
import * as api from '../api'


// Demands
export const addDemand = (demand, dispatch) =>
    fromDemands.addDemand({dispatch, demand});

export const pinDraft = (id) => ({
    type: 'PIN_DRAFT',
    id: id
});

export const unpinDraft = () => ({
    type: 'UNPIN_DRAFT'
});


export const createDemandDraft = (demand, dispatch) => {
    const notificationOpts = {
        title: "Assignment saved as draft",
        message: "You can continue editing the assignment form the 'draft' tab of your Home.",
        position: 'tr',
        autoDismiss: 10
    };
    dispatch(Notifications.info(notificationOpts));
    return fromDemands.createDemandDraft({demand, dispatch});
};

export const onEditDraft = (demand, dispatch) =>
    fromDemands.onEditDraft({demand, dispatch});

export const fetchDemand = (id) => (dispatch, getState) =>
    fromDemands.fetchDemand({id, dispatch, getState});

export const fetchDemands = ({status, filters, authUser, user, skip, limit, order}) => (dispatch, getState) =>
    fromDemands.fetchDemands({status, filters, dispatch, authUser, user, getState, skip, limit, order});

export const exportDemands = ({status, filters, authUser, user}) => (dispatch, getState) =>
    fromDemands.exportDemands({status, filters, dispatch, authUser, user, getState});

export const fetchDemandsStats = ({authUser, user}) => (dispatch, getState) =>
    fromDemands.fetchDemandsStats({authUser, user, dispatch});

export const addComment = (event, demand, dispatch) =>
    fromDemands.addComment({event, demand, dispatch});

export const editDemand = (demand, dispatch) =>
    fromDemands.editDemand({demand, dispatch});

export const draftSubmitDemand = (event, demand, dispatch) =>
    fromDemands.draftSubmitDemand({event, demand, dispatch});

export const submitDemand = (event, demand, dispatch) =>
    fromDemands.submitDemand({event, demand, dispatch});

export const approveDemand = (event, demand, dispatch) => {

    const notificationOpts = {
        title: 'Assignment completed!',
        message: 'It will now be visible in the "completed" tab of you Home.',
        position: 'tr',
        autoDismiss: 10
    };
    dispatch(Notifications.info(notificationOpts));
    return fromDemands.approveDemand({event, demand, dispatch});

};

export const rejectDemand = (event, demand, dispatch) =>
    fromDemands.rejectDemand({event, demand, dispatch});

export const reopenDemand = (event, demand, dispatch) =>
    fromDemands.reopenDemand({event, demand, dispatch});

export const archiveDemand = (event, demand, dispatch) =>
    fromDemands.archiveDemand({event, demand, dispatch});

export const unarchiveDemand = (event, demand, dispatch) =>
    fromDemands.unarchiveDemand({event, demand, dispatch});

export const deleteDemand = (event, demand, dispatch) =>
    fromDemands.deleteDemand({event, demand, dispatch});

export const undeleteDemand = (event, demand, dispatch) =>
    fromDemands.undeleteDemand({event, demand, dispatch});

export const deletePermanentlyDemand = (event, demand, dispatch) =>
    fromDemands.deletePermanentlyDemand({event, demand, dispatch});

export const activateDraftDemand = (event, demand, dispatch) =>
    fromDemands.activateDraftDemand({event, demand, dispatch});


// Modal
export const showModal = ({type, props} = {}) => (dispatch) =>
    dispatch(fromModal.showModal({type, props}));

export const hideModal = () => (dispatch) => {
    dispatch(fromModal.hideModal());
};


// Users

export const setSearchUsersQuery = (query) =>
    fromSearchUsers.setSearchUsersQuery({query});

export const searchUsers = (query) => (dispatch, getState) =>
    fromSearchUsers.searchUsers({query, dispatch, getState});

export const fetchUsers = () => (dispatch, getState) =>
    fromUsers.fetchUsers({dispatch, getState});

export const fetchUser = (id) => (dispatch, getState) =>
    fromUsers.fetchUser({id, dispatch, getState});

export const fetchAuthUser = () => (dispatch, getState) =>
    fromUsers.fetchAuthUser({dispatch, getState});

export const fetchTrustedUsers = () => (dispatch, getState) =>
    fromTrustedUsers.fetchTrustedUsers({dispatch, getState});

export const addTrustedUser = (email) => (dispatch, getState) => {

    return fromTrustedUsers.addTrustedUser({email, dispatch, getState}).then(() => {
        const notificationOpts = {
            title: 'Staff added successfully',
            message: 'Him/her will now be visible in "My Staff"',
            position: 'tr',
            autoDismiss: 10
        };
        dispatch(Notifications.info(notificationOpts));
    })

};

export const removeTrustedUser = (id) => (dispatch, getState) => {
    return fromTrustedUsers.removeTrustedUser({id, dispatch, getState}).then(() => {
        const notificationOpts = {
            title: 'Staff removed successfully',
            message: 'Him/her will now not be visible in "My Staff"',
            position: 'tr',
            autoDismiss: 10
        };
        dispatch(Notifications.info(notificationOpts));
    })

};

// categories
export const fetchCategories = () => (dispatch, getState) =>
    fromCategories.fetchCategories({dispatch, getState});

// timeframes
export const fetchTimeFrames = () => (dispatch, getState) =>
    fromStatuses.fetchTimeFrames({dispatch, getState});

// Country
export const fetchCountries = () => (dispatch, getState) =>
    fromCountries.fetchCountries({dispatch, getState});

// HOME

export const setHomeOngoingDemandsFilters = (filters) => ({type: 'SET_HOME_ONGOING_DEMANDS_FILTERS', filters});

export const setHomeCompletedDemandsFilters = (filters) => ({type: 'SET_HOME_COMPLETED_DEMANDS_FILTERS', filters});

export const setUserOngoingDemandsFilters = (filters = {}) => ({type: 'SET_USER_ONGOING_DEMANDS_FILTERS', filters});

export const setUserCompletedDemandsFilters = (filters = {}) => ({type: 'SET_USER_COMPLETED_DEMANDS_FILTERS', filters});

export const fetchUserStats = (authUser, dispatch, getState) =>
    fromHome.fetchUserStats({dispatch, authUser, getState});

export const changeSelectedTab = (selectedTab, tabNamespace) => (dispatch, getState) =>
    dispatch(fromTabs.changeSelectedTab(selectedTab, tabNamespace));

// Search
export const setSearchDemandFilters = (filters) => (dispatch) =>
    dispatch({type: 'SET_SEARCH_DEMAND_FILTERS', filters});

export const toggleDrawer = () => (dispatch) => {
    const previous = LocalStorage.getItem("drawer");
    const next = previous === "closed" ? "opened" : "closed";
    LocalStorage.setItem("drawer", next);
    dispatch({type: 'TOGGLE_DRAWER'});
};

// Events
export const removeEvent = ({id, demand}) => (dispatch, getState) => {
    return fromDemands.removeEvent({id, demand, dispatch, getState});
};

export const editEvent = ({event, demand}) => (dispatch, getState) => {
    return fromDemands.editEvent({event, demand, dispatch, getState});
};

// Iteration

export const submitIteration = (event, iteration, dispatch) =>
    fromIterations.submitIteration({event, iteration, dispatch});

export const approveIteration = (event, iteration, dispatch) => {

    const notificationOpts = {
        title: 'Delivery completed!',
        message: 'It will now marked as completed.',
        position: 'tr',
        autoDismiss: 10
    };
    dispatch(Notifications.info(notificationOpts));
    return fromIterations.approveIteration({event, iteration, dispatch});

};

export const rejectIteration = (event, iteration, dispatch) =>
    fromIterations.rejectIteration({event, iteration, dispatch});

export const reopenIteration = (event, iteration, dispatch) =>
    fromIterations.reopenIteration({event, iteration, dispatch});

export const ping = () => {
    api.ping();
    return {type: 'PING'}
};
