import {schema} from 'normalizr';

export const category = new schema.Entity('categories');
export const arrayOfCategories = new schema.Array(category);
