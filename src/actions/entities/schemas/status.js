import {schema} from 'normalizr';

export const status = new schema.Entity('statuses');
export const arrayOfStatuses = new schema.Array(status);
