import {schema} from 'normalizr';

export const sdg = new schema.Entity('sdg');
export const arrayOfSdg = new schema.Array(sdg);

export const sp = new schema.Entity('sp');
export const arrayOfSp = new schema.Array(sp);

export const cabinet = new schema.Entity('cabinet');
export const arrayOfCabinet = new schema.Array(cabinet);

export const ddo = new schema.Entity('ddo');
export const arrayOfDdo = new schema.Array(ddo);

export const ddn = new schema.Entity('ddn');
export const arrayOfDdn = new schema.Array(ddn);

export const ddp = new schema.Entity('ddp');
export const arrayOfDdp = new schema.Array(ddp);

export const es = new schema.Entity('es');
export const arrayOfEs = new schema.Array(es);

export const sp1 = new schema.Entity('sp1');
export const arrayOfSp1 = new schema.Array(sp1);

export const sp2 = new schema.Entity('sp2');
export const arrayOfSp2 = new schema.Array(sp2);

export const sp3 = new schema.Entity('sp3');
export const arrayOfSp3 = new schema.Array(sp3);

export const sp4 = new schema.Entity('sp4');
export const arrayOfSp4 = new schema.Array(sp4);

export const sp5 = new schema.Entity('sp5');
export const arrayOfSp5 = new schema.Array(sp5);
