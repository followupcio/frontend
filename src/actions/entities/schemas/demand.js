import {schema} from 'normalizr';

export const demand = new schema.Entity('demands');
export const arrayOfDemands = new schema.Array(demand);
