import {schema} from 'normalizr';

export const highPriority = new schema.Entity('highPriorities');
export const arrayOfHighPriorities = new schema.Array(highPriority);
