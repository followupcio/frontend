import {schema} from 'normalizr';

export const trustedUser = new schema.Entity('trustedUsers', {});
export const arrayOfTrustedUsers = new schema.Array(trustedUser);
