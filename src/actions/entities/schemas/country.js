import {schema} from 'normalizr';

export const country = new schema.Entity('countries');
export const arrayOfCountries = new schema.Array(country);
