import {schema} from 'normalizr';

export const timeFrame = new schema.Entity('timeFrames');
export const arrayOfTimeFrames = new schema.Array(timeFrame);
