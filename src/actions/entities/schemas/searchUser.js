import {schema} from 'normalizr';

export const searchUser = new schema.Entity('searchUsers', {}, {idAttribute: "index_number"});
export const arrayOfSearchUsers = new schema.Array(searchUser);
