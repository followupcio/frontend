import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/user.js';
import {getIsFetching} from '../../../selectors/entities/users';

export const fetchUsers = ({dispatch, getState}) => {
    if (getIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_USERS_REQUEST',
    });

    return api.fetchUsers().then(
        (response) => {
            dispatch({
                type: 'FETCH_USERS_SUCCESS',
                receivedAt: performance.now(),
                response: normalize(response || [], schema.arrayOfUsers),
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_USERS_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });
};

export const fetchUser = ({id, dispatch}) => {
    dispatch({
        type: 'FETCH_USER_REQUEST',
        id,
    });

    return api.fetchUser(id).then(
        (response) => {
            dispatch({
                type: 'FETCH_USER_SUCCESS',
                response: normalize(response || {}, schema.user),
                id,
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_USER_FAILURE',
                id,
                message: error.message || 'Something went wrong.',
            });
        });
};

export const fetchAuthUser = ({dispatch, getState}) => {

    dispatch({
        type: 'FETCH_AUTH_USER_REQUEST',
    });

    return api.fetchAuthUser().then(
        (response) => {
            dispatch({
                type: 'FETCH_AUTH_USER_SUCCESS',
                response: normalize(response || {}, schema.user),
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_AUTH_USER_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });
};
