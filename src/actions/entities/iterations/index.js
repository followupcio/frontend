import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/demand.js';

export const submitIteration = ({event, iteration, dispatch}) =>
    api.submitIteration(event, iteration).then((response) => {
        dispatch({
            type: 'SUBMIT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const approveIteration = ({event, iteration, dispatch}) =>
    api.approveIteration(event, iteration).then((response) => {
        dispatch({
            type: 'APPROVE_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const rejectIteration = ({event, iteration, dispatch}) =>
    api.rejectIteration(event, iteration).then((response) => {
        dispatch({
            type: 'REJECT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const reopenIteration = ({event, iteration, dispatch}) =>
    api.reopenIteration(event, iteration).then((response) => {
        dispatch({
            type: 'REOPEN_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });