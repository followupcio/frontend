import {normalize} from 'normalizr';
import * as api from '../../../api';
import * as schema from '../schemas/timeFrame';
import {getIsFetching} from '../../../selectors/entities/timeFrames';

export const fetchTimeFrames = ({dispatch, getState}) => {

    if (getIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_TIME_FRAMES_REQUEST',
        status: "ongoing"
    });

    return api.fetchTimeFrames().then(
        (response) => {
            dispatch({
                type: 'FETCH_TIME_FRAMES_SUCCESS',
                receivedAt: performance.now(),
                response: normalize(response || [], schema.arrayOfTimeFrames),
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_TIME_FRAMES_FAILURE',
                status: "ongoing",
                message: error.message || 'Something went wrong.',
            });
        });
};
