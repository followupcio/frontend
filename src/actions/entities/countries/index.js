import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/country.js';
import {getIsFetching} from '../../../selectors/entities/countries.js';

export const fetchCountries = ({dispatch, getState}) => {
    if (getIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_COUNTRIES_REQUEST',
    });

    return api.fetchCountries().then(
        (response) => {
            dispatch({
                type: 'FETCH_COUNTRIES_SUCCESS',
                receivedAt: performance.now(),
                response: normalize(response|| [], schema.arrayOfCountries) || [],
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_COUNTRIES_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });
};
