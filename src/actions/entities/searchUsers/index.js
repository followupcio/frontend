import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/searchUser.js';

export const setSearchUsersQuery = ({query, dispatch}) => ({
    query,
    type: 'SET_SEARCH_USERS_QUERY',
});

export const searchUsers = ({query, dispatch, getState}) => {
    /*    if (getIsFetching(getState())) {
            return;
        }*/

    dispatch({
        type: 'SEARCH_USERS_REQUEST',
    });

    return api.searchUsers(query).then(
        (response) => {
            dispatch({
                type: 'SEARCH_USERS_SUCCESS',
                receivedAt: performance.now(),
                response: normalize(response || [], schema.arrayOfSearchUsers),
            });
        },
        (error) => {
            dispatch({
                type: 'SEARCH_USERS_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });
};
