import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/demand.js';
import {getIsFetching} from '../../../selectors/entities/demands';

const dispatcher = (dispatch) => (response) => dispatch({
    type: 'ADD_DEMAND_SUCCESS',
    response: normalize(response, schema.arrayOfDemands)
});

export const addDemand = ({demand, dispatch}) =>
    api.addDemand(demand)
        .then(response => {
            dispatch({
                type: 'ADD_DEMAND_SUCCESS',
                response: normalize(response || {}, schema.demand),
            });

            return response
        });

export const createDemandDraft = ({demand, dispatch}) => {
    return !!demand.id ? onEditDraft({demand, dispatch}) : addDemand({demand, dispatch});
};

export const onEditDraft = ({demand, dispatch}) => {
    return api.editDemand(demand)
        .then((response) => {
            dispatch({
                type: 'EDIT_DEMAND_SUCCESS',
                response: normalize(response| {}, schema.demand),
            });
            return response
        });
};
export const editDemand = ({demand, dispatch}) => {
    return api.editDemand(demand).then((response) => {

        // Invalidate parent if exist
        if (response.parent_id) {
            dispatch({
                type: 'DELETE_DEMAND_SUCCESS',
                response: normalize({id: response.parent_id}, schema.demand)
            });
        }

        dispatch({
            type: 'EDIT_DEMAND_SUCCESS',
            response: normalize(response || {}, schema.demand)
        });
    });
};

export const submitDemand = ({event, demand, dispatch}) =>
    api.submitDemand(event, demand).then((response) => {
        dispatch({
            type: 'SUBMIT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const draftSubmitDemand = ({event, demand, dispatch}) =>
    api.draftSubmitDemand(event, demand).then((response) => {
        dispatch({
            type: 'DRAFT_SUBMIT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const removeEvent = ({id, demand, dispatch}) =>
    api.removeEvent(id, demand).then((response) => {
        dispatch({
            type: 'REMOVE_EVENT_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const editEvent = ({event, demand, dispatch}) =>
    api.editEvent(event, demand).then((response) => {
        dispatch({
            type: 'EDIT_EVENT_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const addComment = ({event, demand, dispatch}) =>
    api.addComment(event, demand).then((response) => {
        dispatch({
            type: 'COMMENT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const approveDemand = ({event, demand, dispatch}) =>
    api.approveDemand(event, demand)
        .then((response) => {
            dispatch({
                type: 'APPROVE_DEMAND_SUCCESS',
                response: normalize(response.data|| {}, schema.demand),
            });
        });

export const rejectDemand = ({event, demand, dispatch}) =>
    api.rejectDemand(event, demand).then((response) => {
        dispatch({
            type: 'REJECT_DEMAND_SUCCESS',
            response: normalize(response.data|| {}, schema.demand),
        });
    });

export const deleteDemand = ({event, demand, dispatch}) =>
    api.deleteDemand(event, demand).then((response) => {
        dispatch({
            type: 'DELETE_DEMAND_SUCCESS',
            response: normalize(response.data|| {}, schema.demand),
        });
    });

export const undeleteDemand = ({event, demand, dispatch}) =>
    api.undeleteDemand(event, demand).then((response) => {
        dispatch({
            type: 'UNDELETE_DEMAND_SUCCESS',
            response: normalize(response.data|| {}, schema.demand),
        });
    });

export const archiveDemand = ({event, demand, dispatch}) =>
    api.archiveDemand(event, demand).then((response) => {
        dispatch({
            type: 'ARCHIVE_DEMAND_SUCCESS',
            response: normalize(response.data|| {}, schema.demand),
        });
    });

export const unarchiveDemand = ({event, demand, dispatch}) =>
    api.unarchiveDemand(event, demand).then((response) => {
        dispatch({
            type: 'UNARCHIVE_DEMAND_SUCCESS',
            response: normalize(response.data|| {}, schema.demand),
        });
    });

export const deletePermanentlyDemand = ({event, demand, dispatch}) =>
    api.deletePermanentlyDemand(event, demand);

export const activateDraftDemand = ({event, demand, dispatch}) =>
    api.activateDraftDemand(event, demand, dispatcher(dispatch)).then((response) => {
        dispatch({
            type: 'EDIT_DEMAND_SUCCESS',
            response: normalize(response|| {}, schema.demand),
        });
    });

export const fetchDemandsStats = ({authUser, user, dispatch}) =>
    api.fetchDemandsStats(authUser, user).then((response) => {
        dispatch({
            type: 'FETCH_DEMANDS_STATS_SUCCESS',
            response: response,
        });
    });

export const reopenDemand = ({event, demand, dispatch}) =>
    api.reopenDemand(event, demand)
        .then((response) => {
            dispatch({
                type: 'REOPEN_DEMAND_SUCCESS',
                response: normalize(response.data|| {}, schema.demand),
            });
        });

// thunk middleware
// Thunk allows you to write event creators that return a function instead of an event
export const exportDemands = ({status, filters, dispatch, getState, authUser, user}) => {

    return api.exportDemands(status, filters, authUser, user).then(
        (response) => {
            const url = response.url;
            if (url) {
                window.location.assign(url);
                //window.open(url,'_blank');
            }

        })
};


export const fetchDemands = ({status, filters, dispatch, getState, authUser, user, skip, limit, order}) => {

    if (getIsFetching(getState())) {
        return;
    }

    const startedAt = performance.now();

    dispatch({
        type: 'FETCH_DEMANDS_REQUEST',
        status,
        filters,
    });

    const {promise, source} = api.fetchDemands(status, filters, authUser, user, skip, limit, order);

    promise.then(
        (response) => {

            const receivedAt = performance.now();

            dispatch({
                type: 'FETCH_DEMANDS_SUCCESS',
                receivedAt,
                timing: receivedAt - startedAt,
                response: normalize(response || [], schema.arrayOfDemands),
                status,
                filters,
            });
        },
        (error) => {

            dispatch({
                type: 'FETCH_DEMANDS_FAILURE',
                status,
                filters,
                receivedAt: performance.now(),
                message: error.message || 'Something went wrong.',
            });
        });


    return {promise, source};
};

export const fetchDemand = ({id, dispatch}) => {
    dispatch({
        type: 'FETCH_DEMAND_REQUEST',
        id,
    });

    return api.fetchDemand(id).then(
        (response) => {
            dispatch({
                type: 'FETCH_DEMAND_SUCCESS',
                response: normalize(response|| {}, schema.demand),
                id,
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_DEMAND_FAILURE',
                id,
                message: error.message || 'Something went wrong.',
            });
        });
};
