import {normalize} from 'normalizr';
import * as api from '../../../api';
import * as schema from '../schemas/category';
import {getIsFetching} from '../../../selectors/entities/categories';

export const fetchCategories = ({dispatch, getState}) => {
    if (getIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_CATEGORIES_REQUEST',
    });

    return api.fetchCategories().then(
        (response) => {
            dispatch({
                type: 'FETCH_CATEGORIES_SUCCESS',
                receivedAt: performance.now(),
                response: normalize(response || [], schema.arrayOfCategories),
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_CATEGORIES_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });
};
