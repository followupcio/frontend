import * as api from '../../../api';
import {normalize} from 'normalizr';
import * as schema from '../schemas/trustedUser.js';
import {getIsFetching} from '../../../selectors/entities/trustedUsers';
import {getAuthUser} from '../../../selectors/authentication';

export const fetchTrustedUsers = ({dispatch, getState}) => {
    if (getIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_TRUSTED_USERS_REQUEST',
    });

    const authUser = getAuthUser(getState());

    return api.fetchTrustedUsers(authUser.id, authUser)
        .then(
            (response) => {
                dispatch({
                    type: 'FETCH_TRUSTED_USERS_SUCCESS',
                    receivedAt: performance.now(),
                    response: normalize(response || {}, schema.arrayOfTrustedUsers),
                });
            },
            (error) => {
                dispatch({
                    type: 'FETCH_TRUSTED_USERS_FAILURE',
                    message: error.message || 'Something went wrong.',
                });
            });
};

export const addTrustedUser = ({email, dispatch, getState}) =>
    api.addTrustedUser(email)
        .then(() => {
                fetchTrustedUsers({dispatch, getState});
            },
            (error) => {
                dispatch({
                    type: 'FETCH_TRUSTED_USERS_FAILURE',
                    message: error.message || 'Something went wrong.',
                });
            });


export const removeTrustedUser = ({id, dispatch, getState}) => {
    const authUser = getAuthUser(getState());

    return api.removeTrustedUser(authUser.id, id)
        .then(
            () => {
                fetchTrustedUsers({dispatch, getState});
            },
            (error) => {
                dispatch({
                    type: 'FETCH_TRUSTED_USERS_FAILURE',
                    message: error.message || 'Something went wrong.',
                });
            });
};
