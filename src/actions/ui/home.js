import * as api from '../../api/';
import {getChartDemandStatusIsFetching} from '../../selectors/ui/home';


export const fetchUserStats = ({dispatch, getState, authUser}) => {

    if (getChartDemandStatusIsFetching(getState())) {
        return;
    }

    dispatch({
        type: 'FETCH_HOME_DEMAND_STATUS_VALUES_REQUEST',
    });

    return api.fetchUserStats(authUser).then(
        (response) => {
            dispatch({
                type: 'FETCH_HOME_DEMAND_STATUS_VALUES_SUCCESS',
                receivedAt: performance.now(),
                response: response,
            });
        },
        (error) => {
            dispatch({
                type: 'FETCH_HOME_DEMAND_STATUS_VALUES_FAILURE',
                message: error.message || 'Something went wrong.',
            });
        });

};

