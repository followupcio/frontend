export const changeSelectedTab = (selectedTab, tabNamespace) => {
    return {
        type: 'CHANGE_SELECTED_TAB',
        tab: selectedTab,
        namespace: tabNamespace
    };
};
