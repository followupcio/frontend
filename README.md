# Follow up - UI

User Interface of the Follow up project.

## Requirements

* [NodeJS](http://nodejs.org/)
* [NPM](https://npmjs.org/)

## Getting started

- `npm i` to install dependencies
- `npm start` to run the dev server on `http://localhost:8080`
- `npm run build` to create the dist folder



